<?php
    function ucPol ($string) {
        return mb_strtoupper(mb_substr($string,0,1,'UTF-8'), 'UTF-8').mb_strtolower(mb_substr($string, 1,strlen($string),'UTF-8'), 'UTF-8');
    }

    define( 'SHORTINIT', true );
    require( dirname(__FILE__).'/wp-load.php');                
    global $wpdb;

    if (file_exists($_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/webcost/inc/geo/TERC_Urzedowy.xml')) {
        $xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/webcost/inc/geo/TERC_Urzedowy.xml');
        if (count($xml->catalog->row) > 0) {
            $wpdb->query('TRUNCATE '.$wpdb->prefix.'geo_list');            
            $full_list = array();
            $compare_list = array();
            foreach($xml->catalog->row as $item) {
                if ($item->RODZ == '' && $item->GMI == '' && $item->POW == '') {
                    $wpdb->insert( $wpdb->prefix.'geo_list', ['name' => ucPol((string)$item->NAZWA), 'id_type' => 1] );
                    $compare_list[(string)$item->WOJ] = array('name' => ucPol((string)$item->NAZWA));
                } else if ($item->RODZ == '' && $item->GMI == '' && $item->POW != '') {
                    if ($compare_list[(string)$item->WOJ] !== null) {
                        $w = $wpdb->get_var('Select id from '.$wpdb->prefix.'geo_list where name = "'.ucPol($compare_list[(string)$item->WOJ]['name']).'" and id_type = 1');
                        // $full_list[$compare_list[(string)$item->WOJ]['name']][ucfirst(mb_strtolower((string)$item->NAZWA))] = array();
                        $wpdb->insert( $wpdb->prefix.'geo_list', ['name' => ucPol((string)$item->NAZWA), 'id_type' => 2, 'id_parent' => $w] );
                        $compare_list[(string)$item->WOJ][(string)$item->POW] = array('name' => ucPol((string)$item->NAZWA));
                    }                 
                }
            }
            if (file_exists($_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/webcost/inc/geo/SIMC_Urzedowy.xml')) {
                $xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/webcost/inc/geo/SIMC_Urzedowy.xml');
                if (count($xml->catalog->row) > 0) {
                    foreach($xml->catalog->row as $item) {
                        if ((string)$item->WOJ != '' && (string)$item->POW != '') {
                            if ($compare_list[(string)$item->WOJ] !== null && $compare_list[(string)$item->WOJ][(string)$item->POW] !== null) {
                                $allow_rodz = array('1','2','3','4','8','9');
                                if (in_array((string)$item->RODZ_GMI, $allow_rodz)) {
                                    $p = $wpdb->get_var('SELECT l.id from '.$wpdb->prefix.'geo_list l JOIN '.$wpdb->prefix.'geo_list l2 ON l2.id=l.id_parent where l.name = "'.ucPol($compare_list[(string)$item->WOJ][(string)$item->POW]['name']).'" and l.id_type = 2 and l2.id_type = 1 AND l2.name = "'.ucPol($compare_list[(string)$item->WOJ]['name']).'"');
                                    $wpdb->insert( $wpdb->prefix.'geo_list', ['name' => ucPol((string)$item->NAZWA), 'id_type' => 3, 'id_parent' => $p] );
                                    // $full_list[$compare_list[(string)$item->WOJ]['name']][$compare_list[(string)$item->WOJ][(string)$item->POW]['name']][] = ucfirst(mb_strtolower((string)$item->NAZWA));
                                }
                            }   
                        }                         
                    }
                }
            }            
            //file_put_contents($_SERVER['DOCUMENT_ROOT'].'/geo_list.json', json_encode($full_list));   
        }
    }
?>