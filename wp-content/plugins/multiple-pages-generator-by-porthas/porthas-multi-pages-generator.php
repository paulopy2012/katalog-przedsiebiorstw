<?php

/**
 * Plugin Name: Multiple Pages Generator by Themeisle
 * Plugin URI: https://themeisle.com/plugins/multi-pages-generator/
 * Description: Plugin for generation of multiple frontend pages from CSV data file.
themeisle-headers
 * Author: Themeisle
 * Author URI: https://themeisle.com
 * Version: 3.1.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

defined( 'MPG_BASENAME' ) || define( 'MPG_BASENAME', __FILE__ );
defined( 'MPG_MAIN_DIR' ) || define( 'MPG_MAIN_DIR', dirname( __FILE__ ) );

add_action( 'admin_init', function () {
	if ( is_plugin_active( 'multi-pages-plugin/porthas-multi-pages-generator.php' )
	     && is_plugin_active( 'multi-pages-plugin-premium/porthas-multi-pages-generator.php' ) ) {
		deactivate_plugins( [ 'multi-pages-plugin/porthas-multi-pages-generator.php' ] );
		add_action( 'admin_notices', function () {
			printf(
				'<div class="notice notice-warning"><p><strong>%s</strong><br>%s</p><p></p></div>',
				sprintf(
				/* translators: %s: Name of deactivated plugin */
					__( '%s plugin deactivated.' ),
					'Multiple Pages Generator(Free)'
				),
				'Using the Premium version of Multiple Pages Generator is not requiring using the Free version anymore.'
			);
		} );
	}
} );

if ( ! function_exists( 'mpg_run' ) ) {
	function mpg_run() {
		static $has_run = false;
		if ( $has_run ) {
			return;
		}
		// ... Your plugin's main file logic ...
		require_once 'controllers/CoreController.php';
		require_once 'controllers/HookController.php';
		require_once 'controllers/MenuController.php';
		require_once 'controllers/SearchController.php';
		// Запуск базового функционала подмены данных
		MPG_HookController::init_replacement();
		// Запуск всяких actions, hooks, filters
		MPG_HookController::init_base();
		// Запуск хуков для ajax. Связываем роуты и функции
		MPG_HookController::init_ajax();
		// Инициализация бокового меню в WordPress
		MPG_MenuController::init();

		add_filter( 'themeisle_sdk_products', function ( $products ) {
			$products[] = __FILE__;

			return $products;
		} );
		add_filter( 'themeisle_sdk_hide_dashboard_widget', '__return_false' );
		$vendor_file = trailingslashit( plugin_dir_path( __FILE__ ) ) . 'vendor/autoload.php';
		if ( is_readable( $vendor_file ) ) {
			require_once $vendor_file;
		}
		if ( ! $has_run ) {
			$has_run = true;
		}
	}
}

require_once 'helpers/Themeisle.php';