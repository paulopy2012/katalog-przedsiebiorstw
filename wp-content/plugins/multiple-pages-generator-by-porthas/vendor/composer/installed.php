<?php return array(
    'root' => array(
        'pretty_version' => 'v3.1.0',
        'version' => '3.1.0.0',
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '21e6dc9494739d23b8ec03dbc09894016f008921',
        'name' => 'codeinwp/multi-pages-plugin',
        'dev' => false,
    ),
    'versions' => array(
        'codeinwp/multi-pages-plugin' => array(
            'pretty_version' => 'v3.1.0',
            'version' => '3.1.0.0',
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '21e6dc9494739d23b8ec03dbc09894016f008921',
            'dev_requirement' => false,
        ),
        'codeinwp/themeisle-sdk' => array(
            'pretty_version' => '3.2.24',
            'version' => '3.2.24.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../codeinwp/themeisle-sdk',
            'aliases' => array(),
            'reference' => 'e5c171e33120fdf8ce6dd3a7bddad984583023f0',
            'dev_requirement' => false,
        ),
    ),
);
