��    &      L      |      |     }     �     �  /   �     �     �     �               %     >     G     O  
   W     b  
   g     r     �  	   �     �     �  
   �     �     �     �     �     �     �     �  )   �  
     d   &     �     �     �     �     �  �  �     �     �  	   �  /   �          +     B     P     `      q     �     �     �     �     �     �     �     �     �     �     �     �  
   �                         "  
   0  8   ;     t  e   �     �     	     	  %   	     D	   <strong>%s</strong> now Active Cancel Cancel this user's subscription at the gateway. Change Password Confirm New Password Current Level Current Password Date Modified Display name publicly as End Date Expired Expires Expires In FREE First Name First&nbsp;Name Free Last Name Last&nbsp;Name Level Level Cost Level ID Membership Level New Password No None Not paying. Paid Send the user an email about this change. Start Date This will not change the subscription at the gateway unless the 'Cancel' checkbox is selected below. Update Profile User is not paying. Yes Your profile has been updated. none Project-Id-Version: paid-memberships-pro
Report-Msgid-Bugs-To: info@paidmembershipspro.com
POT-Creation-Date: 2021-07-06 03:01+1000
PO-Revision-Date: 2021-11-04 10:46+0000
Last-Translator: 
Language-Team: Polski
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pl_PL
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2);
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.5.3; wp-5.8.1 <strong>%s</strong> teraz Aktywny Anulować Anuluj subskrypcję tego użytkownika w bramie. Zmień hasło Potwierdź nowe hasło Obecny Pakiet Aktualne hasło Data modyfikacji Wyświatlaj imie publicznie jako Data końcowa Wygasły Wygasa Wygasa w ciągu Darmowy Imię Imię Darmowy Nazwisko Nazwisko Pakiet Koszt ID pakietu Pakiet Nowe hasło Nie Brak Nie płacąc. Zapłacony Wyślij użytkownikowi wiadomość e-mail o tej zmianie. Data rozpoczęcia Nie spowoduje to zmiany subskrypcji w bramce, chyba że poniżej zaznaczono pole wyboru „Anuluj”. Zaktualizować profil Użytkownik nie płaci. Tak Twój profil został zaktualizowany.  brak 