<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package webcost
 */
if (!isset($_GET['live'])) :
get_header();
?>
<main id="primary" class="site-main">
	<?php
endif;
		global $wpdb;

			if (isset($_GET['letter'])) :
				add_filter( 'posts_join', 'join_pmpro_user_table', 10, 2);
				add_filter( 'posts_orderby', 'orderby_package', 10, 2 );
				add_filter('posts_distinct', 'search_distinct');
				add_filter( 'posts_where', 'search_by_first_letter' );

				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array(
					'post_type' => 'firmy',
					'post_status' => 'publish',
					'suppress_filters' => FALSE,
					'paged' => $paged
				);

				if (isset($_GET['letter']))
					$args['start_with'] = $_GET['letter'];

				$posts = new WP_Query( $args );

				remove_filter( 'posts_where', 'search_by_first_letter' );

				if ( $posts->have_posts() ) :
				?>
					<div id="firm_search_result">
						<div class="container">
							<header class="page-header">
								<h3 class="page-title block_title">
									<?php
										printf( esc_html__( 'Firmy na literę %s', 'webcost' ), '<span>' . $_GET['letter'] . '</span>' );
									?>
								</h3>
							</header>
							<div class="row">
				<?php
						while ( $posts->have_posts() ) :
							$posts->the_post();
							get_template_part( 'template-parts/content', 'firm' );
						endwhile;
						kama_pagenavi($before = '', $after = '', $echo = true, $args = array(), $wp_query = $posts);

				?>
							</div>
						</div>
					</div>
				<?php
				else:
					get_template_part( 'template-parts/content', 'none' );
				endif;

				?>

				<?php

			elseif (isset($_GET['region']) && isset($_GET['s']) && isset($_GET['post_type'])) :
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$per_page = get_option( 'posts_per_page' );

				$sql_order = '';
				if (isset($_GET['order'])) {
					$order = $_GET['order'];
					if (strlen($order) == 0 || $order == 'name_asc') {
						$sql_order .= "wp_posts.post_name ASC";
					} else {
						$order = explode('_', $order);
						$sql_order .= "wp_posts.post_$order[0] $order[1]";
					}
				} else {
					$sql_order .= "wp_posts.post_name ASC";
				}

				if (isset($_GET['live'])) {
					$per_page = 15;
					$sql_query = '';
					if (isset($_GET['s']) && strlen($_GET['s']) > 0) {
						$sql_query .= "
						(
							wp_posts.post_title LIKE '%".trim($_GET["s"])."%'
						)";
					}

					$sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT wp_posts.ID, count(wp_posts.ID)
								FROM wp_posts
								INNER JOIN wp_pmpro_memberships_users ON (wp_pmpro_memberships_users.user_id = wp_posts.post_author)
								WHERE
									wp_posts.ID NOT IN (1309)
									AND wp_posts.post_type = 'firmy'
									AND wp_posts.post_status = 'publish'
									AND wp_pmpro_memberships_users.status = 'active'" .

									(strlen($sql_query) > 0 ? " AND $sql_query" : "") .

								" GROUP BY wp_posts.ID
								ORDER BY wp_pmpro_memberships_users.membership_id DESC" . (strlen($sql_order) > 0 ? ", ".$sql_order : "") . "
								LIMIT ".($per_page * ($paged - 1)).", ".($per_page * $paged - 1);
				} else {
					$sql_places = '';
					if (isset($_GET['region']) && $_GET['region'] != '') {
						$region = explode(',', $_GET['region']);
						$needCities = true;
						foreach ($region as $place) {
							$res = $wpdb->get_results("SELECT id, id_type from wp_geo_list where name = '".trim($place)."' limit 1");
							if ($res && count($res) > 0) {
								if ($res[0]->id_type != 2) {
									if ($res[0]->id_type == 3) {
										$needCities = false;
									}
									$sql_places .= (strlen($sql_places) > 0 ? ' AND ' : '')." wp_postmeta.meta_value LIKE '%".trim($place)."%'";
								} else {
									$sql_places_p = '';
									$cities = $wpdb->get_results("SELECT name from wp_geo_list where id_parent = ".$res[0]->id." AND id_type = 3");
									if ($cities && count($cities) > 0) {
										$sql_places_p .= '(';
										foreach ($cities as $key => $city) {
											$sql_places_p .= (strlen($sql_places_p) > 1 ? ' OR ' : '')." wp_postmeta.meta_value LIKE '%".trim($city->name)."%'";
										}
										$sql_places_p .= ' )';
									}
								}
							}
						}
						if (isset($sql_places_p) && strlen($sql_places_p) > 0 && $needCities) {
							$sql_places .= (strlen($sql_places) > 0 ? ' AND ' : ' ') . $sql_places_p;
						}
						$sql_places .= " AND wp_postmeta.meta_key = 'region'";
					}

					$sql_query = '';
					$sql_category = '';
					if (isset($_GET['s']) && strlen($_GET['s']) > 0) {
						if (isset($_GET['c']) && $_GET['c'] != '') {
							if (strlen($_GET['c']) == strlen((int)$_GET['c']))
								$cat_id = $_GET['c'];
							else
								$cat_id = get_cat_ID($_GET['c']);

							if ($cat_id) {
								$sql_category .= "wp_term_relationships.term_taxonomy_id = '$cat_id'";
							}
						} else {
							$sql_query .= "
							(
								(
									wp_postmeta.meta_value LIKE '%".trim($_GET["s"])."%'
									AND
									(
										wp_postmeta.meta_key in ('Adres', 'numer telefonu', 'e-mail', 'strona_internetowa')
										OR
										wp_postmeta.meta_key LIKE '%_opis_uslugi'
									)
								)
								OR
								(
									wp_posts.post_title LIKE '%".trim($_GET["s"])."%'
									OR
									wp_posts.post_content LIKE '%".trim($_GET["s"])."%'
								)
							)";
						}
					}

					$sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT wp_posts.ID, count(wp_posts.ID)
								FROM wp_posts
								INNER JOIN wp_postmeta ON ( wp_posts.ID = wp_postmeta.post_id )
								INNER JOIN wp_pmpro_memberships_users ON (wp_pmpro_memberships_users.user_id = wp_posts.post_author)" .
								(strlen($sql_category) > 0 ? "INNER JOIN wp_term_relationships ON (wp_term_relationships.object_id = wp_posts.ID)" : "") .
								" WHERE
									wp_posts.ID NOT IN (1309)
									AND wp_posts.post_type = 'firmy'
									AND wp_posts.post_status = 'publish'
									AND wp_pmpro_memberships_users.status = 'active'" .

									(strlen($sql_places) > 0 ? " AND ($sql_places)" : "") .

									(strlen($sql_query) > 0 ? " AND $sql_query" : "") .

									(strlen($sql_category) > 0 ? " AND $sql_category" : "") .

								" GROUP BY wp_posts.ID
								ORDER BY wp_pmpro_memberships_users.membership_id DESC" . (strlen($sql_order) > 0 ? ", ".$sql_order : "") . "
								LIMIT ".($per_page * ($paged - 1)).", ".($per_page * $paged);
				}

				$result = $wpdb->get_results($sql);
				$count = $wpdb->get_results('SELECT FOUND_ROWS() as total');
				if ($count && count($count)) {
					$count = $count[0]->total;
				}
				$ids = [];
				if ($result) {
					$ids = [];
					foreach ($result as $post) {
						$ids[] = $post->ID;
					}
				}

				$posts = new WP_Query([
					'post__in' => count($ids) > 0 ? $ids : [-1],
					'post_type' => 'firmy',
					'post_status' => 'publish'
				]);

				// if ($args_length != count($args)) :
					if ( $posts->have_posts() ) :
						if (isset($_GET['live'])) :
							foreach ($posts->posts as $post) :
								$names .= '<li class="dropdown-item"><a><span class="name">'.str_ireplace($_GET['s'],'<strong>'.$_GET['s'].'</strong>',$post->post_title).'</span></a></li>';
							endforeach;
							$categories = $wpdb->get_results('select term_id,name from '.$wpdb->prefix.'terms where term_id IN (Select term_id from '.$wpdb->prefix.'term_taxonomy where taxonomy = "category" AND term_id != 1) and name like "%'.$_GET['s'].'%"');
							if (is_array($categories)) :
								foreach ($categories as $kcat=>$cat) {
									$names .= '<li class="dropdown-item"><a><span class="name" data-id="'.$cat->term_id.'">'.str_ireplace($_GET['s'], '<strong>'.$_GET['s'].'</strong>',$cat->name).' <small>(kategoria)</small><span></a></li>';
								}
							endif;
							$names .= '</ul>';
							echo $names;
							exit;
						else :
							?>
							<div id="firm_search_result" class="12">
								<div class="container">
									<header class="page-header">
										<h3 class="page-title block_title">
											Wynik wyszukiwania
											<?php
												$parsed = [
													'm' => null,
													'p' => null,
													'w' => null
												];
												if (isset($_GET['region']) && $_GET['region'] != '') {
													$region = explode(', ',$_GET['region']);
													if (count($region)) {
														foreach ($region as $reg) {
															$tmp = $wpdb->get_results('select id_type from '.$wpdb->prefix.'geo_list where id_type in (1,2,3) and name = "'.$reg.'" limit 1');
															if ($tmp && count($tmp) && $tmp[0]->id_type) {
																if ($tmp[0]->id_type == 1) {
																	$parsed['w'] = ucfirst($reg);
																}
																if ($tmp[0]->id_type == 2) {
																	$parsed['p'] = ucfirst($reg);
																}
																if ($tmp[0]->id_type == 3) {
																	$parsed['m'] = ucfirst($reg);
																}
															}
														}
													}
												}
												if (count(array_filter($parsed)) > 0) {
													echo '<br>';
													if ($parsed['w']) {
														echo 'Województwo '.$parsed['w'].' ';
													}
													if ($parsed['p']) {
														echo 'Powiat '.$parsed['p'].' ';
													}
													if ($parsed['m']) {
														echo 'Miasto '.$parsed['m'].' ';
													}
												}
											?>
										</h3>
									</header>
									<div class="row">
										<?php
											while ( $posts->have_posts() ) :
												$posts->the_post();
												get_template_part( 'template-parts/content', 'firm' );
											endwhile;
										?>
									</div>
									<?php
										kama_pagenavi($before = '', $after = '', $echo = true, $args = array(), $wp_query = $posts, $count ?? null, $paged);
									?>
								</div>
							</div>
							<?php
						endif;
					else:
						if (isset($_GET['live'])) :
							echo 'nothing';
							exit;
						else :
							get_template_part( 'template-parts/content', 'none' );
						endif;
					endif;
				// else :
				// 	get_template_part( 'template-parts/content', 'none' );
				// endif;
			elseif (isset($_GET['tag']) && $_GET['tag'] == 'true' && isset($_GET['s']) && $_GET['s'] != ''):
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array(
					'post_type' => 'firmy',
					'post_status' => 'publish',
					'paged' => $paged,
					'tag' => strtolower(makeSlug(str_replace(' ','-',urldecode($_GET['s']))))
				);
				$posts = new WP_Query( $args );

				if ( $posts->have_posts() ) :
				?>
					<div id="firm_search_result">
						<div class="container">
							<header class="page-header">
								<h3 class="page-title block_title">
									<?php
										printf( esc_html__( 'Firmy z metką %s', 'webcost' ), '<span>' . $_GET['s'] . '</span>' );
									?>
								</h3>
							</header>
							<div class="row">
				<?php
						while ( $posts->have_posts() ) :
							$posts->the_post();
							get_template_part( 'template-parts/content', 'firm' );
						endwhile;
						kama_pagenavi($before = '', $after = '', $echo = true, $args = array(), $wp_query = $posts);

				?>
							</div>
						</div>
					</div>
				<?php
				else:
					get_template_part( 'template-parts/content', 'none' );
				endif;

			else :

				if ( have_posts() ) : ?>

					<div id="firm_search_result">
						<div class="container">
							<header class="page-header">
								<h3 class="page-title block_title">
									<?php
										printf( esc_html__( 'Wynik wyszukiwania zapytania %s', 'webcost' ), '<span>"' . $_GET['s'] . '"</span>' );
									?>
								</h3>
							</header>
							<div class="row">
						<?php
						while ( have_posts() ) :
							the_post();
							get_template_part( 'template-parts/content', 'firm' );
						endwhile;
						?>
							</div>
						</div>
					</div>
				<?php
				else :

					get_template_part( 'template-parts/content', 'none' );

				endif;
			endif;
			remove_filter( 'posts_join', 'join_pmpro_user_table');
			remove_filter( 'posts_orderby', 'orderby_package');

	?>

</main><!-- #main -->

<?php
// get_sidebar();
get_footer();
