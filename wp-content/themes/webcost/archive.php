<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package webcost
 */
get_header();
?>
	<main id="primary" class="site-main">
			<?php if (is_category()) :
					$category = get_queried_object();
			?>
				<header class="page-header container">
					<h1><?php echo $category->name; ?></h1>
					<div class="category_description row">
						<div class="col-xs-12 col-lg-6 description">
							<?php
								$cat_title = get_term_meta($category->term_id, '_pagetitle', true);
								if ($cat_title)
									echo "<h3>".$cat_title.'</h3>';
							?>
							<?php echo $category->category_description; ?>
						</div>
						<div class="col-xs-12 col-lg-6 thumb">
							<img src="<?php
								$img_cat = get_taxonomy_image($category->term_id);
								$containerClass = '';
								if ($img_cat == 'Please Upload Image First!') {
									$img_cat = get_template_directory_uri().'/assets/img/no_service_image_615x435.png';
									$containerClass = ' no-image';
								} else {
									$img_cat = explode('/',$img_cat);
									$filename = explode('.',$img_cat[count($img_cat) - 1]);
									$filename[count($filename)-2] .= '-621x440';
									$img_cat[count($img_cat) - 1] = implode('.',$filename);
									$img_cat = implode('/',$img_cat);
								}
								echo $img_cat;?>" alt="<?php echo $category->cat_name; ?>">
						</div>
					</div>
				</header><!-- .page-header -->
			<?php
				do_shortcode('[paid_firm theme="white" category="'.$category->term_id.'"]');
			?>
			<section class="all_firms">
					<?php

						$categories = get_term_children($category->term_id, 'category');
						$categories[] = $category->term_id;

						$meta = $wpdb->get_results('
							SELECT pm.meta_value
							FROM '.$wpdb->prefix.'term_relationships tr
							JOIN '.$wpdb->prefix.'posts p ON p.ID = tr.object_id
							JOIN '.$wpdb->prefix.'pmpro_memberships_users mu ON mu.user_id = p.post_author
							JOIN '.$wpdb->prefix.'postmeta pm ON pm.post_id = p.ID
							WHERE tr.term_taxonomy_id IN ('.implode(',',$categories).')
								AND p.post_status = "publish"
								AND mu.membership_id > 0
								AND mu.status = "active"
								AND pm.meta_key = "region_slug"
						');
						if ($meta) {
							$db_regions = [];
							foreach ($meta as $woj) {
								$slug = explode('-', $woj->meta_value)[0];
								if (!$slug)
									continue;
								$reg = $wpdb->get_row('select name, slug from '.$wpdb->prefix.'geo_list where slug = "'.$slug.'"');
								if (!$reg)
									continue;

								$db_regions[$slug] = $reg;
							}
						}

						if ($db_regions) {
							ksort($db_regions);
							echo '<div class="container"><div class="row">
                            		<div class="col-xs-12">
                                		<strong class="mb-2 d-block">
											Województwa, w których znajdują się firmy
                                		</strong>
                                		<div class="search_region_list_wrapper">
                                        	<ul class="search_region_list">';
												foreach($db_regions as $kr=>$region) {
													$count = $wpdb->get_results('select count(*) as count from '.$wpdb->prefix.'postmeta pm
																				JOIN '.$wpdb->prefix.'posts p ON p.ID = pm.post_id
																				JOIN '.$wpdb->prefix.'term_relationships tr ON tr.object_id = p.ID
																				JOIN '.$wpdb->prefix.'pmpro_memberships_users mu ON mu.user_id = p.post_author
																				where mu.membership_id > 0 AND mu.status = "active" and tr.term_taxonomy_id IN ('.implode(',',$categories).') and pm.meta_value like "%'.$region->slug.'-%" and pm.meta_key = "region_slug" AND p.post_status = "publish"');

													if ($count[0]->count > 0)
														echo "<li><a href='".get_category_link($category->term_id).$region->slug."'>"./*$kr.' -> '.*/$region->name." (".$count[0]->count.")</a></li>";

												}
							echo '			</ul>
										</div>
									</div>
						  		</div></div>';
						}
					?>
				<div class="container">
					<?php
						do_shortcode( '[all_firms category="'.$category->term_id.'"]' );
					?>
				</div>
			</section>
			<?php echo do_shortcode( '[last_blog cat="'.$category->term_id.'"]' ); ?>
			<?php do_shortcode('[home_text cat="'.$category->term_id.'"]'); ?>
			<?php endif; ?>
	</main><!-- #main -->
	<?php /*do_shortcode( '[nearby_firm]' );*/ ?>

<?php
// get_sidebar();
get_footer();
