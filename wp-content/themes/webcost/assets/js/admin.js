var $ = jQuery;

$(document).ready(function(){

    $('.acf-field').each(function(){
        if ($(this).data('name') == 'numer telefonu') {
            var id = $(this).data('key');
            $(this).find('input#acf-'+id).inputmask("+48 999 999 999", { "clearIncomplete": true });            
        }
        if ($(this).data('name') == 'e-mail') {
            var id = $(this).data('key');
            $(this).find('input#acf-'+id).inputmask('999999');            
        }           
    })

    if ($('#w_select').length > 0) {
        $('#p_select select').html('');
        $('#p_select').hide();
        $('#m_select select').html('');
        $('#m_select').hide();  

        jQuery('#w_select').change(function(){
            $('#p_select select').html('');
            $('#p_select').hide();
            $('#m_select select').html('');
            $('#m_select').hide();            

            $('.acf-field[data-name="region"] input[type="text"]').val($('#w_select .select2 .acf-selection').text());

            jQuery.ajax({
                url: "/wp-admin/admin-ajax.php",
                type: 'POST',
                data: {
                    action: 'get_districts',
                    w: $('#w_select .select2 .acf-selection').text(),            
                },
                dataType: 'html',
                success: function (result) {
                    $('#p_select select').html(result);
                    $('#p_select select').select2();
                    $('#p_select .select2').css('width','100%')
                    $('#p_select').show();
                },
                error: function (errorThrown) {
                    console.log(errorThrown);
                }
            });
        })
    }
    if ($('#p_select').length > 0) {
        jQuery('#p_select').change(function(){
            $('#m_select select').html('');
            $('#m_select').hide();         
            
            $('.acf-field[data-name="region"] input[type="text"]').val($('.acf-field[data-name="region"] input[type="text"]').val() + ', ' + $('#p_select .select2 .select2-selection__rendered').text());

            jQuery.ajax({
                url: "/wp-admin/admin-ajax.php",
                type: 'POST',
                data: {
                    action: 'get_cities',
                    p: $('#p_select .select2 .select2-selection__rendered').text(),          
                    w: $('#w_select .select2 .acf-selection').text(),
                },
                dataType: 'html',
                success: function (result) {
                    $('#m_select select').html(result);
                    $('#m_select select').select2();
                    $('#m_select .select2').css('width','100%')
                    $('#m_select').show();
                },
                error: function (errorThrown) {
                    console.log(errorThrown);
                }
            });
        })
    }    

    if ($('#m_select').length > 0) {
        jQuery('#m_select').change(function(){            
            $('.acf-field[data-name="region"] input[type="text"]').val($('.acf-field[data-name="region"] input[type="text"]').val() + ', ' + $('#m_select .select2 .select2-selection__rendered').text());
        })
    }     

    if ($('body.user-edit-php #profile-page form#your-profile').length > 0) {
        var del = [0,2,3,5];
        // $('body.user-edit-php #profile-page form#your-profile h2').each(function(i,e){
        //     if (del.indexOf(i) > -1)
        //         e.remove();
        // })
        // $('body.user-edit-php #profile-page form#your-profile table.form-table').each(function(i,e){
        //     if (del.indexOf(i) > -1)
        //         e.remove();
        // })   
        $('body.user-edit-php #profile-page form#your-profile h2').each(function(){
            if ($(this).text() != 'Контакты' && $(this).text() != 'Имя' && $(this).text() != 'Управление учётной записью' && $(this).text() != 'Name' && $(this).text() != 'Account Management' && $(this).text() != 'Contact Info' && $(this).text() != 'Nazwa' && $(this).text() != 'Zarządzanie kontem' && $(this).text() != 'Dane kontaktowe') {
                $(this).next().remove();
                $(this).remove();
            }
        });
        $('body.user-edit-php #profile-page form#your-profile h3').each(function(){
            if ($(this).text() != 'Тариф' && $(this).text() != 'Pakiet' && $(this).text() != 'Тариф') {
                $(this).next().remove();
                $(this).remove();
            }
        });        
    }
});