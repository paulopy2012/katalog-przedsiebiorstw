<?php
get_header();
?>

<main id="primary" class="site-main">
	<?php

        $url = add_query_arg( NULL, NULL );
        $service_name = urldecode(str_replace('/usluga/', '', preg_replace('/\?(.+?)$/m','',$url)));
        $region_name = '';

        $service_name = explode('-region-',$service_name);
        if (is_array($service_name)) {
            switch (count($service_name)) {
                case 1:
                    $service_name = $service_name[0];
                    break;
                default:
                    if (strlen($service_name[1]) > 0)
                        $region_name = $service_name[1];
                    $service_name = $service_name[0];
                    break;
            }
        }

        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
            'post_type' => 'firmy',
            'post_status' => 'publish',
            'paged' => $paged,
            'meta_query' => array('relation' => 'OR'),
        );

        for ($i=0; $i < 12; $i++) {
            $args['meta_query'][] = array(
                'key' => 'usluga_'.$i.'_slug',
                'value' => $service_name,
                'compare' => '='
            );
        }

        global $wpdb;
        $service_name = mb_strtolower($service_name, 'UTF-8');
        $sl = $wpdb->get_row('SELECT meta_value FROM '.$wpdb->prefix.'postmeta WHERE meta_key = CONCAT((SELECT REPLACE((SELECT meta_key FROM '.$wpdb->prefix.'postmeta p WHERE meta_value = "'.$service_name.'" AND meta_key LIKE "%_slug" LIMIT 1),"_slug", "")), "_opis_uslugi") AND post_id = (SELECT post_id FROM '.$wpdb->prefix.'postmeta p WHERE meta_value = "'.$service_name.'" LIMIT 1)');
        if ($sl)
            $service_name_full = $sl->meta_value;

        $region_name = mb_strtolower($region_name, 'UTF-8');
        $r_sl = $wpdb->get_row('select meta_value from '.$wpdb->prefix.'postmeta where post_id = (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_value = "'.$region_name.'" AND meta_key = "region_slug" LIMIT 1) AND meta_key="region"');
        if ($r_sl) {
            $region_name_full = $r_sl->meta_value;
        }

        $posts = new WP_Query( $args );
        $post_regions = array();
        $post_city = array();
        if ( $posts->have_posts() ) :
            foreach ($posts->posts as $post) :
                if ($region = get_post_meta($post->ID,'region',true)) {
                    $post_regions[] = trim($region);
                    $tmp = explode(',',$region);
                    $post_city[] = trim($tmp[count($tmp)-1]);
                }
                if ($region = get_post_meta($post->ID,'region_slug',true)) {
                    $post_regions_slug[] = trim($region);
                }
            endforeach;
            asort($post_city);
        endif;

        if ( $posts->have_posts() ) :
            ?>
            <div id="firm_search_result">
                <div class="container">
                    <header class="page-header">
                        <h1 class="page-title block_title">
                            <?php
                                echo $service_name_full;
                                if (strlen($region_name_full) > 0) {
                                    $r = explode(',',$region_name_full);
                                    echo ' w '.$r[count($r) - 1];
                                }
                            ?>
                        </h1>
                    </header>
                    <?php if (count($post_regions) > 0 && strlen($region_name) == 0)	: ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <strong class="mb-2 d-block">
                                    Miasta, w których znaleziono usługę
                                </strong>
                                <div class="search_region_list_wrapper">
                                    <?php
                                        $columns = ceil(count($post_city) / 5);
                                        for ($i=0;$i<$columns;$i++) {
                                    ?>
                                        <ul class="search_region_list">
                                            <?php
                                                for($j=0;$j<5;$j++) {
                                                    if ($post_city[$j]) {
                                                        ?>
                                                            <li><a title="Usługa <?php echo $service_name_full;?> w <?php echo $city; ?>" href="<?php echo home_url().'/usluga/'.$service_name.'-region-'.$post_regions_slug[$j]; ?>"><?php echo trim($post_city[$j]) ?></a></li>
                                                        <?php
                                                        unset($post_city[$j]);
                                                        unset($post_regions[$j]);
                                                        unset($post_regions_slug[$j]);
                                                    }
                                                }
                                                $post_city = array_values($post_city);
                                                $post_regions = array_values($post_regions);
                                                $post_regions_slug = array_values($post_regions_slug);
                                            ?>
                                        </ul>
                                    <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <?php
                            foreach ( $posts->posts as $post ) :
                                if (strlen($region_name) > 0) :
                                    if (trim(get_post_meta($post->ID, 'region_slug', true)) === $region_name) :
                                        get_template_part( 'template-parts/content', 'firm' );
                                    else :
                                        continue;
                                    endif;
                                else :
                                    get_template_part( 'template-parts/content', 'firm' );
                                endif;
                            endforeach;
                        ?>
                    </div>
                    <?php kama_pagenavi($before = '', $after = '', $echo = true, $args = array(), $wp_query = $posts); ?>
                </div>
            </div>
            <?php
        else:
            get_template_part( 'template-parts/content', 'none' );
        endif;

	?>

</main>

<?php
get_footer();