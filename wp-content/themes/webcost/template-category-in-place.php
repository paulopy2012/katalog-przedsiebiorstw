<?php
get_header();
?>

<main id="primary" class="site-main">
	<?php
        global $wpdb;
        $path = explode('/', trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/'));
        $cat = 2;
        $r = 1;
        $c = 0;
        $last = $wpdb->get_row('select * from '.$wpdb->prefix.'geo_list where slug = "'.$path[count($path) - 1].'"');
        $prelast = $wpdb->get_row('select * from '.$wpdb->prefix.'geo_list where slug = "'.$path[count($path) - 2].'"');

        if ($prelast) {
            $cat = 3;
            $r = 2;
            $c = 1;
        }

        $category = get_category_by_slug(strtolower($path[count($path) - $cat]));

        $categories = get_term_children($category->term_id, 'category');
        $categories[] = $category->term_id;

        if (!$prelast) {
            $db_region_name = $wpdb->get_row('Select name from '.$wpdb->prefix.'geo_list where id_type = 1 and slug = "'.strtolower($path[count($path) - $r]).'"');

            $meta = $wpdb->get_results('
                SELECT pm.meta_value
                FROM '.$wpdb->prefix.'term_relationships tr
                JOIN '.$wpdb->prefix.'posts p ON p.ID = tr.object_id
                JOIN '.$wpdb->prefix.'pmpro_memberships_users mu ON mu.user_id = p.post_author
                JOIN '.$wpdb->prefix.'postmeta pm ON pm.post_id = p.ID
                WHERE tr.term_taxonomy_id IN ('.implode(',',$categories).')
                    AND p.post_status = "publish"
                    AND mu.membership_id > 0
                    AND mu.status = "active"
                    AND pm.meta_key = "region_slug"
                    AND pm.meta_value like "'.strtolower($path[count($path) - $r]).'-%"
            ');

            if ($meta) {
                $db_cities = [];
                foreach ($meta as $woj) {
                    $slug = explode('-', $woj->meta_value);
                    $db_cities[] = $wpdb->get_row('select gl.name, gl.slug, gl3.name as w_name, gl3.slug as w_slug from '.$wpdb->prefix.'geo_list gl
                                                    JOIN '.$wpdb->prefix.'geo_list gl2 ON gl2.id = gl.id_parent
                                                    JOIN '.$wpdb->prefix.'geo_list gl3 ON gl3.id = gl2.id_parent
                                                    where gl.slug = "'.$slug[count($slug) - 1].'" and gl3.slug = "'.$slug[0].'"');
                }
            }
        } else {
            $db_city_name = $wpdb->get_row('Select name from '.$wpdb->prefix.'geo_list where id_type = 3 and slug = "'.strtolower($path[count($path) - $c]).'"');
        }

        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
            'post_type' => 'firmy',
            'post_status' => 'publish',
            'paged' => $paged,
            'cat' => $categories,
            'meta_query' => array('relation' => 'OR'),
        );

        $args['meta_query'][] = array(
            'key' => 'region_slug',
            'value' => $prelast ? '-'.strtolower($path[count($path) - 1]) : strtolower($path[count($path) - 1]).'-',
            'compare' => 'like'
        );

        $posts = new WP_Query( $args );

        if ( $posts->have_posts() ) :
            ?>
            <div id="firm_search_result">
                <div class="container">
                    <header class="page-header">
                        <h1 class="page-title block_title">
                            <?php
                                if (!$prelast)
                                    echo "Firmy w ".$db_region_name->name." branża ".strtolower($category->name);
                                else
                                    echo "Firmy w ".$db_city_name->name." branża ".strtolower($category->name);
                            ?>
                        </h1>
                    </header>
                    <?php if ($db_cities) : ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <strong class="mb-2 d-block">
                                    Miasta, w których znajdują się firmy
                                </strong>
                                <div class="search_region_list_wrapper">
                                    <ul class="search_region_list">
                                        <?php
                                            foreach ($db_cities as $city) :
                                                $count = $wpdb->get_results('select count(*) as count from '.$wpdb->prefix.'postmeta pm
                                                                            JOIN '.$wpdb->prefix.'posts p ON p.ID = pm.post_id
                                                                            JOIN '.$wpdb->prefix.'term_relationships tr ON tr.object_id = p.ID
                                                                            JOIN '.$wpdb->prefix.'pmpro_memberships_users mu ON mu.user_id = p.post_author
                                                                            where mu.membership_id > 0 AND mu.status = "active" and tr.term_taxonomy_id IN ('.implode(',',$categories).') and pm.meta_value like "%-'.$city->slug.'" and pm.meta_key = "region_slug" AND p.post_status = "publish"');

												if ($count[0]->count > 0)
                                                    echo "<li><a href='".get_category_link($category->term_id).$city->w_slug."/".$city->slug."'>".$city->name." (".$count[0]->count.")</a></li>";
                                            endforeach;
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <?php
                            foreach ( $posts->posts as $post ) :
                                if ($wpdb->get_row('select * from '.$wpdb->prefix.'pmpro_memberships_users where user_id = '.$post->post_author.' and membership_id > 0 and status = "active"')) :
                                    if (strlen($region_name) > 0) :
                                        if (trim(get_post_meta($post->ID, 'region_slug', true)) === $region_name) :
                                            get_template_part( 'template-parts/content', 'firm' );
                                        else :
                                            continue;
                                        endif;
                                    else :
                                        get_template_part( 'template-parts/content', 'firm' );
                                    endif;
                                endif;
                            endforeach;
                        ?>
                    </div>
                    <?php kama_pagenavi($before = '', $after = '', $echo = true, $args = array(), $wp_query = $posts); ?>
                </div>
            </div>
            <?php
        else:
            get_template_part( 'template-parts/content', 'none' );
        endif;
	?>
</main>

<?php
get_footer();