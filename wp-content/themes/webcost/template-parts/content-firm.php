<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package webcost
 */
$imgs = array(
	109	=>'IKO_2.png',
	110	=>'IKO_4.png',
	111	=>'IKO_10.png',
	112	=>'IKO_3.png',
	113	=>'IKO_5.png',
	114	=>'IKO_6.png',
	115	=>'IKO_1.png',
	116	=>'IKO_7.png',
	117	=>'IKO_9.png',
	118	=>'IKO_8.png',
);
$cat = get_the_terms( $post->ID, 'category');
if ($cat != null && count($cat) > 0)
	$cat = $cat[0];
$meta = get_post_meta($post->ID);
$img = get_the_post_thumbnail_url( $post->ID, 'medium' );
$containerClass = '';
if (!$img) {
	$img = get_template_directory_uri().'/assets/img/no_service_image_615x435.png';
	$containerClass = ' no-image';
}
$img_cat = file_exists(get_template_directory().'/assets/img/category/gray/'.(int)$cat->term_id.'.svg') ? get_template_directory_uri().'/assets/img/category/gray/'.(int)$cat->term_id.'.svg' : null;

?>

<div class="col-lg-4 col-md-6 col-xs-12">
	<article id="post-<?php the_ID(); ?>" <?php post_class('post_block'); ?>>
		<div class="img_container<?php echo $containerClass; ?>">
			<img src="<?php echo $img; ?>" alt="<?php echo $post->post_title; ?>">
		</div>
		<div class="post-content">
			<div class="post-title">
				<h3><?php echo $post->post_title; ?></h3>
			</div>
			<div class="post_category">
				<?php
					echo $img_cat ? '<img src="'.$img_cat.'" alt="'.$cat->slug.'">' : '';
					echo $cat->name;
				?>
			</div>
			<div class="post_metas">
				<?php

					if (isset($meta['Adres']) && strlen($meta['Adres'][0]) > 0)
						echo '<span><img src="'.get_template_directory_uri().'/assets/img/IK_POLOZENIE.png"><span>'.addUrlToAddress($meta['Adres'][0]).'</span></span>';
					if (isset($meta['numer telefonu']) && strlen($meta['numer telefonu'][0]) > 0)
						echo '<span><img src="'.get_template_directory_uri().'/assets/img/IK_TEL.png"><a href="tel:'.$meta['numer telefonu'][0].'"><span>'.$meta['numer telefonu'][0].'</span></a></span>';
					if (isset($meta['e-mail']) && strlen($meta['e-mail'][0]) > 0)
						echo '<span><img src="'.get_template_directory_uri().'/assets/img/IK_POCZTA.png"><a href="mailto:'.$meta['e-mail'][0].'"><span>'.$meta['e-mail'][0].'</span></a></span>';
					if (isset($meta['strona_internetowa']) && strlen($meta['strona_internetowa'][0]) > 0)
						echo '<span><img src="'.get_template_directory_uri().'/assets/img/IK_WWW.png"><a href="'.$meta['strona_internetowa'][0].'"><span>'.$meta['strona_internetowa'][0].'</span></a></span>';
					$post_content = strip_tags($post->post_content);
				?>
			</div>
			<div class="post_description">
				<?php
					if (isset($meta['krotki_opis']) && strlen($meta['krotki_opis'][0]) > 1)
						$post_content = strip_tags($meta['krotki_opis'][0]);
					else
						$post_content = strip_tags($post->post_content);
					echo $post_content;
				?>
			</div>
			<a class="see_more btn purple" href="<?php echo $post->guid; ?>">Zobacz więcej</a></div>
	</article><!-- #post-<?php the_ID(); ?> -->
</div>
