<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package webcost
 */

 $allow = array(1140,1119,661,1121,293,1116,1120,1117,1118,1122);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php webcost_post_thumbnail(); ?>
	<div class="entry-content <?php if (!in_array($post->ID,$allow)) : ?>page_wrapper<?php endif; ?>">
		<div class="container">
			<?php if (!in_array($post->ID,$allow)) : ?>
				<h1><?php the_title(); ?></h1>
			<?php endif; ?>
			<div class="description">
				<?php
					the_content();
				?>
			</div>				
		</div>
	</div><!-- .entry-content -->
</article>
