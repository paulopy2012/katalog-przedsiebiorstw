<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package webcost
 */
?>
<?php if ( 'firmy' !== get_post_type() && 'strona_tekstowa' !== get_post_type() ) : ?>
	<?php if( !is_archive() ) : ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class('single_post_page container'); ?>>

				<header class="entry-header ">
					<?php
						the_title( '<h1 class="entry-title">', '</h1>' );
					if ( 'post' === get_post_type() ) :
						?>
						<div class="entry-meta">
							<?php
							webcost_posted_on();
							echo "<span class='d-none d-lg-flex'> - </span>";
							the_category();
							// webcost_posted_by();
							?>
						</div><!-- .entry-meta -->
					<?php endif; ?>
				</header><!-- .entry-header -->

			<div class="entry-content">
				<?php
					$content = explode('. ', get_the_content());
					if (count($content) > 0)
						$content[count($content)/2] = get_the_post_thumbnail($post->ID, 'post_body_img') . $content[count($content)/2];

					$content = implode('. ', $content);

					echo $content;

				?>
			</div><!-- .entry-content -->

		</article><!-- #post-<?php the_ID(); ?> -->

		<div class="similar_posts">
			<div class="container">
				<h3 class='block_title'>Na naszym blogu przeczytasz również</h3>
				<?php
					// $categories = get_the_category();
					// if (!is_wp_error($categories) && count($categories) > 0) {
					// 	$ids = [];
					// 	foreach ($categories as $category)
					// 		$ids[] = $category->term_id;
					// }
					// cat="'.implode(',',$ids).'"
					echo do_shortcode( '[similar_posts exclude="637,621" ]' );
				?>
			</div>
		</div>

		<?php
			// do_shortcode( '[nearby_firm]' );
			echo "<div class='promo_block'>".do_shortcode( '[promo_block]' )."</div>";
		?>
	<?php else : ?>

	<?php endif; ?>
<?php endif; ?>



<?php if ( 'firmy' === get_post_type() ) : ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="container main-info">
		<div class="row">
			<div class="col-xs-12 col-lg-6 left-column">
				<?php webcost_post_thumbnail(); ?>
			</div>
			<div class="col-xs-12 col-lg-6 right-column">
				<?php
					$meta = get_post_meta(get_the_ID());

					$res = '';
					if (isset($meta['Adres']) && strlen($meta['Adres'][0]) > 0)
						$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_POLOZENIE.png"><span>'.addUrlToAddress($meta['Adres'][0]).'</span></span>';
					if (isset($meta['numer telefonu']) && strlen($meta['numer telefonu'][0]) > 0)
						$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_TEL.png"><a href="tel:'.$meta['numer telefonu'][0].'"><span>'.$meta['numer telefonu'][0].'</span></a></span>';
					if (isset($meta['e-mail']) && strlen($meta['e-mail'][0]) > 0)
						$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_POCZTA.png"><a href="mailto:'.$meta['e-mail'][0].'"><span>'.$meta['e-mail'][0].'</span></a></span>';
					if (isset($meta['strona_internetowa']) && strlen($meta['strona_internetowa'][0]) > 0)
						$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_WWW.png"><a href="'.$meta['strona_internetowa'][0].'"><span>'.$meta['strona_internetowa'][0].'</span></a></span>';

					$services = [];
					foreach ($meta as $km => $m) {
						if (strpos($km, '_slug') !== false && strpos($km, 'usluga') !== false && strlen($m[0]) > 0 )
							$services[preg_replace('/[^0-9.]+/', '', $km)]['slug'] = $m[0];
						if (strpos($km, '_opis_uslugi') !== false && strpos($km, '_usluga_') === false && strlen($m[0]) > 0 )
							$services[preg_replace('/[^0-9.]+/', '', $km)]['text'] = $m[0];
						if (strpos($km, '_obraz_uslugi') !== false && strpos($km, '_usluga_') === false && strlen($m[0]) > 0 )
							$services[preg_replace('/[^0-9.]+/', '', $km)]['image'] = wp_get_attachment_image_url( $m[0], 'medium' );
					}

					if ( is_singular() ) :
						the_title( '<h1 class="entry-title">', '</h1>' );
					else :
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
					endif;

					if (strlen($res) > 0) :
				?>
				<div class="entry-meta">
					<?php echo $res; ?>
				</div>
				<?php
					endif;
					echo "<div class='post_description'>";
					if (isset($meta['krotki_opis']) && strlen($meta['krotki_opis'][0]) > 1)
						echo strip_tags($meta['krotki_opis'][0]);
					else
						echo strip_tags($post->post_content);
					echo "</div>";
				?>
				<div class="button_container">
					<button class="btn purple-outline" data-toggle="modal" data-target="#sendEmail">Wyślij wiadomość</button>

					<div class="modal fade" id="sendEmail" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="sendEmailLabel" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<?php echo do_shortcode('[wpforms id="980" title="false"]') ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
				$tags = wp_get_post_tags( get_the_ID() );
				if (count($tags) > 0) {
					?>
						<div class="tags_block col-xs-12">
							<?php
								foreach ($tags as $tag)
									if ($tag->name != 'blog' && $tag->name != 'home_text' && $tag->name != 'paid' && $tag->name != 'promo')
										echo "<a href='".get_site_url()."?s=".$tag->name."&tag=true' class='badge'>".$tag->name."</a>";
							?>
						</div>
					<?php
				}
			?>
		</div>
	</div>
	<?php if (isset($meta['Adres'])) : ?>
		<div class="gmap">
			<iframe src="https://maps.google.com/maps?q=<?php echo $meta['Adres'][0]; ?>&t=&z=17&ie=UTF8&iwloc=&output=embed" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
		</div>
	<?php endif; ?>

	<div class="full_info container">
		<h3 class="block_title">Informacje</h3>
		<div class="full_info_content">
			<?php echo strip_tags($post->post_content); ?>
		</div>
	</div>

	<?php if (count($services) > 0) : ?>
		<div class="service_info container">
			<div class="row">
				<?php foreach ($services as $ks => $s) : ?>
					<article id="service_<?php echo $ks ?>" class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
						<a href="/usluga/<?php echo $s['slug']?>">
							<div class="img_container<?php if (!isset($s['image'])) echo ' no_image'; ?>">
								<img src="<?php if (isset($s['image'])) echo $s['image']; else echo get_template_directory_uri()."/assets/img/no_service_image_615x435.png"; ?>" alt="">
							</div>
							<span class="service_text">
								<?php echo $s['text']; ?>
							</span>
						</a>
					</article>
				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>

	<?php
		$categories = get_the_terms( get_the_ID(), 'category');
		$ids = [];
		$names = [];
		if ($categories && count($categories) > 0) {
			foreach ($categories as $category) {
				$ids[] = $category->term_id;
				$names[] = $category->name;
			}
		}
		if (count($ids) > 0) {
			echo do_shortcode( '[similar_firms exclude="'.get_the_ID().'" cat="'.implode(',',$ids).'"]' );
		}
	?>

	<?php
		if (count($names) > 0)
			echo do_shortcode( '[random_post cat="'.implode(',',$names).'"]' );
		// do_shortcode( '[nearby_firm]' );
		// echo "<div class='promo_block'>".do_shortcode( '[promo_block]' )."</div>";
	?>

</article><!-- #post-<?php the_ID(); ?> -->

<?php endif; ?>

<?php if ( 'strona_tekstowa' == get_post_type() ) : ?>
	<?php if( !is_archive() ) : ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class('single_post_page container'); ?>>

				<header class="entry-header">
					<?php
						the_title( '<h1 class="entry-title">', '</h1>' );
					if ( 'post' === get_post_type() ) :
						?>
						<div class="entry-meta">
							<?php
							webcost_posted_on();
							echo "<span class='d-none d-lg-flex'> - </span>";
							the_category();
							// webcost_posted_by();
							?>
						</div><!-- .entry-meta -->
					<?php endif; ?>
				</header><!-- .entry-header -->

			<div class="entry-content">
				<?php

					$content = explode('. ', get_the_content());
					if (count($content) > 0)
						$content[count($content)/2] = get_the_post_thumbnail($post->ID, 'post_body_img') . $content[count($content)/2];

					$content = implode('. ', $content);

					echo $content;

				?>
			</div><!-- .entry-content -->

		</article><!-- #post-<?php the_ID(); ?> -->

		<?php
			do_shortcode('[paid_firm numberposts="2" theme="white"]');
			// do_shortcode( '[nearby_firm]' );
			echo "<div class='promo_block'>".do_shortcode( '[promo_block]' )."</div>";
		?>
	<?php else : ?>

	<?php endif; ?>
<?php endif; ?>