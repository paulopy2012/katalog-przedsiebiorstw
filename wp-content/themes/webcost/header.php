<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package webcost
 */
if (is_front_page())
	checkRefer();
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Varta&display=swap" rel="stylesheet">

	<?php
		// get_location();
		wp_head();
	?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
		<header id="masthead" class="site-header">
			<div id="nameList" class="dropdown-menu" style="display:none">
				<div class="mobile"><input autocomplete="off" type="text" class="form-control" id="name_inner" name="s" placeholder="Znajdź firmę, branżę"><a class="close"></a></div>
				<div class="nameList"></div>
			</div>
			<div class="big-container adaptive_header">
				<nav id="site-navigation" class="navbar navbar-expand-lg navbar-light">
					<?php the_webcost_logo(); ?>
					<div style="display:flex;">
						<button class="mob_search_toggler" type="button" ><img src="/wp-content/themes/webcost/assets/img/search.svg" alt=""></button>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
						<div class="navbar-nav">
							<?php
								wp_nav_menu(
									array(
										'theme_location' => 'menu-1',
										'menu_id'        => 'primary-menu',
										'menu_class'      => 'left-navbar',
										'fallback_cb'     => false,
									)
								);
								wp_nav_menu(
									array(
										'theme_location'  => 'menu-2',
										'fallback_cb'     => false,
									)
								);
								wp_nav_menu(
									array(
										'theme_location'  => 'menu-3',
										'fallback_cb'     => false,
									)
								);
							?>
						</div>
					</div>
				</nav>
			</div>
			<div class="bottom-header"<?php if (UM()->options()->get( 'core_login' ) == get_queried_object_id() || UM()->options()->get( 'core_register' ) == get_queried_object_id()) echo "style='display:none'" ?>>
				<div class="big-container">
					<section id="search_panel" class="<?php if ( is_front_page() ) echo "home_page "; if (!is_user_logged_in()) echo "half_panel "; ?>">
						<?php if ( is_front_page() ) : ?>
							<ul class="letter-search<?php if (!is_user_logged_in()) : ?> half<?php endif; ?>" id="letter_search">
								<li><a href="/?s=&letter=A">A</a></li>
								<li><a href="/?s=&letter=B">B</a></li>
								<li><a href="/?s=&letter=C">C</a></li>
								<li><a href="/?s=&letter=D">D</a></li>
								<li><a href="/?s=&letter=E">E</a></li>
								<li><a href="/?s=&letter=F">F</a></li>
								<li><a href="/?s=&letter=G">G</a></li>
								<li><a href="/?s=&letter=H">H</a></li>
								<li><a href="/?s=&letter=I">I</a></li>
								<li><a href="/?s=&letter=J">J</a></li>
								<li><a href="/?s=&letter=K">K</a></li>
								<li><a href="/?s=&letter=L">L</a></li>
								<li><a href="/?s=&letter=M">M</a></li>
								<li><a href="/?s=&letter=N">N</a></li>
								<li><a href="/?s=&letter=O">O</a></li>
								<li><a href="/?s=&letter=P">P</a></li>
								<li><a href="/?s=&letter=Q">Q</a></li>
								<li><a href="/?s=&letter=R">R</a></li>
								<li><a href="/?s=&letter=S">S</a></li>
								<li><a href="/?s=&letter=T">T</a></li>
								<li><a href="/?s=&letter=U">U</a></li>
								<li><a href="/?s=&letter=V">V</a></li>
								<li><a href="/?s=&letter=W">W</a></li>
								<li><a href="/?s=&letter=X">X</a></li>
								<li><a href="/?s=&letter=Y">Y</a></li>
								<li><a href="/?s=&letter=Z">Z</a></li>
							</ul>

						<?php endif; ?>
						<?php if (is_category()) : $category = get_queried_object();?>
							<div class="category_search">
								<label><?php echo $category->name; ?></label>
								<form class="form-inline" action="/" >
								<input type="hidden" autocomplete="off" name="region" value="">
								<input type="hidden" class="form-control" name="s" id="name">
								<input type="hidden" name="post_type[]" value="firmy" />
								<input type="hidden" name="order" value="" />
									<div class="input-group dropdown">
										<input autocomplete="off" id="type_w" onkeyup="filterFunction('w',event)" name="" type="text" class="form-control" placeholder="WOJEWÓDZTWO" aria-label="WOJEWÓDZTWO" aria-describedby="basic-addon2">
										<div class="input-group-append" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										</div>
										<div class="dropdown-menu">
											<?php do_shortcode('[geo_list type="w"]') ?>
										</div>
									</div>

									<div class="input-group dropdown">
										<input autocomplete="off" id="type_m" onkeyup="filterFunction('m',event)" name="" type="text" class="form-control" placeholder="MIASTO" aria-label="MIASTO" aria-describedby="basic-addon3">
										<div class="input-group-append" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										</div>
										<div class="dropdown-menu">
										</div>
									</div>

									<div class="input-group dropdown">
										<input autocomplete="off" id="type_c" onkeyup="filterFunction('c',event)" name="c" type="text" class="form-control" placeholder="BRANŻA" aria-label="BRANŻA" aria-describedby="basic-addon4">
										<div class="input-group-append" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										</div>
										<div class="dropdown-menu">
											<?php echo do_shortcode('[firm_categories type="list"]'); ?>
										</div>
									</div>
									<div class="input-group">
										<input id="order_field" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="form-control" placeholder="SORTUJ A-Z" aria-label="SORTUJ A-Z" value="SORTUJ A-Z">
										<div class="dropdown-menu">
											<a class="dropdown-item order_item" href="#" data-name="name_asc">SORTUJ A-Z</a>
											<a class="dropdown-item order_item" href="#" data-name="name_desc">SORTUJ Z-A</a>
											<a class="dropdown-item order_item" href="#" data-name="date_desc">Najpierw nowy</a>
											<a class="dropdown-item order_item" href="#" data-name="date_asc">Najpierw stary</a>
										</div>
									</div>

								</form>
							</div>
						<?php endif; ?>
						<?php if ( !is_front_page() && !is_archive() ) : ?>
							<div class="breadcrumbs">
								<?php
									$breadcrumbs = '';
									$main = '';
									if (is_single()) {
										$cat = get_the_terms( $post->ID, 'category');
										$with_parent = -1;
										if ($cat) {
											$cat_ids = array();
											foreach ($cat as $c) {
												$cat_ids[] = $c->term_id;
												if ((int)$c->parent > 0 && $with_parent == -1)
													$with_parent = (int)$c->term_id;
											}
											if ($refer = wp_get_referer()) {
												$parse_url = parse_url($refer);
												if ($parse_url['host'] == $_SERVER['HTTP_HOST']) {
													$parse_url = explode('/', $parse_url['path']);

													if (strlen($parse_url[count($parse_url) - 1]) == 0)
														unset($parse_url[count($parse_url) - 1]);

													$ref_cat_id = get_cat_ID($parse_url[count($parse_url) - 1]);
													if (in_array($ref_cat_id, $cat_ids)) {
														foreach ($cat as $c) {
															if ((int)$c->parent == (int)$ref_cat_id)
																$ref_cat_id = (int)$c->term_id;
														}
														$breadcrumbs = get_category_parents($ref_cat_id, true, '');
													}
												}
											}
											if (strlen($breadcrumbs) == 0) {
												if ($with_parent != -1)
													$breadcrumbs = get_category_parents($with_parent, true, '');
												else
													$breadcrumbs = get_category_parents($cat[0]->term_id, true, '');
											}
										}

										if (is_singular('firmy')) {
											$firmy = get_page(253);
											if ($firmy)
												$main = '<a href="'.get_permalink(253).'">'.$firmy->post_title.'</a>';
										}
										if (is_singular('post')) {
											$firmy = get_page(255);
											if ($firmy)
												$main = '<a href="'.get_permalink(255).'">'.$firmy->post_title.'</a>';
										}
									} else {
										$page = get_queried_object();
										if ($page) {
											if ($args['woew_name']) {
												$main = '<a href="'.get_permalink($page->ID).'">Firmy w województwie '.$args['woew_name']->name.'</a>';
											} elseif ($args['city_name']) {
												$main = '<a href="'.get_permalink($page->ID).'">Firmy w mieście '.$args['city_name']->name.'</a>';
											} else {
												$main = '<a href="'.get_permalink($page->ID).'">'.$page->post_title.'</a>';
											}
										}
									}
									echo $main.$breadcrumbs;
								?>
							</div>
						<?php endif; ?>
						<div class="button-block">
							<?php if ( !is_front_page() ) :?><a id="header-search-btn" class="btn blue no_hover" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/search.svg" alt="">SZUKAJ</a><?php endif; ?>
							<?php /*if ( is_singular('firmy') || is_singular('strona_tekstowa') || is_category()  || is_home() ) :?><a id="header-search-btn" class="btn blue no_hover" href="#"><img src="<?php echo get_template_directory_uri();?>/assets/img/search.svg" alt="">SZUKAJ</a><?php endif; */?>
							<?php if (!is_user_logged_in()) : ?>
								<a id="login-btn" class="btn white" href="<?php echo get_permalink(UM()->options()->get('core_login')); ?>"><img src="<?php echo get_template_directory_uri();?>/assets/img/user.svg" alt="">ZALOGUJ SIĘ</a>
								<a id="reg-btn" class="btn white" href="<?php echo get_permalink(UM()->options()->get('core_register')); ?>"><img src="<?php echo get_template_directory_uri();?>/assets/img/user.svg" alt="">Rejestracja</a>
							<?php else: ?>
								<a id="user-btn" class="btn white" href="<?php echo get_permalink(UM()->options()->get('core_account')); ?>"><img src="<?php echo get_template_directory_uri();?>/assets/img/user.svg" alt="">KONTO</a>
							<?php endif; ?>
							<?php
								$page_id = '';
								$page_id = get_queried_object_id();
								if ($page_id >= 0 && $page_id != 661 && !have_firm()) {
							?>
								<a id="add-firm-btn" class="btn purple no_hover" href="<?php echo get_page_link(661); ?>"><img src="<?php echo get_template_directory_uri();?>/assets/img/plus.png" alt="">DODAJ FIRMĘ</a>
							<?php } ?>
						</div>
					</section>
					<?php /* if ( is_singular('firmy') || is_singular('strona_tekstowa') || is_home() ) :*/ ?>
					<?php if ( !is_front_page() && !is_category() ) :?>
						<div id="search_block" class="container">
							<form id="slider_search_form" class="form-inline" action="/">
							<input id="type_c" name="c" type="hidden" class="form-control" placeholder="BRANŻA">
							<div class="name_searcher input-group mb-2 mb-lg-0 mr-sm-2">
								<div class="input-group-prepend">
								<div class="input-group-text"><img src="/wp-content/themes/webcost/assets/img/search.svg"></div>
								</div>
								<input type="text" autocomplete="off" class="form-control" name="s" id="name" placeholder="Znajdź firmę, branżę">
							</div>

							<div id="place_search" class="place_searcher no-border input-group mb-2 mb-lg-0 mr-lg-2">
								<div class="input-group-prepend">
								<div class="input-group-text"><img src="/wp-content/themes/webcost/assets/img/place.svg"></div>
								</div>
								<div class="dropdown" style="flex-grow: 1">
									<input autocomplete="off" type="text" class="form-control" id="place" name="region" placeholder="miejscowość, powiat">
								</div>
							</div>
							<input type="hidden" name="post_type[]" value="firmy" />

							<button type="submit" class="blue btn btn-primary mb-2 mb-sm-0">SZUKAJ</button>
							</form>
					<?php
						do_shortcode( '[geo_list position="home"]' );
						endif;
					?>
				</div>
			</div>
			<div class="fix_header">
				<div class="big-container">
					<?php the_webcost_logo(); ?>
					<div id="fixed_search">
						<form id="fixed_search_form" class="form-inline" action="/">
							<input id="type_c" name="c" type="hidden" class="form-control" placeholder="BRANŻA">
							<div class="name_searcher input-group mb-2 mb-lg-0 mr-sm-2">
								<div class="input-group-prepend">
								<div class="input-group-text"><img src="/wp-content/themes/webcost/assets/img/search.svg"></div>
								</div>
								<input type="text" autocomplete="off" class="form-control" name="s" id="name" placeholder="Znajdź firmę, branżę">
							</div>

							<div id="place_search" class=" place_searcher no-border input-group mb-2 mb-lg-0 mr-lg-2">
								<div class="input-group-prepend">
								<div class="input-group-text"><img src="/wp-content/themes/webcost/assets/img/place.svg"></div>
								</div>
								<div class="dropdown" style="flex-grow: 1">
									<input autocomplete="off" type="text" class="form-control" id="place" name="region" placeholder="miejscowość, powiat">
								</div>
								<div id="input_search_block"></div>
							</div>
							<input type="hidden" name="post_type[]" value="firmy" />

							<button type="submit" class="blue btn btn-primary mb-2 mb-sm-0">SZUKAJ</button>
						</form>
					</div>
				</div>
			</div>
		</header>