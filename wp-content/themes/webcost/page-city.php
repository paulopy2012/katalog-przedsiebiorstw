<?php
$url = array_values(array_filter(explode('/', add_query_arg( NULL, NULL ))));
$city = isset($url[0]) ? $url[0] : '';
global $wpdb;
$db_name_woew = $wpdb->get_row('Select name from '.$wpdb->prefix.'geo_list where id_type = 1 and slug = "'.$city.'"');
$db_name_city = $wpdb->get_row('Select name from '.$wpdb->prefix.'geo_list where id_type = 3 and slug = "'.$city.'"');
if ($db_name_woew) {
    get_header('', ['woew_name' => $db_name_woew]);
} elseif ($db_name_city) {
    get_header('', ['city_name' => $db_name_city]);
}
?>

<main id="primary" class="site-main">
	<?php

        if ($city) {
            $city = str_replace('_','-',$city);
            $paged = array_search('page', $url) !== false && isset($url[array_search('page', $url) + 1]) ? (int)$url[array_search('page', $url) + 1] : 1;
            $args = array(
                'post_type' => 'firmy',
                'post_status' => 'publish',
                'paged' => $paged,
                'meta_query' => array('relation' => 'OR'),
            );

            $args['meta_query'][] = array(
                'key' => 'region_slug',
                'value' => ($db_name_woew ? $city.'-' : '-'.$city),
                'compare' => 'like'
            );

            $posts = new WP_Query( $args );
            if ( $posts->have_posts() ) :
                ?>
                <div id="firm_search_result">
                    <div class="container">
                        <header class="page-header">
                            <h1 class="page-title block_title">
                                <?php
                                    if ($db_name_woew)
                                        echo "Firmy w województwie ".$db_name_woew->name;
                                    elseif ($db_name_city)
                                        echo "Firmy w mieście ".$db_name_city->name;
                                    else
                                        echo "Firmy w mieście ".$city;
                                ?>
                            </h1>
                            <?php
                                $topText = get_field('gorne_pole', 1309);
                                if ($topText) :
                                    ?>
                                        <div class="w-100 p-3 mb-5"><?php echo $topText; ?></div>
                                    <?php
                                endif;
                            ?>
                        </header>
                        <div class="row">
                            <?php
                                foreach ( $posts->posts as $post ) :
                                    if (strlen($region_name) > 0) :
                                        if (trim(get_post_meta($post->ID, 'region_slug', true)) === $region_name) :
                                            get_template_part( 'template-parts/content', 'firm' );
                                        else :
                                            continue;
                                        endif;
                                    else :
                                        get_template_part( 'template-parts/content', 'firm' );
                                    endif;
                                endforeach;
                            ?>
                        </div>
                        <?php kama_pagenavi($before = '', $after = '', $echo = true, $args = array(), $wp_query = $posts); ?>
                        <?php
                            $bottomText = get_field('dolne_pole', 1309);
                            if ($bottomText) :
                                ?>
                                    <div class="w-100 p-3 mt-5"><?php echo $bottomText; ?></div>
                                <?php
                            endif;
                        ?>
                    </div>
                </div>
                <?php
            else:
                get_template_part( 'template-parts/content', 'none' );
            endif;
        }

	?>

</main>

<?php
get_footer();