<?php
/**
 * webcost functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package webcost
 */

global $woew_list;
global $city_list;

 if ( ! defined( '_S_VERSION' ) ) {
	define( '_S_VERSION', '1.0.0.6' );
}

if ( ! function_exists( 'webcost_setup' ) ) :

	function webcost_setup() {

		load_theme_textdomain( 'webcost', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );

		register_nav_menus(
			array(
				'menu-1' => 'revs',
				'menu-2' => 'leds',
				'social' => 'soc_links',
				'footer_bottom' => 'f_bottom',
				'footer_middle' => 'footer_widget',
				'menu-3' => 'mob_header_menu',
			)
		);

		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		add_theme_support(
			'custom-background',
			apply_filters(
				'webcost_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		add_theme_support( 'customize-selective-refresh-widgets' );

		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'webcost_setup' );

function webcost_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'webcost_content_width', 640 );
}
add_action( 'after_setup_theme', 'webcost_content_width', 0 );

function webcost_scripts() {
	wp_enqueue_style( 'webcost-bootstrap.min', get_template_directory_uri().'/assets/css/bootstrap.min.css' );
	wp_enqueue_style( 'webcost-style', get_template_directory_uri().'/assets/css/style.css');
	wp_style_add_data( 'webcost-style', 'rtl', 'replace' );
	if (is_page(661)){
		wp_enqueue_style('webcost-bootstrap-treeview.min', get_template_directory_uri().'/assets/css/bootstrap-treeview.min.css');
		wp_enqueue_style('webcost-tagsinput', get_template_directory_uri().'/assets/css/tagsinput.css');
	}


	wp_enqueue_script( 'webcost-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'webcost-jquery-3.5.1.min', get_template_directory_uri() . '/assets/js/jquery-3.5.1.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'webcost-bootstrap.bundle.min', get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'webcost-main', get_template_directory_uri() . '/assets/js/main.js', array(), _S_VERSION, true );
	if (is_page(661)){
		wp_enqueue_script('webcost-bootstrap-treeview.min', get_template_directory_uri().'/assets/js/bootstrap-treeview.min.js', array(), _S_VERSION, true );
		wp_enqueue_script('webcost-jquery.inputmask', get_template_directory_uri() . '/assets/js/jquery.inputmask.min.js', array(), _S_VERSION, true );
		wp_enqueue_script('webcost-tagsinput', get_template_directory_uri() . '/assets/js/tagsinput.js', array(), _S_VERSION, true );
	}

	// if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	// 	wp_enqueue_script( 'comment-reply' );
	// }
}
add_action( 'wp_enqueue_scripts', 'webcost_scripts' );

function webcost_admin_scripts($hook) {
	wp_enqueue_style( 'webcost-admin', get_template_directory_uri().'/assets/css/admin.css' );
    wp_enqueue_script('webcost-admin', get_template_directory_uri() . '/assets/js/admin.js');
    wp_enqueue_script('webcost-jquery.inputmask.min', get_template_directory_uri() . '/assets/js/jquery.inputmask.min.js');
}

add_action('admin_enqueue_scripts', 'webcost_admin_scripts');

require get_template_directory() . '/inc/template-tags.php';

if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'post_body_img', 660, 440 );
}

add_filter('xmlrpc_methods', function($methods) {
	unset($methods['pingback.ping']);
	return $methods;
});

function get_woocommerce_categories_shortcode($attr = array()) {

	$args = array(
		'taxonomy' => 'category',
		'orderby' => 'name',
		'order'   => 'ASC',
		'show_count'   => 0,
		'pad_counts'   => 0,
		'hierarchical' => 1,
		'title_li'     => '',
		'hide_empty'   => 0,
		'parent'       => 0,
		'exclude'	   => array(1)
	);

		if (isset($attr['number']))
			$args['number'] = $attr['number'];

		$all_categories = get_categories( $args );
	if (!isset($attr['type'])) {
		$res = '<div id="industry_search_block_content" class="container">';
		foreach ($all_categories as $cat) {
			if($cat->category_parent == 0) {
				$category_id = $cat->term_id;
				$image = get_template_directory_uri().'/assets/img/category/'.(int)$cat->term_id.'.svg';
				$res .= '<a href="'. get_term_link($cat->slug, 'category') .'"><div class="img_container"><img src="'.$image.'" alt="'.$cat->name.'"></div><span>'. $cat->name .'</span></a>';
			}
		}
		$res .= '</div>';
	} else {
		if ($attr['type'] == 'list') {
			$res = '';
			foreach ($all_categories as $cat) {
				if($cat->category_parent == 0) {
					$res .= '<a class="dropdown-item cat_item" href="#">'.mb_ucfirst($cat->name).'</a>';
				}
			}
		}
	}

  return $res;
}
add_shortcode('firm_categories', 'get_woocommerce_categories_shortcode');

function addUrlToAddress($address) {
	global $woew_list;
	global $city_list;
	$address = array_values(array_filter(explode(' ', $address)));
	$wIndex = 0;
	foreach ($address as $kitem => $item) {
		if ($kitem > count($address) - 4) {
			if ($wIndex) {
				$city = array_slice($address, $wIndex+1);
				if ($city) {
					$indexC = array_search(mb_strtolower(implode(' ',$city)), $city_list);
					if ($indexC) {
						$address[$kitem] = '<a class="ml-1" href="'.get_site_url().'/'.$indexC.'">'.implode(' ',$city).'</a>';
						$address = array_slice($address, 0, $kitem+1);
					}
				}
				break;
			}

			$indexW = array_search(mb_strtolower($item), $woew_list);
			if ($indexW) {
				$address[$kitem] = '<a class="ml-1" href="'.get_site_url().'/'.$indexW.'">'.$item.'</a>';
				$wIndex = $kitem;
			}
		}
	}
	return implode(' ', $address);
}

function get_random_paid_firm_shortcode($attr = array()) {
	global $wpdb;
	$nmb = 4;
	$c = '';
	$wc = '';
	if (isset($attr['category'])) {
		$c = 'JOIN '.$wpdb->prefix.'term_relationships t ON t.object_id = p.ID';
		$wc = 't.term_taxonomy_id IN ('.$attr['category'].') AND ';
	}
	if (isset($attr['numberposts']) && $attr['numberposts'] == 2) {
		$nmb = 3;
	}

	$posts = $wpdb->get_results("SELECT Distinct p.ID FROM ".$wpdb->prefix."pmpro_memberships_users pm
									JOIN ".$wpdb->prefix."usermeta um ON um.user_id = pm.user_id
									JOIN ".$wpdb->prefix."posts p ON p.post_author = pm.user_id
									".$c."
									WHERE
									".$wc."
									p.post_status='publish' and p.post_type = 'firmy' and um.meta_key = 'account_status' AND um.meta_value = 'approved' and pm.membership_id = 4 and pm.status = 'active' ORDER BY RAND() desc limit 0,".$nmb);

	if (is_array($posts))
		foreach ($posts as $ku=>$u)
			$posts[$ku] = $u->ID;
	$res = '';

	if (is_array($posts) && count($posts) > 0) {
		$args = array(
			'post_type' => 'firmy',
			'post_status' => 'publish',
			'orderby' => 'rand',
			'order'    => 'ASC',
			'include' => implode(',', $posts)
		);

		$in_line_md = 6;
		$in_line_xl = 4;
		if (isset($attr['numberposts'])) {
			if ($attr['numberposts'] = 2) {
				$in_line_md = 6;
				$in_line_xl = 6;
			}
		}

		if (isset($attr['exclude']))
			$args['exclude'] = $attr['exclude'];
		$posts = get_posts($args);
		if (count($posts) > 0) {
			$theme = '';
			if (isset($attr['theme']) && $attr['theme'] == 'white')
				$theme = ' white';

			foreach ($posts as $kpost=>$post) {
				$cat = get_the_terms( $post->ID, 'category');

				if (is_array($cat) && count($cat) > 0)
					if (isset($attr['category']))
						foreach ($cat as $ca) {
							if ($ca->term_id == $attr['category'])
								$cat = $ca;
						}
					else
						$cat = $cat[0];

				$meta = get_post_meta($post->ID);
				$img = get_the_post_thumbnail_url( $post->ID, 'medium' );
				$containerClass = '';
				if (!$img) {
					$img = get_template_directory_uri().'/assets/img/no_service_image_615x435.png';
					$containerClass = ' no-image';
				}
				$img_cat = file_exists(get_template_directory().'/assets/img/category/gray/'.(int)$cat->term_id.'.svg') ? get_template_directory_uri().'/assets/img/category/gray/'.(int)$cat->term_id.'.svg' : null;
				if ($kpost == 0) {
					$res = "<section class='paid_firm'><div class='container'><span class='block_title'>Wyróżnione firmy</span>";
					$meta = get_post_meta($post->ID);
					$services = array();
					if (count($meta)>0)
						foreach ($meta as $km=>$m)
							if (count($services) < 4)
								if (strpos($km, '_opis_uslugi') !== false && strpos($km, '_usluga_') === false && strlen($m[0]) > 0) {
									$services[] = array('text' => $m[0], 'slug' => get_post_meta( $post->ID, str_replace('_opis_uslugi','_slug',$km), true ));
								}

					$img = get_the_post_thumbnail_url( $post->ID, 'medium' );
					$containerClass = '';
					if (!$img) {
						$img = get_template_directory_uri().'/assets/img/no_service_image_615x435.png';
						$containerClass = ' no-image';
					}
					$res .= '<article id="paid_firm" class="row"><div class="col-md-6 col-xs-12 left-column"><div class="img_container'.$containerClass.'"><img src="'.$img.'" alt="'.$post->post_title.'"></div></div><div class="col-md-6 col-xs-12 right-column"><a href="'.get_permalink($post->ID).'"><h3 class="paid_firm_title">'.$post->post_title.'</h3></a><span>'.strip_tags($post->post_content).'</span><a class="btn purple-outline" href="'.get_permalink($post->ID).'">Zobacz więcej</a></div>';

					if (count($services) > 0) {
						$res .= '<div class="col-xs-12 services"><div class="row">';
						foreach ($services as $s)
							$res .= '<div class="col-xs-12 col-md-6 service">
										<a href="/usluga/'.$s['slug'].'" alt="'.$s['text'].'"><label>'.$s['text'].'</label></a>
									</div>';
						$res .= '</div></div>';
					}

					$res .= '</article>';
					$res .= '</div></section>';
				} else {
					if ($kpost == 1)
						$res .= "<section class='paid_firm_three'><div class='container'><div id='paid_firm_three' class='row".$theme."'>";

					$res .= '<div class="col-md-6 col-xl-4 col-xs-12 post_wrapper"><article class="post_block"><div class="img_container '.$containerClass.'"><img src="'.$img.'" alt="'.$post->post_title.'"></div><div class="post-content"><div class="post-title"><a href="'.get_permalink($post->ID).'"><h3>'.$post->post_title.'</h3></a></div><div class="post_category">'.($img_cat ? '<img src="'.$img_cat.'" alt="'.$cat->slug.'">' : '').$cat->name.'</div><div class="post_metas">';

					if (isset($meta['Adres']) && strlen($meta['Adres'][0]) > 0)
						$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_POLOZENIE.png"><span>'.addUrlToAddress($meta['Adres'][0]).'</span></span>';
					if (isset($meta['numer telefonu']) && strlen($meta['numer telefonu'][0]) > 0)
						$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_TEL.png"><a href="tel:'.$meta['numer telefonu'][0].'"><span>'.$meta['numer telefonu'][0].'</span></a></span>';
					if (isset($meta['e-mail']) && strlen($meta['e-mail'][0]) > 0)
						$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_POCZTA.png"><a href="mailto:'.$meta['e-mail'][0].'"><span>'.$meta['e-mail'][0].'</span></a></span>';
					if (isset($meta['strona_internetowa']) && strlen($meta['strona_internetowa'][0]) > 0)
						$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_WWW.png"><a href="'.$meta['strona_internetowa'][0].'"><span>'.$meta['strona_internetowa'][0].'</span></a></span>';
					if (isset($meta['krotki_opis']) && strlen($meta['krotki_opis'][0]) > 1)
						$post_content = strip_tags($meta['krotki_opis'][0]);
					else
						$post_content = strip_tags($post->post_content);

					$res .= '</div><div class="post_description">'.$post_content.'</div><a class="see_more btn purple" href="'.get_permalink($post->ID).'">Zobacz więcej</a></div></article></div>';

					if ($kpost == count($posts) - 1)
						$res .= '</div></div></section>';
				}
			}
		}
	}
  echo $res;
}
add_shortcode('paid_firm', 'get_random_paid_firm_shortcode');

// function get_location() {
// 	if (isset($_COOKIE['place']) && $_COOKIE['place'] != 'not_accept') {
// 		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
// 			$ip = $_SERVER['HTTP_CLIENT_IP'];
// 		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
// 			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
// 		} else {
// 			$ip = $_SERVER['REMOTE_ADDR'];
// 		}

// 		if ($ip == '127.0.0.1')
// 			$ip = '5.173.152.95';
// 			// $ip = '195.164.135.154';

// 		$url = "http://ip-api.com/json/".$ip."?fields=status,zip,countryCode";
// 		$ch = curl_init();
// 		curl_setopt($ch, CURLOPT_URL, $url);
// 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// 		$output = json_decode(curl_exec($ch),true);
// 		curl_close($ch);
// 		if (is_array($output) && isset($output['status']) && $output['status'] == 'success' && isset($output['countryCode']) && $output['countryCode'] == 'PL' && isset($output['zip'])) {
// 			if ($_COOKIE['place'] != $output['zip']) {
// 				$zip_base = explode("\n",file_get_contents($_SERVER['DOCUMENT_ROOT'].'/kody.csv'));
// 				if (is_array($zip_base) && count($zip_base) > 0) {
// 					foreach ($zip_base as $zip) {
// 						if (strpos($zip,$output['zip']) !== false) {
// 							setcookie("place", $output['zip']);
// 							return explode(';',$zip);
// 							break;
// 						}
// 					}
// 				}
// 			}
// 		}
// 	}
// 	return false;
// }

function have_firm() {
	$cuser = wp_get_current_user();
	if (!is_null($cuser->data->ID)) {
		$args = array(
			'post_type' => 'firmy',
			'post_status' => 'publish',
			'orderby'     => 'date',
			'order'       => 'DESC',
			'posts_per_page' => '1',
			'author' => $cuser->data->ID
		);

		$posts = new WP_Query( $args );
		if (count($posts->posts) != 0)
			return true;
	}
	return false;
}

// function get_nearby_firm_shortcode($attr = array()) {

// 	$res = '';
// 	if ($location = get_location()) {
// 		$args = array(
// 			'post_type' => 'firmy',
// 			'post_status' => 'publish',
// 			'orderby'     => 'date',
// 			'order'       => 'DESC',
// 			'posts_per_page' => '8',
// 			'meta_query' => array('relation' => 'and'),
// 		);

// 		$args['meta_query'][] = array(
// 			'key' => 'region',
// 			'value' => $location[1],
// 			'compare' => 'like'
// 		);
// 		$args['meta_query'][] = array(
// 			'key' => 'region',
// 			'value' => $location[2],
// 			'compare' => 'like'
// 		);

// 		$posts = new WP_Query( $args );
// 		if (count($posts->posts) < 8) {
// 			$args = array(
// 				'post_type' => 'firmy',
// 				'post_status' => 'publish',
// 				'orderby'     => 'date',
// 				'order'       => 'DESC',
// 				'posts_per_page' => '8',
// 				'meta_query' => array('relation' => 'and'),
// 			);

// 			$args['meta_query'][] = array(
// 				'key' => 'region',
// 				'value' => $location[2],
// 				'compare' => 'like'
// 			);
// 			$posts = new WP_Query( $args );
// 			if (count($posts->posts) > 0) {
// 				$res = "<div class='nearby_firms'><h3 class='block_title'>Polecane firmy z Twojego miasta</h3>";
// 				$res .= '<div class="container"><div id="nearby_firm_grid" class="row">';
// 				foreach ($posts->posts as $post) {
// 					$meta = get_post_meta($post->ID);

// 					$img = get_the_post_thumbnail_url( $post->ID, 'medium' );
// 					if (!$img) {
// 						$img = get_template_directory_uri().'/assets/img/no_service_image_615x435.png';
// 					}

// 					$services = '';
// 					foreach ($meta as $km=>$m)
// 						if (strlen($services) == 0)
// 							if (strpos($km, '_opis_uslugi') !== false && strpos($km, '_usluga_') === false && strlen($m[0]) > 0) {
// 								$services = $m[0];
// 								break;
// 							}

// 					if (isset($meta['Adres']) && strlen($meta['Adres'][0]) > 0)
// 						$address = $meta['Adres'][0];
// 					else
// 						$address = '';
// 					if (isset($meta['numer telefonu']) && strlen($meta['numer telefonu'][0]) > 0)
// 						$phone = 'tel. '.$meta['numer telefonu'][0];
// 					else
// 						$phone = '';
// 					if (isset($meta['region']) && strlen($meta['region'][0]) > 0)
// 						$city = explode(',',$meta['region'][0])[0];
// 					else
// 						$city = '';

// 					$res .= '
// 					<div class="col-xs-12 col-sm-6 col-xl-3 post_block_wrapper">
// 						<a href="'.get_permalink($post->ID).'" alt="'.$post->post_title.'">
// 							<div class="post_block_item">
// 								<img src="'.$img.'" alt="'.$post->post_title.'">
// 								<div class="post_block_content">
// 									<h3 class="city">'.$city.'</h3>
// 									<span>'.$post->post_title.'</span>
// 									<span>'.$services.'</span>
// 									<span>'.$address.'</span>
// 									<span>'.$phone.'</span>
// 								</div>
// 							</div>
// 						</a>
// 					</div>';
// 				}
// 				$res .= '</div></div>';
// 				$res .= "</div>";
// 			}
// 		}
// 	}
// 	echo $res;

// }
// add_shortcode('nearby_firm', 'get_nearby_firm_shortcode');

function get_last_firm_shortcode($attr = array()) {
	$args = array(
		'post_type' => 'firmy',
    	'post_status' => 'publish',
		'orderby'     => 'date',
		'order'       => 'DESC',
	);

	$res = '';

		$args['numberposts'] = 3;
		if (isset($attr['exclude']))
			$args['exclude'] = $attr['exclude'];

		$posts = get_posts($args);
		if (count($posts) > 0) {
			$res .= '<div class="container"><div id="last_firm_three" class="row">';
			foreach ($posts as $post) {
				$cat = get_the_terms( $post->ID, 'category');
				if ($cat != null && count($cat) > 0)
					$cat = $cat[0];
				$meta = get_post_meta($post->ID);
				$img = get_the_post_thumbnail_url( $post->ID, 'medium' );
				$containerClass = '';
				if (!$img) {
					$img = get_template_directory_uri().'/assets/img/no_service_image_615x435.png';
					$containerClass = ' no-image';
				}
				$img_cat = file_exists(get_template_directory().'/assets/img/category/gray/'.(int)$cat->term_id.'.svg') ? get_template_directory_uri().'/assets/img/category/gray/'.(int)$cat->term_id.'.svg' : null;
				$res .= '<div class="col-md-6 col-xl-4 col-xs-12"><article class="post_block"><div class="img_container'.$containerClass.'"><img src="'.$img.'" alt="'.$post->post_title.'"></div><div class="post-content"><div class="post-title"><a href="'.get_permalink($post->ID).'"><h3>'.$post->post_title.'</h3></a></div><div class="post_category">'.($img_cat ? '<img src="'.$img_cat.'" alt="'.$cat->slug.'">' : '').$cat->name.'</div><div class="post_metas">';

				if (isset($meta['Adres']) && strlen($meta['Adres'][0]) > 0)
					$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_POLOZENIE.png"><span>'.addUrlToAddress($meta['Adres'][0]).'</span></span>';
				if (isset($meta['numer telefonu']) && strlen($meta['numer telefonu'][0]) > 0)
					$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_TEL.png"><a href="tel:'.$meta['numer telefonu'][0].'"><span>'.$meta['numer telefonu'][0].'</span></a></span>';
				if (isset($meta['e-mail']) && strlen($meta['e-mail'][0]) > 0)
					$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_POCZTA.png"><a href="mailto:'.$meta['e-mail'][0].'"><span>'.$meta['e-mail'][0].'</span></a></span>';
				if (isset($meta['strona_internetowa']) && strlen($meta['strona_internetowa'][0]) > 0)
					$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_WWW.png"><a href="'.$meta['strona_internetowa'][0].'"><span>'.$meta['strona_internetowa'][0].'</span></a></span>';

				if (isset($meta['krotki_opis']) && strlen($meta['krotki_opis'][0]) > 1)
					$post_content = strip_tags($meta['krotki_opis'][0]);
				else
					$post_content = strip_tags($post->post_content);

				$res .= '</div><div class="post_description">'.$post_content.'</div><a class="see_more btn purple" href="'.get_permalink($post->ID).'">Zobacz więcej</a></div></article></div>';
			}
			$res .= '</div></div>';
		}

  return $res;
}
add_shortcode('last_firm', 'get_last_firm_shortcode');

function get_similar_firms_shortcode($attr = array()) {
	$args = array(
		'post_type' => 'firmy',
    	'post_status' => 'publish',
		'orderby'     => 'date',
		'order'       => 'DESC',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field' => 'term_id',
                'terms' => explode(',',$attr['cat']),
            )
        )
	);

	$res = '';

		$args['numberposts'] = 9;
		if (isset($attr['exclude']))
			$args['exclude'] = $attr['exclude'];

		$posts = get_posts($args);
		if (count($posts) > 0) {
			$res .= '<div class="similar_firms"><div class="container"><h3 class="block_title">Podobne firmy</h3><div id="similar_firms" class="row">';
			foreach ($posts as $post) {
				$cat = get_the_terms( $post->ID, 'category');
				if (count($cat) > 0)
					$cat = $cat[0];
				$meta = get_post_meta($post->ID);
				$img = get_the_post_thumbnail_url( $post->ID, 'medium' );
				$containerClass = '';
				if (!$img) {
					$containerClass = ' no-image';
					$img = get_template_directory_uri().'/assets/img/no_service_image_615x435.png';
				}
				$img_cat = file_exists(get_template_directory().'/assets/img/category/gray/'.(int)$cat->term_id.'.svg') ? get_template_directory_uri().'/assets/img/category/gray/'.(int)$cat->term_id.'.svg' : null;
				$res .= '<div class="col-md-6 col-xl-4 col-xs-12"><article class="post_block"><div class="img_container'.$containerClass.'"><img src="'.$img.'" alt="'.$post->post_title.'"></div><div class="post-content"><div class="post-title"><a href="'.get_permalink($post->ID).'"><h3>'.$post->post_title.'</h3></a></div><div class="post_category">'.($img_cat ? '<img src="'.$img_cat.'" alt="'.$cat->slug.'">' : '').$cat->name.'</div><div class="post_metas">';

				if (isset($meta['Adres']) && strlen($meta['Adres'][0]) > 0)
					$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_POLOZENIE.png"><span>'.addUrlToAddress($meta['Adres'][0]).'</span></span>';
				if (isset($meta['numer telefonu']) && strlen($meta['numer telefonu'][0]) > 0)
					$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_TEL.png"><a href="tel:'.$meta['numer telefonu'][0].'"><span>'.$meta['numer telefonu'][0].'</span></a></span>';
				if (isset($meta['e-mail']) && strlen($meta['e-mail'][0]) > 0)
					$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_POCZTA.png"><a href="mailto:'.$meta['e-mail'][0].'"><span>'.$meta['e-mail'][0].'</span></a></span>';
				if (isset($meta['strona_internetowa']) && strlen($meta['strona_internetowa'][0]) > 0)
					$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_WWW.png"><a href="'.$meta['strona_internetowa'][0].'"><span>'.$meta['strona_internetowa'][0].'</span></a></span>';
				if (isset($meta['krotki_opis']) && strlen($meta['krotki_opis'][0]) > 1)
					$post_content = strip_tags($meta['krotki_opis'][0]);
				else
					$post_content = strip_tags($post->post_content);

				$res .= '</div><div class="post_description">'.$post_content.'</div><a class="see_more btn purple" href="'.get_permalink($post->ID).'">Zobacz więcej</a></div></article></div>';
			}
			$res .= '</div></div></div>';
		}

  return $res;
}
add_shortcode('similar_firms', 'get_similar_firms_shortcode');

function get_all_firms_shortcode($attr = array()) {
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

	$args = array(
		'post_type' => 'firmy',
    	'post_status' => 'publish',
		'orderby'     => 'name',
		'order'       => 'ASC',
		'paged' => $paged,
		// 'posts_per_page' => get_option('posts_per_page')
	);

	$res = '';

		if (isset($attr['exclude']))
			$args['exclude'] = $attr['exclude'];

		if (isset($attr['category'])) {
			// $args['category'] = $attr['category'];
			$args['cat'] = $attr['category'];
		}

		// $posts = get_posts($args);
		add_filter( 'posts_join', 'join_pmpro_user_table', 10, 2);
		add_filter( 'posts_orderby', 'orderby_package', 10, 2 );
		add_filter( 'posts_where', 'active_user_packages', 10, 2 );
		$posts = new WP_Query( $args );
		remove_filter( 'posts_join', 'join_pmpro_user_table');
		remove_filter( 'posts_orderby', 'orderby_package');
		remove_filter( 'posts_where', 'active_user_packages');

		if (count($posts->posts) > 0) {
			$res .= '<div id="all_firms" class="row">';
			foreach ($posts->posts as $post) {
				$cat = get_the_terms( $post->ID, 'category');
				if ($cat != null && count($cat) > 0)
					$cat = $cat[0];
				$meta = get_post_meta($post->ID);
				$img = get_the_post_thumbnail_url( $post->ID, 'medium' );
				$containerClass = '';
				if (!$img) {
					$img = get_template_directory_uri().'/assets/img/no_service_image_615x435.png';
					$containerClass = ' no-image';
				}
				$img_cat = file_exists(get_template_directory().'/assets/img/category/gray/'.(int)$cat->term_id.'.svg') ? get_template_directory_uri().'/assets/img/category/gray/'.(int)$cat->term_id.'.svg' : null;
				$res .= '<div class="col-md-6 col-xl-4 col-xs-12 post_wrapper"><article class="post_block"><div class="img_container'.$containerClass.'"><img src="'.$img.'" alt="'.$post->post_title.'"></div><div class="post-content"><div class="post-title"><a href="'.get_permalink($post->ID).'"><h3>'.$post->post_title.'</h3></a></div><div class="post_category">'.($img_cat ? '<img src="'.$img_cat.'" alt="'.$cat->slug.'">' : '').$cat->name.'</div><div class="post_metas">';

				if (isset($meta['Adres']) && strlen($meta['Adres'][0]) > 0)
					$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_POLOZENIE.png"><span>'.addUrlToAddress($meta['Adres'][0]).'</span></span>';
				if (isset($meta['numer telefonu']) && strlen($meta['numer telefonu'][0]) > 0)
					$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_TEL.png"><a href="tel:'.$meta['numer telefonu'][0].'"><span>'.$meta['numer telefonu'][0].'</span></a></span>';
				if (isset($meta['e-mail']) && strlen($meta['e-mail'][0]) > 0)
					$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_POCZTA.png"><a href="mailto:'.$meta['e-mail'][0].'"><span>'.$meta['e-mail'][0].'</span></a></span>';
				if (isset($meta['strona_internetowa']) && strlen($meta['strona_internetowa'][0]) > 0)
					$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_WWW.png"><a href="'.$meta['strona_internetowa'][0].'"><span>'.$meta['strona_internetowa'][0].'</span></a></span>';
				if (isset($meta['krotki_opis']) && strlen($meta['krotki_opis'][0]) > 1)
					$post_content = strip_tags($meta['krotki_opis'][0]);
				else
					$post_content = strip_tags($post->post_content);

				$res .= '</div><div class="post_description">'.$post_content.'</div><a class="see_more btn purple" href="'.get_permalink($post->ID).'">Zobacz więcej</a></div></article></div>';
			}
			if (isset($attr['category']) && $posts->found_posts != $paged) {
				$res .= '<div id="see_more_block" class="col-xs-12"><a class="btn purple-outline see_more" href="'.get_pagenum_link( $paged + 1 ).'">Pokaż więcej</a><span id="total_posts_found" class="d-none">'.$posts->found_posts.'</span><span id="current_page" class="d-none">'.$paged.'</span></div>';
			}
			$res .= '</div>';
		}


  	echo $res;
  	if (!isset($attr['category']))
  		kama_pagenavi($before = '', $after = '', $echo = true, $args = array(), $wp_query = $posts);
}
add_shortcode('all_firms', 'get_all_firms_shortcode');

function fix_category_pagination($qs){
    if(isset($qs['category_name']) && isset($qs['paged'])){
        $qs['post_type'] = array('firmy');
    }
    return $qs;
}
add_filter('request', 'fix_category_pagination');

function get_similar_posts_shortcode($attr = array()) {
	$args = array(
		'post_type' => 'post',
    	'post_status' => 'publish',
		'orderby' => 'rand',
		'order'    => 'ASC',
	);

	$res = '';

		$args['numberposts'] = 6;
		if (isset($attr['exclude']))
			$args['exclude'] = $attr['exclude'];

		$posts = get_posts($args);
		if (count($posts) > 0) {
			$res .= '<div id="similar_posts" class="row">';
			foreach ($posts as $post) {

				$cat = get_the_category($post->ID);
				if (count($cat) > 0)
					$cat = $cat[0];

				$post_date = explode(' ',$post->post_date)[0];
				$post_date = explode('-', $post_date);
				$post_date = $post_date[2].'.'.$post_date[1].'.'.$post_date[0];
				$img_cat = file_exists(get_template_directory().'/assets/img/category/gray/'.(int)$cat->term_id.'.svg') ? get_template_directory_uri().'/assets/img/category/gray/'.(int)$cat->term_id.'.svg' : null;
				$res .= '<div class="col-sm-6 col-xl-4 col-xs-12 post_mini_block_wrapper">
							<article class="post_mini_block">
								<div class="post_category">
									<div class="img_container">'.($img_cat ? '<img src="'.$img_cat.'" alt="'.$cat->slug.'">' : '').'</div>'.$cat->name.'
								</div>
								<div class="post-title"><a href="'.get_permalink($post->ID).'"><h3>'.$post->post_title.'</h3></a></div>
								<div class="post-on">'.$post_date.'</div>
								<a class="see_more btn purple-outline" href="'.get_permalink($post->ID).'">Czytaj  więcej</a>
							</article>
						</div>';
			}
			$res .= '</div>';
		}
  return $res;
}
add_shortcode('similar_posts', 'get_similar_posts_shortcode');

function get_all_posts_shortcode($attr = array()) {

	$args = array(
    	'post_status' => 'publish',
		'orderby'     => 'date',
		'order'       => 'DESC',
		'numberposts' => get_option('posts_per_page')
	);
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$args['paged'] = $paged;

	$res = '';

		if (isset($attr['exclude']))
			$args['exclude'] = $attr['exclude'];

		$posts = get_posts($args);
		if (count($posts) > 0) {
			$res .= '<div class="container"><div id="all_posts" class="row">';
			foreach ($posts as $pk=>$post) {
				$img = get_the_post_thumbnail_url( $post->ID, 'post_body_img' );

				$res .= '<div class="col-xs-12"><div class="article_block"><div class="post-content col-xs-12 col-sm-6"><h3>'.$post->post_title.'</h3>';

				$post_content = strip_tags($post->post_content);

				$res .= '<div class="post_description">'.$post_content.'</div><div style="flex-grow: 1;display: flex;align-items: flex-end;"><a class="see_more btn purple-outline" href="'.get_permalink($post->ID).'">Czytaj więcej</a></div></div><div class="img_container col-xs-12 col-sm-6"><img src="'.$img.'" alt="'.$post->post_title.'"></div></div></div>';
			}
			$res .= '</div></div>';
		}


  return $res;
}
add_shortcode('all_posts', 'get_all_posts_shortcode');

function get_last_blog_shortcode($attr = array()) {

	$args = array(
    	'post_status' => 'publish',
    	'exclude'	  => array(637,621),
		'orderby'     => 'date',
		'order'       => 'DESC',
	);

	$res = '';

		$args['numberposts'] = 2;
		if (isset($attr['exclude']))
			$args['exclude'] = $attr['exclude'];
		if (isset($attr['cat']))
			$args['category'] = $attr['cat'];

		$posts = get_posts($args);


		if (count($posts) > 0) {
			if (!isset($attr['cat'])) {
				$res .= '<div class="container"><div id="last_blog_article" class="row"><h3 class="block_title">';
				$res .= 'Najnowsze artykuły';
				$res .=  '</h3>';
				foreach ($posts as $pk=>$post) {
					$img = get_the_post_thumbnail_url( $post->ID, 'post_body_img' );

					$res .= '<div class="col-xs-12"><div class="article_block"><div class="post-content col-xs-12 col-md-6"><h3>'.$post->post_title.'</h3>';

					$post_content = strip_tags($post->post_content);

					$res .= '<div class="post_description">'.$post_content.'</div><div style="flex-grow: 1;display: flex;align-items: flex-end;"><a class="see_more btn purple-outline" href="'.get_permalink($post->ID).'">Czytaj więcej</a></div></div><div class="img_container col-xs-12 col-md-6"><img src="'.$img.'" alt="'.$post->post_title.'"></div></div></div>';
				}
				$res .= '</div></div>';
			} else {
				$res .= '<div id="last_blog_article"><h3 class="block_title">Najnowsze artykuły w branży</h3>';
				foreach ($posts as $pk=>$post) {
					$gr = '';
					if ($pk % 2 == 1)
						$gr = 'gray ';

					$img = get_the_post_thumbnail_url( $post->ID, 'post_body_img' );
					$post_date = explode(' ',$post->post_date)[0];
					$post_date = explode('-', $post_date);
					$post_date = $post_date[2].'.'.$post_date[1].'.'.$post_date[0];

					$content = strip_tags($post->post_content);
					if (strlen($content) > 150)
						$content = substr($content, 0, 150)."...";

					$res .= '<article class="'.$gr.'category_post">
								<div class="container">
									<div class="row">
										<div class="col-xs-12">
											<header class="row category_post_header">
												<div class="col-xs-12 col-md-6 left-column">
													<div class="header_desc">
														<h3>'.$post->post_title.'</h3>
														<span class="posted-on">'.$post_date.'</span>
													</div>
												</div>
												<div class="col-xs-12 col-md-6 right-column">
													<img src="'.$img.'" alt="'.$post->post_title.'">
												</div>
											</header>
										</div>
										<div class="col-xs-12">
											<div class="post_description">'.$content.'</div>
											<a class="see_more btn purple-outline" href="'.get_permalink($post->ID).'">Czytaj więcej</a>
										</div>
									</div>
								</div>
							</article>';
				}
				$res .= '</div>';
			}
		}


  return $res;
}
add_shortcode('last_blog', 'get_last_blog_shortcode');

function get_random_blog_shortcode($attr = array()) {
	$args = array(
    	'post_status' => 'publish',
    	'exclude'	  => array(637,621),
		'orderby'     => 'date',
		'order'       => 'DESC',
		'numberposts' => 1
	);

	$res = '';

		if (isset($attr['cat']) && $attr['cat'] != '') {
			$args['category_name'] = $attr['cat'];
		}

		$posts = get_posts($args);

		if (count($posts) > 0) {
			$res .= '<div class="random_post"><h3 class="block_title">Najnowsze artykuły w branży</h3><div class="container"><div id="random_blog_article"><div  class="row">';
			foreach ($posts as $pk=>$post) {
				$img = get_the_post_thumbnail_url( $post->ID, 'post_body_img' );

				$cat = get_the_category($post->ID);
				if (count($cat) > 0)
					$cat = $cat[0];

				$img_cat = file_exists(get_template_directory().'/assets/img/category/'.(int)$cat->term_id.'.svg') ? get_template_directory_uri().'/assets/img/category/'.(int)$cat->term_id.'.svg' : null;
				$post_content = strip_tags($post->post_content);
				if (strlen($post_content) > 150)
					$post_content = substr($post_content, 0, 150)."...";

				$post_date = explode(' ',$post->post_date)[0];
				$post_date = explode('-', $post_date);
				$post_date = $post_date[2].'.'.$post_date[1].'.'.$post_date[0];

				$res .= '<article class="col-xs-12 col-md-6 content">
							<div class="category_info">
								<div class="img_container">'.
									($img_cat ? '<img src="'.$img_cat.'" alt="'.$cat->name.'">' : '')
								.'</div>
								'.$cat->name.'
							</div>
							<div class="entry-content">
								<h3>'.$post->post_title.'</h3>
								<div class="post_content">
									'.$post_content.'
								</div>
								<span class="posted-on">'.$post_date.'</span>
							</div>
							<div style="flex-grow: 1;display: flex;align-items: flex-end;"><a class="see_more btn white-outline" href="'.get_permalink($post->ID).'">Czytaj więcej</a></div>
						</article>
						<div class="col-xs-12 col-md-6 pl-md-0">
							<img class="thumb" src="'.$img.'" alt="'.$post->post_title.'" width="614" height="362">
						</div>
				';

			}
			$res .= '</div></div></div></div>';
		}


  return $res;
}
add_shortcode('random_post', 'get_random_blog_shortcode');

function get_promo_block_shortcode($attr = array()) {
	$args = array(
    	'post_status' => 'publish',
    	'tag' => array('promo'),
		'orderby'     => 'date',
		'order'       => 'DESC'
	);

	$res = '';

		$args['numberposts'] = 1;
		if (isset($attr['exclude']))
			$args['exclude'] = $attr['exclude'];

		$posts = get_posts($args);

		if (count($posts) > 0) {
			$res .= '<div class="container"><div id="promo_block" class="row">';
			foreach ($posts as $pk=>$post) {
				$img = get_the_post_thumbnail_url( $post->ID, 'large' );

				$res .= '<div class="col-xs-12"><div class="article_block row"><div class="post-content col-xs-12 col-lg-6"><h3>'.$post->post_title.'</h3>';

				$post_content = strip_tags($post->post_content);


				$res .= '<div class="post_description">'.$post_content.'</div><a class="see_more btn purple" href="'.get_permalink(257).'">Zacznij promocję</a></div><div class="img_container col-xs-12 col-lg-6"><img src="'.$img.'" alt="'.$post->post_title.'"></div></div></div>';
			}
			$res .= '</div></div>';
		}


  return $res;
}
add_shortcode('promo_block', 'get_promo_block_shortcode');

function get_home_text_block_shortcode($attr = array()) {

	$res = '';
	if (isset($attr['cat'])) {
		$title = get_term_meta($attr['cat'], '_optimtitle', true);
		$desc = get_term_meta($attr['cat'], '_optimtext', true);
	} else {
		$title = get_field('tekst_optymalizacyjny_naglowek',get_the_ID());
		$desc = get_field('tekst_optymalizacyjny_tekst',get_the_ID());
	}

	if (!is_null($title) && !is_null($desc) && strlen($title) > 0 && strlen($desc) > 0) {
		$res = '<section class="text_block"><div class="container"><div id="home_text_block" class="row">';
		$res .= '<div class="col-xs-12"><div class="article_block"><div class="post-content"><h3>'.$title.'</h3>';
		$res .= '<div class="post_description">'.$desc.'</div></div></div></div>';
		$res .= '</div></div></section>';
	}

  echo $res;
}
add_shortcode('home_text', 'get_home_text_block_shortcode');

function get_firm_categories_shortcode($attr = array()) {

	$args = array(
		'taxonomy' => 'category',
		'orderby' => 'name',
		'order'   => 'ASC',
		'show_count'   => 0,
		'pad_counts'   => 0,
		'hierarchical' => 1,
		'title_li'     => '',
		'hide_empty'   => 0,
		'parent'       => 0,
		'exclude'	   => array(1)
	);

	// if (isset($attr['number']))
	// 	$args['number'] = $attr['number'];

	$res = '<ul>';

	$all_categories = get_categories( $args );
	$last_index5 = count($all_categories) - count($all_categories) % 5;
	if ($last_index5 == count($all_categories))
		$last_index5 = count($all_categories) - 5;

	$last_index4 = count($all_categories) - count($all_categories) % 4;
	if ($last_index4 == count($all_categories))
		$last_index4 = count($all_categories) - 4;

	$last_index3 = count($all_categories) - count($all_categories) % 3;
	if ($last_index3 == count($all_categories))
		$last_index3 = count($all_categories) - 3;

	foreach ($all_categories as $kcat => $cat) {
		if($cat->category_parent == 0) {
			$last_line4 = '';
			if (($kcat + 1) > $last_index4)
				$last_line4 = ' last_line4';
			$last_line3 = '';
			if (($kcat + 1) > $last_index3)
				$last_line3 = ' last_line3';
			$last_line5 = '';
			if (($kcat + 1) > $last_index5)
				$last_line5 = ' last_line5';

			$res .= '<li class="cat-item cat-item-'.$cat->term_id.$last_line5.$last_line4.$last_line3.'"><a href="'.get_term_link($cat->slug, 'category').'">'.$cat->name.'</a>';
			$args['parent'] = $cat->term_id;
			$child = get_categories( $args );
			if (count($child) > 0) {
				shuffle($child);
				$res .= '<ul class="children">';
				foreach ($child as $kccat => $ccat)
					// if ($kccat < 5)
						$res .= '<li class="cat-item cat-item-'.$ccat->term_id.'"><a href="'.get_term_link($ccat->slug, 'category').'">'.$ccat->name.'</a>';
				$res .= '</ul>';
			}
			$res .= '</li>';
		}
	}
	$res .= '</ul>';


  return $res;
}
add_shortcode('firm_categories_menu', 'get_firm_categories_shortcode');

add_filter( 'woocommerce_add_to_cart_redirect', 'skip_woo_cart' );
function skip_woo_cart() {
	if( ! WC()->cart->is_empty() ) {
		$i = 1;
		$cart = WC()->cart->get_cart();
		foreach ( $cart as $cart_item_key => $cart_item ) {
			if ( $i < count($cart) )
			 	WC()->cart->remove_cart_item( $cart_item_key );
			else
				WC()->cart->set_quantity( $cart_item_key, 1 );
			$i++;
   		}
	}

	return wc_get_checkout_url();
}
add_filter( 'woocommerce_is_purchasable', 'customizing_is_purchasable', 20, 2 );
function customizing_is_purchasable( $purchasable, $product ){
    if( $product->exists() && ( 'publish' === $product->get_status() || current_user_can( 'edit_post', $product->get_id() ) ) )
        $purchasable = true;

    return $purchasable;
}
add_filter( 'woocommerce_product_single_add_to_cart_text', 'cw_btntext_cart' );
add_filter( 'woocommerce_product_add_to_cart_text', 'cw_btntext_cart' );
function cw_btntext_cart() {
    return 'Wybierz';
}

if (! function_exists( 'woocommerce_template_loop_product_link_open')) {
    function woocommerce_template_loop_product_link_open() {
		echo '<article class="offer_item">';
    }
}
if (! function_exists( 'woocommerce_template_loop_product_link_close')) {
    function woocommerce_template_loop_product_link_close() {
		echo '</article>';
    }
}
if ( ! function_exists( 'woocommerce_template_loop_product_title' ) ) {
	function woocommerce_template_loop_product_title() {
		echo '<div class="offer_description"><h2 class="' . esc_attr( apply_filters( 'woocommerce_product_loop_title_classes', 'woocommerce-loop-product__title' ) ) . '">' . get_the_title() . '</h2>';
	}
}
if ( ! function_exists( 'woocommerce_template_loop_price' ) ) {
	function woocommerce_template_loop_price() {
		wc_get_template( 'loop/price.php' );
		echo woocommerce_product_description_tab();
		woocommerce_template_loop_add_to_cart_webcost();
		echo '</div>';
	}
}
if ( ! function_exists( 'woocommerce_get_product_thumbnail' ) ) {
	function woocommerce_get_product_thumbnail( $size = 'woocommerce_thumbnail', $deprecated1 = 0, $deprecated2 = 0 ) {
		global $product;
		$image_size = apply_filters( 'single_product_archive_thumbnail_size', $size );
		$html = "<div class='img_container'>";
		$html .= $product ? $product->get_image( $image_size ) : '';
		$html .= "</div>";
		return $html;
	}
}
if ( ! function_exists( 'woocommerce_template_loop_add_to_cart' ) ) {
	function woocommerce_template_loop_add_to_cart( $args = array() ) {
		return;
	}
}
if ( ! function_exists( 'woocommerce_template_loop_add_to_cart_webcost' ) ) {
	function woocommerce_template_loop_add_to_cart_webcost( $args = array() ) {
		global $product;

		if ( $product ) {
			$defaults = array(
				'quantity'   => 1,
				'class'      => implode(
					' ',
					array_filter(
						array(
							'button',
							'product_type_' . $product->get_type(),
							$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
							$product->supports( 'ajax_add_to_cart' ) && $product->is_purchasable() && $product->is_in_stock() ? 'ajax_add_to_cart' : '',
						)
					)
				),
				'attributes' => array(
					'data-product_id'  => $product->get_id(),
					'data-product_sku' => $product->get_sku(),
					'aria-label'       => $product->add_to_cart_description(),
					'rel'              => 'nofollow',
				),
			);

			$args = apply_filters( 'woocommerce_loop_add_to_cart_args', wp_parse_args( $args, $defaults ), $product );

			if ( isset( $args['attributes']['aria-label'] ) ) {
				$args['attributes']['aria-label'] = wp_strip_all_tags( $args['attributes']['aria-label'] );
			}

			wc_get_template( 'loop/add-to-cart.php', $args );
		}
	}
}

function makeSlug($string) {
	$polskie = array(',', ' - ',' ','ę', 'Ę', 'ó', 'Ó', 'Ą', 'ą', 'Ś', 's', 'ł', 'Ł', 'ż', 'Ż', 'Ź', 'ź', 'ć', 'Ć', 'ń', 'Ń','-',"'","/","?", '"', ":", 'ś', '!','.', '&', '&', '#', ';', '[',']', '(', ')', '`', '%', '?', '?', '?');
	$miedzyn = array('-','-','-','e', 'e', 'o', 'o', 'a', 'a', 's', 's', 'l', 'l', 'z', 'z', 'z', 'z', 'c', 'c', 'n', 'n','-',"","","","","",'s','','', '', '', '', '',  '', '', '', '', '', '', '');
	return str_replace($polskie, $miedzyn, trim($string));
}

function get_new_firm_shortcode($attr = array()) {
	if (count($_POST) == 0) {
		$cuser = wp_get_current_user();
		$package = null;
		if ($cuser && $cuser->ID > 0) {
			$package = pmpro_getMembershipLevelForUser($cuser->ID);
		}
		if ($package == null) :
			$res = '<section id="offer_select"><div class="container">'
				.(get_field('show_select_title') == 1 ? '<h3 class="block_title">'.get_field('select_title_text').'</h3>' : '').
				'<div class="page_description">'
				.get_field( "text" ).
				'</div>
				<div class="offers_wrapper">
					<span class="entry-title">Informacje o Pakietach:</span>';

			$res .= do_shortcode('[product_category category="pakiety" orderby="id" order="ASC"]');

			$res .=	'</div>
			</div></section>';
		elseif (have_firm() && !isset($_GET['id'])) :
			global $wp_query;
			$wp_query->set_404();
			status_header( 404 );
			get_template_part( 404 );
			exit();
		else :
			$cat = get_categories(array(
				'taxonomy' => 'category',
				'orderby' => 'name',
				'order'   => 'ASC',
				'show_count'   => 0,
				'pad_counts'   => 0,
				'hierarchical' => 1,
				'title_li'     => '',
				'hide_empty'   => 0,
				'exclude'	   => array(1)
			));

			$cat_sort = array();
			$not_parent = array();
			if (count($cat) > 0) {
				foreach ($cat as $c) {
					if ($c->parent == 0) {
						// if (count($cat_sort) == 0)
						// 	$cat_sort[$c->term_id] = array('text' => $c->name, 'state' => array('selected' => true));
						// else
							$cat_sort[$c->term_id] = array('text' => $c->name, 'href' => $c->term_id);
					} else
						$not_parent[] = $c;
				}
				foreach ($not_parent as $c) {
					$cat_sort[$c->parent]['nodes'][] = array('text' => $c->name, 'href' => $c->term_id);
				}
			}
			$cat_sort = array_values($cat_sort);

			if (is_user_logged_in()) {
				global $current_user;

				$fname = '';
				$fphone = '';
				$faddress = '';
				$femail = '';
				$fwebsite = '';
				$fshortdesc = '';
				$fregion = '';
				$fimage = '';
				$ffulldesc = '';
				$fwojew = '';
				$fcity = '';
				$ftag = [];
				$fservices = [];


				if (!isset($_GET['id'])) {
					$umeta = get_user_meta( $current_user->ID );
					$user_info = get_userdata($current_user->ID);
					foreach ($umeta as $kmeta=>$meta){
						switch ($kmeta) {
							case 'firm_name':
								$fname = $meta[0];
								break;
							case 'phone':
								$fphone = $meta[0];
								break;
							case 'address':
								if (strlen($faddress) > 0)
									$faddress .= ' '.$meta[0];
								break;
							case 'post_code':
								$faddress .= ' '.$meta[0];
								break;
						}
					}

					$femail = $user_info->user_email;
					$faddress = trim($faddress);
				} else {
					$args = array(
						'post_type' => 'firmy',
						'post_status' => 'publish',
						'author'	  => $current_user->ID,
						'posts_per_page'=> 1,
						'p' => $_GET['id']
					);

					$post = new WP_Query( $args );
					if (count($post->posts) == 0) {
						global $wp_query;
						$wp_query->set_404();
						status_header( 404 );
						get_template_part( 404 ); exit();
					} else {
						$post = $post->posts[0];

						$fname = $post->post_title;
						$fimage = get_the_post_thumbnail_url($post->ID, 'medium');
						$ffulldesc = $post->post_content;

						$pmeta = get_post_meta( $post->ID );
						foreach ($pmeta as $kmeta=>$meta){
							switch ($kmeta) {
								case 'numer telefonu':
									$fphone = $meta[0];
									break;
								case 'Adres':
									$faddress = $meta[0];
									break;
								case 'e-mail':
									$femail = $meta[0];
									break;
								case 'strona_internetowa':
									$fwebsite = $meta[0];
									break;
								case 'strona_internetowa':
									$fwebsite = $meta[0];
									break;
								case 'krotki_opis':
									$fshortdesc = $meta[0];
									break;
								case 'region':
									$fregion = explode(', ',$meta[0]);
									break;
							}

							if (strpos($kmeta, '_opis_uslugi') !== false && strpos($kmeta, '_usluga_') === false && strlen($meta[0]) > 0 ) {
								$fservices[preg_replace('/[^0-9.]+/', '', $kmeta)]['text'] = $meta[0];
							}
							if (strpos($kmeta, '_obraz_uslugi') !== false && strpos($kmeta, '_usluga_') === false && strlen($meta[0]) > 0 )
								$fservices[preg_replace('/[^0-9.]+/', '', $kmeta)]['image'] = wp_get_attachment_image_url( $meta[0], 'medium' );
						}

						if (is_array($fregion)) {
							$fwojew = $fregion[0];
							$fcity = $fregion[1];
						}

						$terms = get_the_terms( $post->ID , 'category' );
						foreach ($cat_sort as $kcat=>$cat) {
							foreach ( $terms as $kterm=>$term ) {
								if ($cat['text'] == $term->name) {
									$cat_sort[$kcat]['state'] = array('selected' => true);
									$fcategory[] = $cat['text'];
								}
								if (isset($cat['nodes']) && is_array($cat['nodes'])) {
									foreach ($cat['nodes'] as $ksub=>$sub) {
										if ($sub['text'] == $term->name) {
											$fcategory[] = $cat['text'].'/'.$sub['text'];
											$cat_sort[$kcat]['nodes'][$ksub]['state'] = array('selected' => true);
											$cat_sort[$kcat]['state'] = array('expanded' => true);
										}
									}
								}
							}
						}
						$fcategory = implode(', ',$fcategory);

						$tags = wp_get_post_tags($post->ID);
						if (is_array($tags))
							foreach ($tags as $tag) {
								$ftag[] = $tag->name;
							}

						$ftag = implode(',', $ftag);
					}
				}

				global $wpdb;
				$w_list = $wpdb->get_results('Select name from '.$wpdb->prefix.'geo_list where id_parent = 0 and id_type=1');
				$w = '<div class="dropdown-menu" style="">';
				if ($w_list) {
					foreach ($w_list as $kw=>$wojew) {
						$w .= '<a class="dropdown-item wojew_item" href="#">'.$wojew->name.'</a>';
					}
				}
				$w .= '</div>';

				$res = '
					<div class="container">
					<div class="add_firm_form row">
						<div class="spiner_layout">
							<div class="spinner-border text-danger" role="status">
								<span class="sr-only">Loading...</span>
							</div>
						</div>
						<div class="left-column col-xs-12 col-md-7">
							<form id="add_firm" method="post">
								<input autocomplete="off" type="hidden" id="current_user" value="'.get_current_user_id().'">
								<div id="step1" class="step step1">
									<h3>Moja firma nazywa się:</h3>
									<div class="form-group">
										<input autocomplete="off" required type="text" placeholder="Nazwa firmy" class="form-control" id="firm_name" aria-describedby="firm_name_help" value="'.$fname.'">
										<small id="firm_name_help" class="form-text text-muted">To jest opis dla pola firma, ktory wyświetli się na samej górze wizytowki.</small>
										<div id="validationName" class="invalid-feedback">Pole wymagane</div>
									</div>
								</div>
								<div id="step2" class="step step2';
				if ($package->id == 1 || $package->id == 2)	$res .= ' disabled';
				$res .= '">
									<h3>Logo firmy:</h3>
									<div class="form-group">
										<div class="custom-file">
											<input ';
											if ($package->id == 1 || $package->id == 2)	$res .= 'disabled ';
										$res .= 'autocomplete="off" accept=".jpg,.jpeg.,.gif,.png" required type="file" class="custom-file-input" id="customFile">
											<label class="custom-file-label" for="customFile">Wybierz plik</label>
											<div id="validationLogo" class="invalid-feedback">Pole wymagane</div>'
											.((isset($_GET['id']) && ($package->id != 1 || $package->id != 2)) ? "<small class=\"form-text text-muted\">Aby zmienić logo, prześlij nowy plik. Jeśli zmiana nie jest potrzebna, pomiń kliknij przycisk Dalej. <strong>Zalecany rozmiar 615x435px</strong></small>" : "").
										'</div>
									</div>
								</div>
								<div id="step3" class="step step3">
									<h3>Zajmujemy się:</h3>
									<div class="form-group">
										<input autocomplete="off" type="hidden" class="form-control" id="firm_category" value="">
										<div id="cat_view" class=""></div>
										<div id="validationCat" class="invalid-feedback">Pole wymagane</div>
									</div>
								</div>
								<div id="step4" class="step step4">
									<h3>Numer komórkowy mojej firmy to:</h3>
									<div class="form-group">
										<input autocomplete="off" type="text" placeholder="Numer telefonu" class="form-control" id="firm_phone" aria-describedby="firm_phone_help" value="'.$fphone.'">
										<small id="firm_phone_help" class="form-text text-muted">W tym miejscu wpisz numer telefonu do Twojej firmy. Pamiętaj o tym, żeby zamieścić w nim numer kierunkowy Twojego kraju (np. +48 w przypadku polskich telekomów).</small>
									</div>
								</div>
								<div id="step5" class="step step5">
									<h3>Adres e-mail do kontaktu z moją firmą to:</h3>
									<div class="form-group">
										<input autocomplete="off" type="text" placeholder="Email" class="form-control" id="firm_email" aria-describedby="firm_email_help" value="'.$femail.'" '.((isset($_GET['id']) || strlen($femail) > 0) ? "disabled=\"disabled\"" : "").'>
										<small id="firm_email_help" class="form-text text-muted">Tutaj wpisz adres e-mail Twojej firmy. Ta informacja znajdzie się tuż pod numerem telefonu na Twojej wizytówce internetowej.</small>
									</div>
								</div>
								<div id="step6" class="step step6">
									<h3>Adres mojej firmy to:</h3>
									<div class="form-group">
										<input autocomplete="off"  type="text" placeholder="Pełny adres firmy" class="form-control" id="firm_address" aria-describedby="firm_address_help" value="'.$faddress.'">
										<small id="firm_address_help" class="form-text text-muted">Dzięki nim zamieścimy Mapę Google z Twoją lokalizacją.</small>
									</div>
									<div class="form-group search_bar">
										<div class="input-group">
											<input autocomplete="off" id="type_w" onkeyup="filterFunction(`w`,event)" name="" type="text" class="form-control" placeholder="WOJEWÓDZTWO" aria-label="WOJEWÓDZTWO" aria-describedby="basic-addon2" value="'.$fwojew.'">
											<div class="input-group-append" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											</div>';
											if (strlen($w) > 10)
												$res .= $w;
					$res .=					'</div>

										<div class="input-group">
											<input autocomplete="off" id="type_m" onkeyup="filterFunction(`m`,event)" name="" type="text" class="form-control" placeholder="MIASTO" aria-label="MIASTO" aria-describedby="basic-addon3" value="'.$fcity.'">
											<div class="input-group-append" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											</div>
											<div class="dropdown-menu">
											</div>
										</div>
									</div>
								</div>
								<div id="step7" class="step step7';
								if ($package->id == 1)	$res .= ' disabled';
								$res .= '">
									<h3>Strona internetowa mojej firmy to:</h3>
									<div class="form-group">
										<input ';
								if ($package->id == 1)	$res .= 'disabled ';
									$res .= 'autocomplete="off"  type="text" placeholder="Strona internetowa" class="form-control" id="firm_site" aria-describedby="firm_site_help" value="'.$fwebsite.'">
										<small id="firm_site_help" class="form-text text-muted">Wpisz tutaj adres strony internetowej, np. www.site.pl.</small>
									</div>
								</div>
								<div id="step8" class="step step8">
									<h3>Dodaj dodatkową informację:</h3>
									<div class="form-group">
										<input autocomplete="off" type="text" placeholder="Tagi" class="form-control" id="firm_tags" value="'.((!is_array($ftag) && strlen($ftag) > 0) ? $ftag : "").'" aria-describedby="firm_tags_help">
										<small id="firm_tags_help" class="form-text text-muted">Słowa kluczowe opisujące Twoją firmę. Aby dodać, użyj przycisku przecinka.';
									if ($package->id == 1) $res .= ' Maksymalna liczba słów to 10.';
									if ($package->id == 2) $res .= ' Maksymalna liczba słów to 20.';
									$res .= '</small>
									</div>
									<div class="description_wrap';
									if ($package->id == 1)	$res .= ' disabled';
									$res .= '">
										<div class="form-group mb-3">
											<textarea ';
											if ($package->id == 1)	$res .= 'disabled ';
												$res .= 'placeholder="Krótki opis" class="form-control" id="firm_short_description" rows="3" aria-describedby="firm_short_description_help">'.$fshortdesc.'</textarea>
											<small id="firm_short_description_help" class="form-text text-muted">Krótki opis firmy.</small>
										</div>
										<div class="form-group">
											<textarea ';
											if ($package->id == 1)	$res .= 'disabled ';
												$res .= 'placeholder="Pełny opis" class="form-control" id="firm_description" rows="3" aria-describedby="firm_description_help">'.$ffulldesc.'</textarea>
											<small id="firm_description_help" class="form-text text-muted">Pełny opis firmy.</small>
										</div>

										<div id="services_block"';
										if ($package->id == 1)	$res .= ' class="disabled"';
										$res .= '>';
										$res .= '<a class="btn purple-outline mb-3" id="add-service" ';
										if ($package->id == 1)	$res .= ' data-toggle="popover" data-placement="top" data-content="Niedostępne w ramach wybranego Pakietu"';

										$res .='>Dodaj usługi</a>';
										if (is_array($fservices) && count($fservices) > 0) {
											foreach ($fservices as $ks=>$s) {
												$res .= '
													<section class="service_block service_block_'.$ks.'">
														<div class="form-group">
															<input type="text" placeholder="Opis usługi" class="form-control" id="firm_service_'.$ks.'" value="'.$s['text'].'">
														</div>
														<div class="custom-file"><input accept=".jpg,.jpeg.,.gif,.png" required="" type="file" class="custom-file-input" id="custom_file_'.$ks.'" onchange="preview(event, '.$ks.')">
														<label class="custom-file-label" for="customFile">Wybierz plik</label>
														<div class="img_container img_'.$ks.'" '.((isset($s['image'])) ? "style=\"display:block\"" : "").'>
															<img id="img_th_'.$ks.'" src="'.((isset($s['image'])) ? $s['image'] : "").'">
															<span onclick="deleteThumb('.$ks.')">Usuń zdjęcie</span>
														</div>
														</div>
														<img id="delete_service" onclick="deleteService('.$ks.')" src="/wp-content/themes/webcost/assets/img/delete.png">
													</section>';
											}
										}

									$res .='</div>
									</div>
								</div>
							</form>
							<div class="navi-btns">
								<button class="btn" id="prev-step">Wstecz</button>
								<div class="right-nav">
									<button class="btn purple d-flex" id="next-step">
										Dalej
									</button>
								</div>
							</div>
						</div>
						<div class="rigt-column preview col-xs-12 col-md-5">
							<article class="post_block">
								<div class="img_container" '.((strlen($fimage)) ? "style=\"display:block\"" : "").'>
									<img id="pr_firm_logo" src="'.$fimage.'" alt="">
								</div>
								<div class="post-content">
									<div class="post-title" '.((strlen($fname)) ? "style=\"display:block\"" : "").'><h3 id="pr_firm_name">'.$fname.'</h3></div>
									<div class="post_category" '.((strlen($fcategory)) ? "style=\"display:flex\"" : "").' id="pr_firm_category">'.$fcategory.'</div>
									<div class="post_metas" '.((strlen($faddress) || strlen($fphone) || strlen($femail) || strlen($fwebsite)) ? "style=\"display:flex\"" : "").'>
										<span id="pr_firm_address" '.((strlen($faddress)) ? "style=\"display:flex\"" : "").'><img src="'.get_template_directory_uri().'/assets/img/IK_POLOZENIE.png"><span>'.$faddress.'</span></span>
										<span id="pr_firm_phone" '.((strlen($fphone)) ? "style=\"display:flex\"" : "").'><img src="'.get_template_directory_uri().'/assets/img/IK_TEL.png"><span>'.$fphone.'</span></span>
										<span id="pr_firm_email" '.((strlen($femail)) ? "style=\"display:flex\"" : "").'><img src="'.get_template_directory_uri().'/assets/img/IK_POCZTA.png"><span>'.$femail.'</span></span>
										<span id="pr_firm_site" '.((strlen($fwebsite)) ? "style=\"display:flex\"" : "").'><img src="'.get_template_directory_uri().'/assets/img/IK_WWW.png"><span>'.$fwebsite.'</span></span>
									</div>
									<div class="post_description" '.((strlen($ffulldesc) || strlen($fshortdesc)) ? "style=\"display:block\"" : "").' id="pr_firm_description">'.((strlen($fshortdesc) > 0) ? $fshortdesc : $ffulldesc).'</div>
								</div>
							</article>
						</div>
					</div>
					</div>
					<script>
						document.addEventListener("DOMContentLoaded", function(){
							var defaultData = '.json_encode($cat_sort).';

							if ($(".step.step8 #firm_tags").length > 0) {
								$(".step.step8 #firm_tags").tagsinput({
									confirmKeys: [44],
									';

				if ($package->id == 1) $res .= 'maxTags: 10,';
				if ($package->id == 2) $res .= 'maxTags: 20,';

					$res .= '	});
							}

							$("#cat_view").treeview({
								levels: 1,
								data: defaultData,';
				if ($package->id != 1)	$res .= 'multiSelect: true,';

				$res .= '
							});

							$("#cat_view").treeview("expandAll");

							var ids = [];
							var i = 0;
							defaultData.forEach(function(item){
								ids[i] = item["href"];
								i++;
								if (item["nodes"] != null) {
									item["nodes"].forEach(function(child){
										ids[i] = child["href"];
										i++;
									})
								}
							});

							selected = $("#cat_view").treeview("getSelected");
							selected.forEach(function(item,index){
								selected[index] = item.href;
							});
							$("#firm_category").val(selected.join());


							$("#cat_view").treeview("collapseAll");

							$("#cat_view").on("nodeSelected", function(event, data) {
								selected = $("#cat_view").treeview("getSelected");';

							if ($package->id == 2)
								$res .= '
									if (selected.length > 3) {
										$("#cat_view").treeview("unselectNode", [ data.nodeId, { silent: true } ]);
										alert("W ramach wybranego pakietu można dodać tylko 3 branży");
										return;
									}
								';

							$res .= '
								$("#firm_category").val("");
								cat = [];
								cat_text = [];

								selected.forEach(function(item){
									if (data.nodeId != item.href) {
										cat.push(item.href);
										if ("undefined" !== typeof item.parentId) {
											parent = $("#cat_view").treeview("getNode", item.parentId);
											cat_text.push(parent.text+"/"+item.text);
										} else
											cat_text.push(item.text);
									}
								});
								if (cat_text.length > 0)
									$(".rigt-column #pr_firm_category").css("display","flex").text(cat_text.join(", "));
								else
									$(".rigt-column #pr_firm_category").css("display","none").text(cat_text.join(", "));
								$("#firm_category").val(cat.join());
							});

							$("#cat_view").on("nodeUnselected", function(event, data) {
								selected = $("#cat_view").treeview("getSelected");
								$("#firm_category").val("");
								cat = [];
								cat_text = [];

								selected.forEach(function(item){
									if (data.nodeId != item.href) {
										cat.push(item.href);
										if ("undefined" !== typeof item.parentId) {
											parent = $("#cat_view").treeview("getNode", item.parentId);
											cat_text.push(parent.text+"/"+item.text);
										} else
											cat_text.push(item.text);
									}
								});
								if (cat_text.length > 0)
									$(".rigt-column #pr_firm_category").css("display","flex").text(cat_text.join(", "));
								else
									$(".rigt-column #pr_firm_category").css("display","none").text(cat_text.join(", "));

								$("#firm_category").val(cat.join());
							});

							$("input#firm_phone").inputmask("+48 999 999 999", { "clearIncomplete": true });
						});
					</script>
				';
			} else {
				$res = 'need login';
			}
		endif;
	} else {
		if (isset($_POST['status']) && $_POST['status'] == 'trash') {
			wp_delete_post($_POST['ID']);
			die();
		}

		$cuser = wp_get_current_user();
		$package = null;
		if ($cuser && $cuser->ID > 0) {
			$package = pmpro_getMembershipLevelForUser($cuser->ID);
		}
		if ($package != null) {

			require_once ABSPATH . 'wp-admin/includes/image.php';
			require_once ABSPATH . 'wp-admin/includes/file.php';
			require_once ABSPATH . 'wp-admin/includes/media.php';

			$post_data = array(
				'post_title'    => sanitize_text_field( $_POST['title'] ),
				'post_content'  => $_POST['content'],
				'post_status'   => $_POST['status'],
				'post_author'   => 1,
				'comment_status'=> $_POST['comment_status'],
				'post_type'		=> 'firmy'
			);

			if (isset($_POST['ID'])) {
				$post_data['ID'] = $_POST['ID'];
				update_user_meta($cuser->ID, 'firm_name', $_POST['title']);
			}

			$polskie = array(',', ' - ',' ','ę', 'Ę', 'ó', 'Ó', 'Ą', 'ą', 'Ś', 's', 'ł', 'Ł', 'ż', 'Ż', 'Ź', 'ź', 'ć', 'Ć', 'ń', 'Ń','-',"'","/","?", '"', ":", 'ś', '!','.', '&', '&', '#', ';', '[',']', '(', ')', '`', '%', '?', '?', '?');
			$miedzyn = array('-','-','-','e', 'e', 'o', 'o', 'a', 'a', 's', 's', 'l', 'l', 'z', 'z', 'z', 'z', 'c', 'c', 'n', 'n','-',"","","","","",'s','','', '', '', '', '',  '', '', '', '', '', '', '');
			$metas = [];
			if (isset($_POST['phone'])) {
				$metas['numer telefonu'] = $_POST['phone'];
				if (isset($_POST['ID']))
					update_user_meta($cuser->ID, 'phone', $_POST['phone']);
			}
			if (isset($_POST['email'])) {
				$metas['e-mail'] = $_POST['email'];
			}
			if (isset($_POST['address'])) {
				$metas['Adres'] = $_POST['address'];
				if (isset($_POST['ID'])) {
					update_user_meta($cuser->ID, 'address', trim(str_replace(get_user_meta($cuser->ID, 'post_code')[0],'',$_POST['address'])));
				}
			}
			if (isset($_POST['web']))
				$metas['strona_internetowa'] = $_POST['web'];
			if (isset($_POST['short_content']))
				$metas['krotki_opis'] = $_POST['short_content'];

			$region = '';
			if (isset($_POST['region'])) {
				$region = $_POST['region'];
				$region_slug = mb_strtolower(str_replace('--','-',str_replace($polskie, $miedzyn, trim($_POST['region']))), 'UTF-8');
			}
			if (isset($_POST['city']))
				if (strlen($region) > 0) {
					$region .= ', '.$_POST['city'];
					$city_slug = mb_strtolower(str_replace('--','-',str_replace($polskie, $miedzyn, trim($_POST['city']))), 'UTF-8');
				}

			$metas['region'] = $region;

			$metas['region_slug'] = $region_slug.'-'.$city_slug;

			foreach ($_POST as $kpost => $post) {
				if (strpos($kpost, 'firm_service_') !== false) {
					$metas['usluga_'.preg_replace('/[^0-9.]+/', '', $kpost).'_opis_uslugi'] = $post;
					$metas['usluga_'.preg_replace('/[^0-9.]+/', '', $kpost).'_slug'] = mb_strtolower(str_replace('--','-',str_replace($polskie, $miedzyn, trim($post))), 'UTF-8');
				}
			}

			if (isset($_POST['image']))
				foreach ($_FILES as $kfile => $file) {
					if (strpos($kfile, 'custom_file_') !== false) {
						$attachment_id = media_handle_upload( $kfile,  $post_id );
						if (is_numeric($attachment_id))
							$metas['usluga_'.preg_replace('/[^0-9.]+/', '', $kpost).'_obraz_uslugi'] = $attachment_id;
					}
				}

			if (count($metas) > 0)
				$post_data['meta_input'] = $metas;

			$post_id = 0;
			$post_id = wp_insert_post(wp_slash($post_data));

			if ($post_id > 0) {
				$cur_user_id = get_current_user_id();
				if ($cur_user_id > 0) {
					$auth = array(
						'ID' => $post_id,
						'post_author' => $cur_user_id,
					);
					wp_update_post( $auth );
				}

				if (isset($_POST['categories'])) {
					$cat = explode(',', $_POST['categories']);
					if (count($cat) > 0) {
						foreach ($cat as $c) {
							wp_set_post_terms($post_id, (int)$c, 'category', true );
						}
					}
				}

				if (isset($_POST['firm_tags'])) {
					$tags = explode(',', $_POST['firm_tags']);
					if (count($tags) > 0) {
						foreach ($tags as $tag) {
							wp_set_post_terms($post_id, $tag, 'post_tag', true );
						}
					}
				}

				if (isset($_FILES['image'])) {
					$attachment_id = media_handle_upload( 'image',  $post_id );
					if (is_numeric($attachment_id))
						set_post_thumbnail( $post_id, $attachment_id );
				}


				$res = "<span id='reload-page'>".get_post_permalink($post_id)."</span>";
			}
		} else {
			$res = "<div class='alert alert-danger' role='alert'>Nie masz wykupionego pakietu</div>";
		}
	}

	return $res;
}
add_shortcode('add_new_firm', 'get_new_firm_shortcode');

function be_exclude_category_from_blog( $query ) {

	if( $query->is_main_query() && ! is_admin() && $query->is_home() && !$query->is_front_page() ) {
		$query->set( 'cat', '-1' );
	}
}
add_action( 'pre_get_posts', 'be_exclude_category_from_blog' );

function mb_ucfirst($str) {
    $fc = mb_strtoupper(mb_substr($str, 0, 1));
    return $fc.mb_strtolower(mb_substr($str, 1));
}

function get_geo_shortcode($attr = array()) {
	global $wpdb;
	if (isset($attr['position'])) {
		if ($attr['position'] == 'home') {
			$res = '';
			$find = $wpdb->get_results('select w.name as wname,w.id as wid from '.$wpdb->prefix.'geo_list w where w.id_type = 1');
			$list = array();
			if ($find) {
				$res = '<div id="regionList" class="dropdown-menu">
							<div class="mobile"><input autocomplete="off" type="text" class="form-control" id="place_inner" name="region" placeholder="miejscowość, powiat"><a class="close"></a></div>
							<ul class="categorySelectList regionsList">
								<li class="dropdown-item">
									<a id="a-region-0" href="#" class="rel block tdnone regionSelectA1 a-region-0" data-name="" data-id=""><span class="name"><strong>Cała Polska</strong></span></a>
								</li>';
				foreach ($find as $kl=>$l) {
					$list[$l->wname] = array();
					$child = $wpdb->get_results('select p.name as pname,p.id as pid from '.$wpdb->prefix.'geo_list p where p.id_type = 2 and p.id_parent = '.$l->wid);
					$res .= '<li class="dropdown-item'
							.((is_array($child) && count($child) > 0) ? " has_child" : "").'"'
							.'data-region="'.$l->wid.'">
								<a
									id="a-region-'.$l->wid.'"
									href="#"
									class="rel block tdnone regionSelectA1 a-region-'.$l->wid.'"
									data-name="'.$l->wname.'"
									data-id="'.$l->wid.'"
									>
										<span class="name">'.$l->wname.'</span>'
							.((is_array($child) && count($child) > 0) ? "<span class=\"expand\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAB9ElEQVR4nO2asS5EQRSGP4SCRkKU20qo6Ei8AHq67XgC0Z5W4gVoNbzAegCNTilaVAqJhoLs1bjZkLXZnTN3JufMfsn0/znJ3ft/sxfGjGkSAY5zh8iFANXPOcwbJT1Cb/gK6AL7OQOlRPg9fH0+ge18sdIg9B++Ph/AVq5wTSMMHr4+b8B6nojNIQw3fH1egOUcQZtAGG34+jwCrfRx43NK2AIq4AFYSh85LpPABeFLuAPmk6eOzDTQIXwJN8Bs8tSRmQNuCV/CNTCTPHVkFoB7wpdwBUwlTx2ZFvBE+BLOgYnkqSOzArwSvoST9JHjswG8E74EFxq9A3wRvgQXGt0mfAFuNPqI8CW40WhNZXah0drK7EKjtZXZhUZrK7MLjdZWZhcara3MLjRaW5ldaLS2MrvQaG1ldqHRbcIXUOFEozWVucKBRhe9AO0jcIbhR2AX3Y/gJYZ/BDfRvQY7GH4NrlJwEWoBz4QPb7oKL1KwDBWtw0VfiMS4EltLnjoiRV+KFn0t3iZ8+C6wlz5yPLQV9yB95HhoK67pP0e1Fde01morrmmt1VZc01qrrbimtbboz+SK/1Cy+E9lhbDhTWvtX4TRhjettf8hDDe8ea0dhDB4ePNaOwxC/+HNa+0oCL+HN6+1IQi9BZjWWg2Cca0d451vuzMtsF6u8xMAAAAASUVORK5CYII=\" height=\"16px\"></span>" : "")
							.'</a></li>';
					if (is_array($child) && count($child) > 0) {
						$res .= '<ul class="abs" data-region="'.$l->wid.'">';
						foreach ($child as $kc=>$c) {
							$res .= '<li>
								<a
								id="a-region-'.$l->wid.'"
								href="#"
								class="rel block tdnone regionSelectA2"
								data-name="'.$c->pname.'"
								data-id="'.$l->wid.$c->pid.'"
								><span class="name">'.$c->pname.'</span></a>
							</li>';
						}
						$res .= '</ul>';
					}
				}
				$res .= '	</ul>
				</div>';
			}
			echo $res;
		}
	}
	if (isset($attr['type'])) {
		if ($attr['type'] == 'w') {
			$res = '';
			$list = $wpdb->get_results('Select name from '.$wpdb->prefix.'geo_list where id_parent = 0 and id_type = 1');
			if ($list) {
				foreach ($list as $kl=>$l)
					$res .= '<a class="dropdown-item wojew_item" href="#">'.$l->name.'</a>';
			}
			echo $res;
		}
	}

}
add_shortcode('geo_list', 'get_geo_shortcode');

function get_post_search_shortcode($attr = array()) {

	add_filter( 'posts_where', 'search_by_first_letter' );
	$args = array(
		'post_type' => 'firmy',
    	'post_status' => 'publish',
		'suppress_filters' => FALSE
	);

	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$args['paged'] = $paged;

	if (isset($attr['letter']))
		$args['start_with'] = $attr['letter'];

	$posts = new WP_Query( $args );

	remove_filter( 'posts_where', 'search_by_first_letter' );

	return json_encode($posts->posts);
}
add_shortcode('post_search', 'get_post_search_shortcode');

/* pagination */
function kama_pagenavi( $before = '', $after = '', $echo = true, $args = array(), $wp_query = null, $customMaxPageCount = null, $customPages = null ) {
    if ( ! $wp_query ) {
        global $wp_query;
    }

    // параметры по умолчанию
    $default_args = array(
        'text_num_page'   => '',
        // Текст перед пагинацией. {current} - текущая; {last} - последняя (пр. 'Страница {current} из {last}' получим: "Страница 4 из 60" )
        'num_pages'       => 3,
        // сколько ссылок показывать
        'step_link'       => 0,
        // ссылки с шагом (значение - число, размер шага (пр. 1,2,3...10,20,30). Ставим 0, если такие ссылки не нужны.
        'dotright_text'   => '…',
        // промежуточный текст "до".
        'dotright_text2'  => '…',
        // промежуточный текст "после".
        'back_text'       => '<',
        // текст "перейти на предыдущую страницу". Ставим 0, если эта ссылка не нужна.
        'next_text'       => '>',
        // текст "перейти на следующую страницу". Ставим 0, если эта ссылка не нужна.
        'first_page_text' => 0,
        // текст "к первой странице". Ставим 0, если вместо текста нужно показать номер страницы.
        'last_page_text'  => 0,
        // текст "к последней странице". Ставим 0, если вместо текста нужно показать номер страницы.
    );

    $default_args = apply_filters( 'kama_pagenavi_args', $default_args ); // чтобы можно было установить свои значения по умолчанию

    $args = array_merge( $default_args, $args );

    extract( $args );

    $posts_per_page = (int) $wp_query->query_vars['posts_per_page'];
    $paged          = $customPages ?? (int) $wp_query->query_vars['paged'];
    $max_page       = $customMaxPageCount ?? $wp_query->max_num_pages;

    //проверка на надобность в навигации
    if ( $max_page <= 1 ) {
        return false;
    }

    if ( empty( $paged ) || $paged == 0 ) {
        $paged = 1;
    }

    $pages_to_show         = intval( $num_pages );
    $pages_to_show_minus_1 = $pages_to_show - 1;

    $half_page_start = floor( $pages_to_show_minus_1 / 2 ); //сколько ссылок до текущей страницы
    $half_page_end   = ceil( $pages_to_show_minus_1 / 2 ); //сколько ссылок после текущей страницы

    $start_page = $paged - $half_page_start; //первая страница
    $end_page   = $paged + $half_page_end; //последняя страница (условно)

    if ( $start_page <= 0 ) {
        $start_page = 1;
    }
    if ( ( $end_page - $start_page ) != $pages_to_show_minus_1 ) {
        $end_page = $start_page + $pages_to_show_minus_1;
    }
    if ( $end_page > $max_page ) {
        $start_page = $max_page - $pages_to_show_minus_1;
        $end_page   = (int) $max_page;
    }

    if ( $start_page <= 0 ) {
        $start_page = 1;
    }

    //выводим навигацию
    $out = '';

    // создаем базу чтобы вызвать get_pagenum_link один раз
    $link_base = str_replace( 99999999, '___', get_pagenum_link( 99999999 ) );
    $first_url = get_pagenum_link( 1 );
    if ( false === strpos( $first_url, '?' ) ) {
        $first_url = user_trailingslashit( $first_url );
    }

    $out .= $before . "<div class='wp-pagenavi'>\n";

    if ( $text_num_page ) {
        $text_num_page = preg_replace( '!{current}|{last}!', '%s', $text_num_page );
        $out           .= sprintf( "<span class='pages'>$text_num_page</span> ", $paged, $max_page );
    }
    // назад
    if ( $back_text && $paged != 1 ) {
        $out .= '<a class="prev" href="' . ( ( $paged - 1 ) == 1 ? $first_url : str_replace( '___', ( $paged - 1 ), $link_base ) ) . '">' . $back_text . '</a> ';
    }
    // в начало
    if ( $start_page >= 2 && $pages_to_show < $max_page ) {
        $out .= '<a class="first" href="' . $first_url . '">' . ( $first_page_text ? $first_page_text : 1 ) . '</a> ';
        if ( $dotright_text && $start_page != 2 ) {
            $out .= '<span class="extend">' . $dotright_text . '</span> ';
        }
    }
    // пагинация
    for ( $i = $start_page; $i <= $end_page; $i ++ ) {
        if ( $i == $paged ) {
            $out .= '<span class="current">' . $i . '</span> ';
        } elseif ( $i == 1 ) {
            $out .= '<a href="' . $first_url . '">1</a> ';
        } else {
            $out .= '<a href="' . str_replace( '___', $i, $link_base ) . '">' . $i . '</a> ';
        }
    }

    //ссылки с шагом
    $dd = 0;
    if ( $step_link && $end_page < $max_page ) {
        for ( $i = $end_page + 1; $i <= $max_page; $i ++ ) {
            if ( $i % $step_link == 0 && $i !== $num_pages ) {
                if ( ++ $dd == 1 ) {
                    $out .= '<span class="extend">' . $dotright_text2 . '</span> ';
                }
                $out .= '<a href="' . str_replace( '___', $i, $link_base ) . '">' . $i . '</a> ';
            }
        }
    }
    // в конец
    if ( $end_page < $max_page ) {
        if ( $dotright_text && $end_page != ( $max_page - 1 ) ) {
            $out .= '<span class="extend">' . $dotright_text2 . '</span> ';
        }
        $out .= '<a class="last" href="' . str_replace( '___', $max_page, $link_base ) . '">' . ( $last_page_text ? $last_page_text : $max_page ) . '</a> ';
    }
    // вперед
    if ( $next_text && $paged != $end_page ) {
        $out .= '<a class="next" href="' . str_replace( '___', ( $paged + 1 ), $link_base ) . '">' . $next_text . '</a> ';
    }

    $out .= "</div>" . $after . "\n";

    $out = apply_filters( 'kama_pagenavi', $out );

    if ( $echo ) {
        return print $out;
    }

    return $out;
}

//html in category description
add_action('init','disable_kses_if_allowed');
function disable_kses_if_allowed() {
	if (current_user_can('unfiltered_html')) {
		foreach (array('pre_term_description', 'pre_link_description', 'pre_link_notes', 'pre_user_description') as $filter) {
			remove_filter($filter, 'wp_filter_kses');
		}
	}

	foreach (array('term_description', 'link_description', 'link_notes', 'user_description') as $filter) {
		remove_filter($filter, 'wp_kses_data');
	}
}

//
function active_user_packages ( $where ) {
	global $wpdb;
	$where .= ' AND ('.$wpdb->prefix.'pmpro_memberships_users.status = "active")';
	return $where;
}
//search by first letter
function search_by_first_letter ( $where ) {
	global $wpdb;

	if (isset($_GET['letter']))
		$where .= " AND $wpdb->posts.post_title LIKE '".$_GET['letter']."%' AND (".$wpdb->prefix."pmpro_memberships_users.status = 'active')";

	return $where;
}
//search firm by title/content like
function title_filter( $where )
{
    global $wpdb;
    if ( isset($_GET['s'] ) ) {
        // $where .= ' OR (' . $wpdb->posts . '.post_title LIKE "%' . esc_sql( like_escape( $_GET['s'] ) ) . '%" OR '.$wpdb->posts.'.post_content LIKE "%'. esc_sql( like_escape( $_GET['s'] ) ) .'" AND '.$wpdb->posts.'.post_type = "firmy")';
        $where .= ' OR ('.$wpdb->posts.'.ID IN (SELECT '.$wpdb->posts.'.ID FROM '.$wpdb->posts.' WHERE ('.$wpdb->posts.'.post_title LIKE "%'. esc_sql( like_escape( $_GET['s'] ) ) .'%" OR '.$wpdb->posts.'.post_content LIKE "%'. esc_sql( like_escape( $_GET['s'] ) ) .'%") AND '.$wpdb->posts.'.post_type = "firmy") ) AND ('.$wpdb->prefix.'pmpro_memberships_users.status = "active")';
    }
    return $where;
}
//search firm by title like
function title_only_filter( $where )
{
    global $wpdb;
    if ( isset($_GET['s'] ) ) {
        $where .= ' AND ('.$wpdb->posts.'.ID IN (SELECT '.$wpdb->posts.'.ID FROM '.$wpdb->posts.' WHERE '.$wpdb->posts.'.post_title LIKE "%'. esc_sql( like_escape( $_GET['s'] ) ) .'%" AND '.$wpdb->posts.'.post_type = "firmy") ) AND ('.$wpdb->prefix.'pmpro_memberships_users.status = "active")';
    }
    return $where;
}
//add pmpro users table
function join_pmpro_user_table( $join, $query )
{
    global $wpdb;
	$join .= ' INNER JOIN '.$wpdb->prefix.'pmpro_memberships_users ON ('.$wpdb->prefix.'pmpro_memberships_users.user_id = '.$wpdb->posts.'.post_author)';
    return $join;
}
function orderby_package( $orderby, $query ){
    global $wpdb;
	$orderby = $wpdb->prefix.'pmpro_memberships_users.membership_id DESC, '.$orderby;
	return $orderby;
}

//add page title field to category edit page
function addTitleFieldToCat(){
    $cat_title = get_term_meta($_GET['tag_ID'], '_pagetitle', true);
    $cat_optim_title = get_term_meta($_GET['tag_ID'], '_optimtitle', true);
    $cat_optim_text = get_term_meta($_GET['tag_ID'], '_optimtext', true);
    ?>
	<div class="form-field">
		<tr class="form-field">
			<th scope="row" valign="top"><label for="cat_page_title"><?php _e('Kategoria Opis Tytuł'); ?></label></th>
			<td>
			<input type="text" name="cat_title" id="cat_title" value="<?php echo $cat_title ?>"><br />
				<span class="description"><?php _e('Tytuł opisu kategorii '); ?></span>
			</td>
		</tr>
	</div>
	<div class="form-field">
		<tr class="form-field">
			<th scope="row" valign="top"><label for="cat_text_optim_title"><?php _e('Nagłówek tekstu optymalizacyjnego'); ?></label></th>
			<td>
			<input type="text" name="cat_text_optim_title" id="cat_text_optim_title" value="<?php echo $cat_optim_title ?>"><br />
			</td>
		</tr>
	</div>
	<div class="form-field">
		<tr class="form-field">
			<th scope="row" valign="top"><label for="cat_text_optim_text"><?php _e('Tekst optymalizacyjny'); ?></label></th>
			<td>
			<textarea rows="5" cols="40" name="cat_text_optim_text" id="cat_text_optim_text"><?php echo $cat_optim_text ?></textarea><br />
			</td>
		</tr>
	</div>
    <?php

}
add_action ( 'category_edit_form_fields', 'addTitleFieldToCat');
add_action ( 'category_add_form_fields', 'addTitleFieldToCat');

add_action( 'edit_term','ad_cat_tax_img_url_save1' );
add_action( 'create_term','ad_cat_tax_img_url_save1' );
function ad_cat_tax_img_url_save1( $term_id ){
    if ( isset( $_POST['cat_title'] ) ) {
        update_term_meta($_POST['tag_ID'], '_pagetitle', $_POST['cat_title']);
    }
    if ( isset( $_POST['cat_text_optim_title'] ) ) {
        update_term_meta($_POST['tag_ID'], '_optimtitle', $_POST['cat_text_optim_title']);
    }
    if ( isset( $_POST['cat_text_optim_text'] ) ) {
        update_term_meta($_POST['tag_ID'], '_optimtext', $_POST['cat_text_optim_text']);
    }
}

// add_action( 'save_post', 'wpse_redirect_to_dashboard', 40, 3 );
// function wpse_redirect_to_dashboard( $post_id, $post, $update ) {
//     global $_POST;
//     if( count( $_POST ) > 0 && true === $update) {
//         wp_redirect( admin_url( 'edit.php?post_type='.$post->post_type ) ); exit;
//     }
// }

function um_custom_validate_post_code( $key, $array, $args ) {
	if ( strlen($args[$key]) > 0 && isset( $args[$key] ) && !preg_match('/\d{2}-\d{3}$/', $args[$key]) ) {
		UM()->form()->add_error( $key, __( 'Proszę wpisać poprawny Kod pocztowy.', 'ultimate-member' ) );
	}
}
add_action( 'um_custom_field_validation_post_code', 'um_custom_validate_post_code', 30, 3 );



// ADD custom columns to users table
function new_modify_user_table( $column ) {
    $column['firmy'] = 'Firmy';
    $column['package'] = 'Pakiet';
	unset($column['name']);
    return $column;
}
add_filter( 'manage_users_columns', 'new_modify_user_table' );

function new_modify_user_table_row( $val, $column_name, $user_id ) {
    switch ($column_name) {
        case 'firmy' :
			$args = array(
				'post_type' => 'firmy',
				'post_status' => 'publish',
				'author' => $user_id,
			);
			$posts = new WP_Query( $args );

            return '<a href="'.admin_url().'edit.php?post_type=firmy&author='.$user_id.'">'.$posts->found_posts.'</a>';
		case 'package' :
			$level = pmpro_getMembershipLevelForUser($user_id);
			if ($level && $level->id > 0)
				return $level->name;
			else
				return '-';
        case 'account_status' :
			um_fetch_user( $user_id );
			$status = explode('_',um_user('account_status'));
			foreach($status as $ks=>$s) {
				if ($s == 'email')
					$s = 'e-mail';
				$status[$ks] = mb_ucfirst($s);
			}
			$status = implode(' ', $status);
			um_reset_user();
			return __( $status, 'ultimate-member' );
		default:
			return '-';
    }
    return $val;
}
add_filter( 'manage_users_custom_column', 'new_modify_user_table_row', 10, 3 );

if(is_admin()){
	remove_action("admin_color_scheme_picker", "admin_color_scheme_picker");
}

//account page modify
add_action("wp_head", function () {
    remove_shortcode("pmpro_account");
    add_shortcode("pmpro_account", function($atts, $content=null, $code="")
	{
		global $wpdb, $pmpro_msg, $pmpro_msgt, $pmpro_levels, $current_user, $levels;

		// $atts    ::= array of attributes
		// $content ::= text within enclosing form of shortcode element
		// $code    ::= the shortcode found, when == callback name
		// examples: [pmpro_account] [pmpro_account sections="membership,profile"/]

		extract(shortcode_atts(array(
			'section' => '',
			'sections' => 'membership,profile,invoices,links'
		), $atts));

		//did they use 'section' instead of 'sections'?
		if(!empty($section))
			$sections = $section;

		//Extract the user-defined sections for the shortcode
		$sections = array_map('trim',explode(",",$sections));
		ob_start();

		//if a member is logged in, show them some info here (1. past invoices. 2. billing information with button to update.)
		$order = new MemberOrder();
		$order->getLastMemberOrder();
		$mylevels = pmpro_getMembershipLevelsForUser();
		$pmpro_levels = pmpro_getAllLevels(false, true); // just to be sure - include only the ones that allow signups
		$invoices = $wpdb->get_results("SELECT *, UNIX_TIMESTAMP(CONVERT_TZ(timestamp, '+00:00', @@global.time_zone)) as timestamp FROM $wpdb->pmpro_membership_orders WHERE user_id = '$current_user->ID' AND status NOT IN('review', 'token', 'error') ORDER BY timestamp DESC LIMIT 6");
		?>
		<div id="pmpro_account">
			<?php if(in_array('membership', $sections) || in_array('memberships', $sections)) { ?>
				<div id="pmpro_account-membership" class="col-xs-12 col-md-6 <?php echo pmpro_get_element_class( 'pmpro_box', 'pmpro_account-membership' ); ?>">
					<div class="pmpro_box_content">
						<h3><?php _e("Moje pakiety", 'paid-memberships-pro' );?></h3>
						<div class="membership_block">
							<?php if ( !empty( $mylevels ) ) {
									foreach($mylevels as $level) {
							?>
								<p><?php echo "Nazwa: <strong>".$level->name."</strong>"; ?></p>
								<p><?php _e("Suma", 'paid-memberships-pro' ); echo ": <strong>".pmpro_getLevelCost($level, true, true)."</strong>"; ?></p>
								<p><?php _e("Ważność", 'paid-memberships-pro' );
										if($level->enddate)
											$expiration_text = date_i18n( get_option( 'date_format' ), $level->enddate );
										else
											$expiration_text = "---";

											echo ": <strong>".apply_filters( 'pmpro_account_membership_expiration_text', $expiration_text, $level )."</strong>";
								?></p>
							<?php }
								} else {
									$url = pmpro_url( 'levels' );
									printf( __( "Nie masz aktywnego pakietu. <a href='%s'>Wybierz pakiet.</a>", 'paid-memberships-pro' ), $url );
								}
							?>
						</div>
					</div>
				</div> <!-- end pmpro_account-membership -->
			<?php } ?>

			<?php if(in_array('profile', $sections)) { ?>
				<div id="pmpro_account-profile" class="col-xs-12 col-md-6 mt-4 mt-md-0 <?php echo pmpro_get_element_class( 'pmpro_box', 'pmpro_account-profile' ); ?>">
					<div class="pmpro_box_content">
						<?php wp_get_current_user(); ?>
						<h3><?php _e("Moje konto", 'paid-memberships-pro' );?></h3>
						<?php if($current_user->user_firstname) { ?>
							<p><?php echo $current_user->user_firstname?> <?php echo $current_user->user_lastname?></p>
						<?php } ?>

							<?php do_action('pmpro_account_bullets_top');?>
							<p><?php _e("Nazwa użytkownika", 'paid-memberships-pro' );?>: <strong> <?php echo $current_user->user_login?></strong></p>
							<p><?php _e("E-mail", 'paid-memberships-pro' );?>: <strong> <?php echo $current_user->user_email?></strong></p>
							<p><?php _e("Nazwa firmy", 'webcost' );?>: <strong> <?php echo $current_user->firm_name?></strong></p>
							<!-- <p><?php _e("Telefon", 'webcost' );?>: <strong> <?php echo $current_user->phone?></strong></p>
							<p><?php _e("NIP", 'webcost' );?>: <strong> <?php echo $current_user->nip?></strong></p>
							<p><?php _e("Kod pocztowy", 'webcost' );?>: <strong> <?php echo $current_user->post_code?></strong></p>
							<p><?php _e("Adres", 'webcost' );?>: <strong> <?php echo $current_user->address?></strong></p> -->
							<?php do_action('pmpro_account_bullets_bottom');?>

						<div class="<?php echo pmpro_get_element_class( 'pmpro_actionlinks' ); ?>">
							<?php
								// Get the edit profile and change password links if 'Member Profile Edit Page' is set.
								if ( ! empty( pmpro_getOption( 'member_profile_edit_page_id' ) ) ) {
									$edit_profile_url = pmpro_url( 'member_profile_edit' );
									$change_password_url = add_query_arg( 'view', 'change-password', pmpro_url( 'member_profile_edit' ) );
								} elseif ( ! pmpro_block_dashboard() ) {
									$edit_profile_url = admin_url( 'profile.php' );
									$change_password_url = admin_url( 'profile.php' );
								}

								// Build the links to return.
								$pmpro_profile_action_links = array();
								if ( ! empty( $edit_profile_url) ) {
									$pmpro_profile_action_links['edit-profile'] = sprintf( '<a id="pmpro_actionlink-profile" href="%s">%s</a>', esc_url( $edit_profile_url ), esc_html__( 'Edytuj profil', 'paid-memberships-pro' ) );
								}

								if ( ! empty( $change_password_url ) ) {
									$pmpro_profile_action_links['change-password'] = sprintf( '<a id="pmpro_actionlink-change-password" href="%s">%s</a>', esc_url( $change_password_url ), esc_html__( 'Zmień hasło', 'paid-memberships-pro' ) );
								}

								$pmpro_profile_action_links['delete'] = do_shortcode( '[plugin_delete_me /]' );

								$pmpro_profile_action_links['logout'] = sprintf( '<a id="pmpro_actionlink-logout" href="%s">%s</a>', esc_url( get_permalink(UM()->options()->get('core_logout')) ), esc_html__( 'Wyloguj', 'paid-memberships-pro' ) );

								$pmpro_profile_action_links = apply_filters( 'pmpro_account_profile_action_links', $pmpro_profile_action_links );

								$allowed_html = array(
									'a' => array (
										'class' => array(),
										'href' => array(),
										'id' => array(),
										'target' => array(),
										'title' => array(),
									),
								);
								echo wp_kses( implode( pmpro_actions_nav_separator(), $pmpro_profile_action_links ), $allowed_html );
							?>
						</div>
					</div>
				</div> <!-- end pmpro_account-profile -->
			<?php } ?>
			<div id="account-orders" class="col-xs-12 mt-4 pmpro_box">
				<div class="pmpro_box_content">
					<h3>Moje płatności</h3>
					<?php
						$args = array(
							'customer_id' => $current_user->ID
						);
						$orders = wc_get_orders($args);
						if (count($orders) > 0) {
							?>
							<table id="account_order">
								<thead>
									<tr>
										<th>Data</th>
										<th>Suma</th>
										<th>Pakiet</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
							<?php
							foreach ($orders as $ko=>$order) {
								$product = '';
								foreach ( $items = $order->get_items() as $item_id => $item ) {
									$product .= $item->get_name().(($item_id < count($items) - 1) ? ',' : '');
								}
								echo '<tr>
										<td data-label="Data" style="min-width:170px">'.$order->get_date_created()->date("d-m-Y g:i:s").'</td>
										<td data-label="Suma" style="min-width:80px">'.$order->get_total().' zł</td>
										<td data-label="Pakiet" style="min-width:170px">'.$product.'</td>
										<td data-label="Status" style="min-width:170px">'.__(mb_ucfirst($order->get_status()),'woocommerce').(($order->get_status() == 'pending') ? ". <a href='".$order->get_checkout_payment_url()."'>Płacić</a>" : "").'</td>
									  </tr>';
							}
							?>
								</tbody>
							</table>
							<?php
						} else {
							?>
							Nie masz płatności. <a href="<?php echo get_permalink(661); ?>">Wybierz pakiet.</a>
							<?php
						}
					?>
				</div>
			</div>

			<div id="account-firms" class="col-xs-12 mt-4 pmpro_box">
				<div class="pmpro_box_content">
					<?php
						$args = array(
							'post_type' => 'firmy',
							'post_status' => 'publish',
							'orderby'     => 'date',
							'order'       => 'DESC',
							'author'	  => $current_user->ID,
							'numberposts' => 1,
						);
						$posts = get_posts($args);
					?>
					<h3>Moja firma <?php if (count($posts) > 0) { ?><a href="<?php echo get_permalink(661); ?>?id=<?php echo $posts[0]->ID; ?>" class="btn purple account_button ml-3">Edytuj</a><a data-toggle="modal" data-target="#delete_firm" class="btn red account_button ml-3">Usunąć</a><?php } ?></h3>
					<?php
						if (count($posts) > 0) {
							?>
							<div class="modal" id="delete_firm" tabindex="-1">
								<div class="modal-dialog modal-dialog-centered">
									<div class="modal-content">
										<div class="modal-body">
											Usunięcia firmy nie można cofnąć. Jesteś pewny?
											<div class="button-block">
												<button type="button" class="btn" data-dismiss="modal">
													Odrzucać
												</button>
												<button type="button" class="btn purple" onclick="delete_firm(<?php echo $posts[0]->ID; ?>);">
													Usunąć
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="account_firm_info">
								<?php
								foreach ($posts as $post) { ?>
									<div class="container mt-4">
										<div class="row">
											<div class="col-xs-12 col-md-4 left-column mb-4 mb-mb-0">
												<?php
													$img = get_the_post_thumbnail_url( $post->ID, 'medium' );
													$containerClass = '';
													if (!$img) {
														$img = get_template_directory_uri().'/assets/img/no_service_image_615x435.png';
														$containerClass = ' no-image';
													}
												?>
												<div class="post-thumbnail<?php echo $containerClass; ?>">
													<img src="<?php echo $img; ?>" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" loading="lazy">
												</div>
											</div>
											<div class="col-xs-12 col-md-8 right-column">
												<?php
													$meta = get_post_meta($post->ID);

													$res = '';
													if (isset($meta['Adres']) && strlen($meta['Adres'][0]) > 0)
														$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_POLOZENIE.png"><span>'.addUrlToAddress($meta['Adres'][0]).'</span></span>';
													else
														$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_POLOZENIE.png"><span><small>Brak adresu</small></span></span>';
													if (isset($meta['numer telefonu']) && strlen($meta['numer telefonu'][0]) > 0)
														$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_TEL.png"><span>'.$meta['numer telefonu'][0].'</span></span>';
													else
														$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_TEL.png"><span><small>Brak telefonu</small></span></span>';
													if (isset($meta['e-mail']) && strlen($meta['e-mail'][0]) > 0)
														$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_POCZTA.png"><span>'.$meta['e-mail'][0].'</span></span>';
													else
														$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_POCZTA.png"><span><small>Brak e-maila</small></span></span>';
													if (isset($meta['strona_internetowa']) && strlen($meta['strona_internetowa'][0]) > 0)
														$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_WWW.png"><span>'.$meta['strona_internetowa'][0].'</span></span>';
													else
														$res .= '<span><img src="'.get_template_directory_uri().'/assets/img/IK_WWW.png"><span><small>Brak strony internetowej</small></span></span>';

													$services = [];
													foreach ($meta as $km => $m) {
														if (strpos($km, '_opis_uslugi') !== false && strpos($km, '_usluga_') === false && strlen($m[0]) > 0 )
															$services[preg_replace('/[^0-9.]+/', '', $km)]['text'] = $m[0];
														if (strpos($km, '_obraz_uslugi') !== false && strpos($km, '_usluga_') === false && strlen($m[0]) > 0 )
															$services[preg_replace('/[^0-9.]+/', '', $km)]['image'] = wp_get_attachment_image_url( $m[0], 'medium' );
													}
												?>
												<h4 class="entry-title"><?php echo $post->post_title; ?></h4>
												<?php
													if (strlen($res) > 0) {
												?>
													<div class="entry-meta">
														<?php echo $res; ?>
													</div>
												<?php
													}
												?>
											</div>
											<?php
												$tags = wp_get_post_tags( $post->ID );
												if (count($tags) > 0) {
													?>
														<div class="tags_block col-xs-12">
															<h4 class="entry-title mt-4">Tagi</h4>
															<?php

																if (is_array($tags) && count($tags) > 0) {
																	foreach ($tags as $ktag=>$tag) {
																		if ($tag->name != 'blog' && $tag->name != 'home_text' &&  $tag->name != 'promo')
																			echo "<span class='badge'>".$tag->name."</span>";
																		else
																			unset($tags[$ktag]);
																	}
																	if (count($tags) == 0)
																		echo "<small>Brak tagów</small>";
																} else {
																	echo "<small>Brak tagów</small>";
																}

															?>
														</div>
													<?php
												}
											?>
										</div>
									</div>

									<div class="full_info container">
										<h4 class="entry-title mt-4">Krotki opis</h4>
										<?php
											if (isset($meta['krotki_opis']) && strlen($meta['krotki_opis'][0]) > 1)
												echo strip_tags($meta['krotki_opis'][0]);
											else
												echo "<small>Brak krótkiego opisu</small>";
										?>
										<h4 class="entry-title mt-3">Informacje</h4>
										<div class="full_info_content">
											<?php
												if (strlen(strip_tags($post->post_content)) > 1)
													echo strip_tags($post->post_content);
												else
													echo "<small>Brak opisu</small>";
											?>
										</div>
									</div>

								<?php if (count($services) > 0) { ?>
									<div class="service_info container">
										<h4 class="entry-title mt-4">Usługi</h4>
										<div class="row">
											<?php foreach ($services as $ks => $s) { ?>
												<article id="service_<?php echo $ks ?>" class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
													<a href="/usluga/<?php echo $s['text']?>">
														<div class="img_container<?php if (!isset($s['image'])) echo ' no_image'; ?>">
															<img src="<?php if (isset($s['image'])) echo $s['image']; else echo get_template_directory_uri()."/assets/img/no_service_image_615x435.png"; ?>" alt="">
														</div>
														<span class="service_text">
															<?php echo $s['text']; ?>
														</span>
													</a>
												</article>
											<?php } ?>
										</div>
									</div>
								<?php }
								}
							?>
							</div>
							<?php
						} else {
							?>
								Nie masz firm. <a href="<?php echo get_permalink(661); ?>"><?php if (!empty($mylevels)) echo "Dodaj firmę"; else echo "Wybierz pakiet"; ?>.</a>
							<?php
						}
					?>
				</div>
			</div>
		</div> <!-- end pmpro_account -->
		<?php

		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	});
	remove_shortcode("pmpro_member_profile_edit");
	add_shortcode("pmpro_member_profile_edit", function ( $atts, $content=null, $code='' ) {
		// $content ::= text within enclosing form of shortcode element
		// $code    ::= the shortcode found, when == callback name
		// examples: [pmpro_member_profile_edit]

		ob_start();

		// Get the current action for the view.
		if ( ! empty( $_REQUEST[ 'view' ] ) ) {
			$view = sanitize_text_field( $_REQUEST[ 'view' ] );
		} else {
			$view = NULL;
		}

		if ( ! empty( $view ) && $view == 'change-password' ) {
			// Display the Change Password form.
			pmpro_change_password_form();
		} else {
			// Display the Member Profile Edit form.
			custom_pmpro_member_profile_edit_form();
		}

		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	});
});


function custom_pmpro_member_profile_edit_form() {
	global $current_user;

	if ( ! is_user_logged_in() ) {
		echo '<div class="' . pmpro_get_element_class( 'pmpro_message pmpro_alert', 'pmpro_alert' ) . '"><a href="' . esc_url( pmpro_login_url() ) . '">' . esc_html__( 'Log in to edit your profile.', 'paid-memberships-pro' ) . '</a></div>';
		return;
	}

	// Saving profile updates.
	if ( isset( $_POST['action'] ) && $_POST['action'] == 'update-profile' && $current_user->ID == $_POST['user_id'] && wp_verify_nonce( $_POST['update_user_nonce'], 'update-user_' . $current_user->ID ) ) {
		$update           = true;
		$user     		  = new stdClass;
		$user->ID         = $_POST[ 'user_id' ];
		do_action( 'pmpro_personal_options_update', $user->ID );
	} else {
		$update = false;
	}

	if ( $update ) {

		$errors = array();

		// Get all values from the $_POST, sanitize them, and build the $user object.
		if ( isset( $_POST['user_email'] ) ) {
			$user->user_email = sanitize_text_field( wp_unslash( $_POST['user_email'] ) );
		}
		if ( isset( $_POST['first_name'] ) ) {
			$user->first_name = sanitize_text_field( $_POST['first_name'] );
		}
		if ( isset( $_POST['last_name'] ) ) {
			$user->last_name = sanitize_text_field( $_POST['last_name'] );
		}
		if ( isset( $_POST['display_name'] ) ) {
			$user->display_name = sanitize_text_field( $_POST['display_name'] );
			$user->nickname = $user->display_name;
		}
		// if ( isset( $_POST['firm_name'] ) ) {
		// 	$user->firm_name = sanitize_text_field( $_POST['firm_name'] );
		// }
		// if ( isset( $_POST['phone'] ) ) {
		// 	$user->phone = sanitize_text_field( $_POST['phone'] );
		// }
		// if ( isset( $_POST['nip'] ) ) {
		// 	$user->nip = sanitize_text_field( $_POST['nip'] );
		// }
		// if ( isset( $_POST['post_code'] ) ) {
		// 	$user->post_code = sanitize_text_field( $_POST['post_code'] );
		// }
		// if ( isset( $_POST['address'] ) ) {
		// 	$user->address = sanitize_text_field( $_POST['address'] );
		// }

		// Validate display name.
		if ( empty( $user->display_name ) ) {
			$errors[] = __( 'Please enter a display name.', 'paid-memberships-pro' );
		}

		// Don't allow admins to change their email address.
		if ( current_user_can( 'manage_options' ) ) {
			$user->user_email = $current_user->user_email;
		}

		// Validate email address.
		if ( empty( $user->user_email ) ) {
			$errors[] = __( 'Please enter an email address.', 'paid-memberships-pro' );
		} elseif ( ! is_email( $user->user_email ) ) {
			$errors[] = __( 'The email address isn&#8217;t correct.', 'paid-memberships-pro' );
		} else {
			$owner_id = email_exists( $user->user_email );
			if ( $owner_id && ( ! $update || ( $owner_id != $user->ID ) ) ) {
				$errors[] = __( 'This email is already registered, please choose another one.', 'paid-memberships-pro' );
			}
		}

		/**
		 * Fires before member profile update errors are returned.
		 *
		 * @param $errors WP_Error object (passed by reference).
		 * @param $update Whether this is a user update.
		 * @param $user   User object (passed by reference).
		 */
		do_action_ref_array( 'pmpro_user_profile_update_errors', array( &$errors, $update, &$user ) );

		// Show error messages.
		if ( ! empty( $errors ) ) { ?>
			<div class="<?php echo pmpro_get_element_class( 'pmpro_message pmpro_error', 'pmpro_error' ); ?>">
				<?php
					foreach ( $errors as $key => $value ) {
						echo '<p>' . $value . '</p>';
					}
				?>
			</div>
		<?php } else {
			// Save updated profile fields.
			wp_update_user( $user );
			?>
			<div class="<?php echo pmpro_get_element_class( 'pmpro_message pmpro_success', 'pmpro_success' ); ?>">
				<?php _e( 'Your profile has been updated.', 'paid-memberships-pro' ); ?>
			</div>
		<?php }
	} else {
		// Doing this so fields are set to new values after being submitted.
		$user = $current_user;
	}
	?>
	<div class="<?php echo pmpro_get_element_class( 'pmpro_member_profile_edit_wrap' ); ?>">
		<form id="member-profile-edit" class="<?php echo pmpro_get_element_class( 'pmpro_form' ); ?>" action="" method="post"
			<?php
				/**
				 * Fires inside the member-profile-edit form tag in the pmpro_member_profile_edit_form function.
				 *
				 * @since 2.4.1
				 */
				do_action( 'pmpro_member_profile_edit_form_tag' );
			?>
		>

			<?php wp_nonce_field( 'update-user_' . $current_user->ID, 'update_user_nonce' ); ?>

			<?php
			$user_fields = apply_filters( 'pmpro_member_profile_edit_user_object_fields',
				array(
					'first_name'	=> __( 'First Name', 'paid-memberships-pro' ),
					'last_name'		=> __( 'Last Name', 'paid-memberships-pro' ),
					'display_name'	=> __( 'Display name publicly as', 'paid-memberships-pro' ),
					'user_email'	=> __( 'Email', 'paid-memberships-pro' ),
					// 'firm_name'		=> __('Nazwa firmy', 'webcost'),
					// 'phone'		=> __('Telefon', 'webcost'),
					// 'nip'		=> __('NIP', 'webcost'),
					// 'post_code'		=> __('Kod pocztowy', 'webcost'),
					// 'address'		=> __('Adres', 'webcost'),
				)
			);
			?>

			<div class="<?php echo pmpro_get_element_class( 'pmpro_checkout_box-user' ); ?>">
				<div class="<?php echo pmpro_get_element_class( 'pmpro_member_profile_edit-fields' ); ?>">
				<?php foreach ( $user_fields as $field_key => $label ) { ?>
					<div class="<?php echo pmpro_get_element_class( 'pmpro_member_profile_edit-field pmpro_member_profile_edit-field- ' . $field_key, 'pmpro_member_profile_edit-field- ' . $field_key ); ?>">
						<label for="<?php echo esc_attr( $field_key ); ?>"><?php esc_html_e( $label ); ?></label>
						<?php if ( current_user_can( 'manage_options' ) && $field_key === 'user_email' ) { ?>
							<input type="text" readonly="readonly" name="user_email" id="user_email" value="<?php echo esc_attr( $user->user_email ); ?>" class="<?php echo pmpro_get_element_class( 'input', 'user_email' ); ?>" />
							<p class="<?php echo pmpro_get_element_class( 'lite' ); ?>"><?php esc_html_e( 'Site administrators must use the WordPress dashboard to update their email address.', 'paid-memberships-pro' ); ?></p>
						<?php } else { ?>
							<input type="text" name="<?php echo esc_attr( $field_key ); ?>" id="<?php echo esc_attr( $field_key ); ?>" value="<?php echo esc_attr( stripslashes( $user->{$field_key} ) ); ?>" class="<?php echo pmpro_get_element_class( 'input', $field_key ); ?>" />
						<?php } ?>
	            	</div>
				<?php } ?>
				</div> <!-- end pmpro_member_profile_edit-fields -->
			</div> <!-- end pmpro_checkout_box-user -->

			<?php
				/**
				 * Fires after the default Your Member Profile fields.
				 *
				 * @since 2.3
				 *
				 * @param WP_User $current_user The current WP_User object.
				 */
				do_action( 'pmpro_show_user_profile', $current_user );
			?>
			<input type="hidden" name="action" value="update-profile" />
			<input type="hidden" name="user_id" value="<?php echo $current_user->ID; ?>" />
			<div class="<?php echo pmpro_get_element_class( 'pmpro_submit' ); ?>">
				<hr />
				<input type="submit" name="submit" class="<?php echo pmpro_get_element_class( 'pmpro_btn pmpro_btn-submit', 'pmpro_btn-submit' ); ?>" value="<?php _e( 'Update Profile', 'paid-memberships-pro' );?>" />
				<input type="button" name="cancel" class="<?php echo pmpro_get_element_class( 'pmpro_btn pmpro_btn-cancel', 'pmpro_btn-cancel' ); ?>" value="<?php _e( 'Cancel', 'paid-memberships-pro' );?>" onclick="location.href='<?php echo pmpro_url( 'account'); ?>';" />
			</div>
		</form>
	</div> <!-- end pmpro_member_profile_edit_wrap -->
	<?php
}

//add custom fields to user info in admin page
function tm_additional_contact_methods( $fields, $user ) {
	$fields['firm_name'] = 'Nazwa firmy';
	$fields['phone'] = 'Telefon';
	$fields['nip'] = 'NIP';
	$fields['post_code'] = 'Kod pocztowy';
	$fields['address'] = 'Adres';
	return $fields;
}
add_filter( 'user_contactmethods', 'tm_additional_contact_methods', 10, 2 );

//remove shipping/billing from account info
add_filter( 'woocommerce_customer_meta_fields', 'remove_fields_woo' );
function remove_fields_woo( $show_fields ) {
    unset( $show_fields['shipping'] );
    unset( $show_fields['billing'] );
    return $show_fields;
}

//remove menu items
add_action( 'admin_menu', 'remove_menus', 9999 );
function remove_menus(){
	remove_menu_page( 'index.php' );
	remove_menu_page( 'edit-comments.php' );
	remove_menu_page( 'woocommerce-marketing' );
	remove_menu_page( 'wp-mail-smtp' );
	remove_submenu_page( 'pmpro-dashboard', 'pmpro-license' );
	remove_submenu_page( 'pmpro-dashboard', 'pmpro-addons' );
	remove_submenu_page( 'pmpro-dashboard', 'pmpro-reports' );
	remove_submenu_page( 'pmpro-dashboard', 'pmpro-orders' );
	remove_submenu_page( 'pmpro-dashboard', 'pmpro-dashboard' );
	remove_submenu_page( 'ultimatemember', 'ultimatemember-extensions' );
	remove_submenu_page( 'ultimatemember', 'um_roles' );
	remove_submenu_page( 'ultimatemember', 'ultimatemember' );
	remove_submenu_page( 'woocommerce', 'p24-multi-currency' );
	remove_submenu_page( 'woocommerce', 'p24-order-status' );
	remove_submenu_page( 'woocommerce', 'p24-subscription' );
	remove_submenu_page( 'woocommerce', 'wc-admin&path=/customers' );
	remove_submenu_page( 'woocommerce', 'wc-addons' );
	remove_submenu_page( 'wpforms-overview', 'wpforms-entries' );
	remove_submenu_page( 'wpforms-overview', 'wpforms-addons' );
	remove_submenu_page( 'wpforms-overview', 'wpforms-analytics' );
	remove_submenu_page( 'wpforms-overview', 'wpforms-about' );
	remove_submenu_page( 'wpforms-overview', 'wpforms-community' );
	remove_submenu_page( 'smart-slider3', 'smart-slider3-help' );
	remove_submenu_page( 'smart-slider3', 'smart-slider3-go-pro' );
}

//change status to complete after pay
add_action( 'woocommerce_order_status_processing', 'processing_to_completed');
function processing_to_completed($order_id){
    $order = new WC_Order($order_id);
    $order->update_status('completed');
}

//remove link from row actions
function remove_link($actions, $page_object) {
	unset($actions['frontend_profile']);
	unset($actions['view']);
   	return $actions;
}
add_filter('um_admin_user_row_actions', 'remove_link', 10, 2);

//remove role
function wps_remove_role() {
    remove_role( 'editor' );
    remove_role( 'contributor' );
    remove_role( 'subscriber' );
    remove_role( 'shop_manager' );
    remove_role( 'customer' );
}
add_action( 'init', 'wps_remove_role' );


function custom_pmpro_membership_history_profile_fields( $user ) {
	global $current_user;
	$membership_level_capability = apply_filters( 'pmpro_edit_member_capability', 'manage_options' );

	if ( ! current_user_can( $membership_level_capability ) ) {
		return false;
	}

	global $wpdb;

	//Show all invoices for user
	$invoices = $wpdb->get_results("SELECT mo.*, UNIX_TIMESTAMP(mo.timestamp) as timestamp, du.code_id as code_id FROM $wpdb->pmpro_membership_orders mo LEFT JOIN $wpdb->pmpro_discount_codes_uses du ON mo.id = du.order_id WHERE mo.user_id = '$user->ID' ORDER BY mo.timestamp DESC");

	$levelshistory = $wpdb->get_results("SELECT * FROM $wpdb->pmpro_memberships_users WHERE user_id = '$user->ID' ORDER BY id DESC");

	$totalvalue = $wpdb->get_var("SELECT SUM(total) FROM $wpdb->pmpro_membership_orders WHERE user_id = '$user->ID' AND status NOT IN('token','review','pending','error','refunded')");

	if ( $invoices || $levelshistory ) { ?>
		<hr />
		<h4>Historia zakupów</h4>
		<h4><strong>Pełna kwota</strong> <?php echo pmpro_formatPrice( $totalvalue ); ?></h4>
		<div id="member-history-memberships" class="widgets-holder-wrap">
		<?php if ( $levelshistory ) { ?>
			<table class="wp-list-table widefat striped fixed" width="100%" cellpadding="0" cellspacing="0" border="0">
			<thead>
				<tr>
					<th><?php esc_html_e( 'Level ID', 'paid-memberships-pro' ); ?>
					<th><?php esc_html_e( 'Level', 'paid-memberships-pro' ); ?></th>
					<th><?php esc_html_e( 'Start Date', 'paid-memberships-pro' ); ?></th>
					<th><?php esc_html_e( 'Date Modified', 'paid-memberships-pro' ); ?></th>
					<th><?php esc_html_e( 'End Date', 'paid-memberships-pro' ); ?></th>
					<th><?php esc_html_e( 'Level Cost', 'paid-memberships-pro' ); ?></th>
					<th><?php esc_html_e( 'Status', 'paid-memberships-pro' ); ?></th>
					<?php do_action( 'pmpromh_member_history_extra_cols_header' ); ?>
				</tr>
			</thead>
			<tbody>
			<?php
				foreach ( $levelshistory as $levelhistory ) {
					$level = pmpro_getLevel( $levelhistory->membership_id );

					if ( $levelhistory->enddate === null || $levelhistory->enddate == '0000-00-00 00:00:00' ) {
						$levelhistory->enddate = __( 'Never', 'paid-memberships-pro' );
					} else {
						$levelhistory->enddate = date_i18n( get_option( 'date_format'), strtotime( $levelhistory->enddate ) );
					} ?>
					<tr>
						<td><?php if ( ! empty( $level ) ) { echo $level->id; } else { esc_html_e( 'N/A', 'paid-memberships-pro' ); } ?></td>
						<td><?php if ( ! empty( $level ) ) { echo $level->name; } else { esc_html_e( 'N/A', 'paid-memberships-pro' ); } ?></td>
						<td><?php echo ( $levelhistory->startdate === '0000-00-00 00:00:00' ? __('N/A', 'paid-memberships-pro') : date_i18n( get_option( 'date_format' ), strtotime( $levelhistory->startdate ) ) ); ?></td>
						<td><?php echo date_i18n( get_option( 'date_format'), strtotime( $levelhistory->modified ) ); ?></td>
						<td><?php echo esc_html( $levelhistory->enddate ); ?></td>
						<td><?php echo pmpro_getLevelCost( $levelhistory, true, true ); ?></td>
						<td>
							<?php
								if ( empty( $levelhistory->status ) ) {
									echo '-';
								} else {
									echo esc_html( $levelhistory->status );
								}
							?>
						</td>
						<?php do_action( 'pmpromh_member_history_extra_cols_body', $user, $level ); ?>
					</tr>
					<?php
				}
			?>
			</tbody>
			</table>
			<?php } else {
				esc_html_e( 'No membership history found.', 'paid-memberships-pro');
			} ?>
		</div>
		<?php
	}
}
remove_action('edit_user_profile', 'pmpro_membership_history_profile_fields');
remove_action('show_user_profile', 'pmpro_membership_history_profile_fields');
add_action('edit_user_profile', 'custom_pmpro_membership_history_profile_fields');
add_action('show_user_profile', 'custom_pmpro_membership_history_profile_fields');

function search_distinct() {
    return "DISTINCT";
}
add_filter('posts_distinct', 'search_distinct');

function the_webcost_logo($unlink = false) {
	if (!$unlink)
		echo '<a href="'.site_url().'" class="custom-logo-link" rel="home"><img width="346" src="'.get_template_directory_uri().'/assets/img/logo.svg" class="custom-logo" alt="'.get_bloginfo('name').'"></a>';
	else
		echo '<a href="'.site_url().'" class="custom-logo-link" rel="nofollow"><img width="346" src="'.get_template_directory_uri().'/assets/img/logo.svg" class="custom-logo" alt="'.get_bloginfo('name').'"></a>';
}

function checkRefer() {
	if (isset($_COOKIE['pakiet'])) {
		$id = $_COOKIE['pakiet'];
		setcookie("pakiet","",time()-3600,"/");
		header('Location: '.get_permalink(661).'?add-to-cart='.$id);
	}
}

add_action( 'rest_api_init', 'geo_search_route' );
function geo_search_route() {
    register_rest_route( 'geo', 'search/(?P<st>(.+?)+)/(?P<w>.+)', array(
                    'methods' => 'GET',
					'args' => ['st', 'type'],
                    'callback' => 'geo_search',
                )
            );
}

function geo_search( $search ) {

	if (isset($search['st']) && strlen(trim($search['st'])) > 1) {
		global $wpdb;
		$search['st'] = urldecode(trim($search['st']));
		$result = array('status' => 'empty');
		switch ($search['w']) {
			case 'all':
				if ($find = $wpdb->get_results('
							SELECT l.name as m,l2.name as p from '.$wpdb->prefix.'geo_list l
							JOIN '.$wpdb->prefix.'geo_list l2 ON l2.id=l.id_parent
							where l.name like "%'.urldecode($search['st']).'%"')) {
					$city = array();
					foreach($find as $item) {
						$city[] = $item->m.' <small>('.$item->p.' powiat)</small>';
					}
					$result = array('status' => 'success', 'city' => $city);
				}
				break;
			case 'throw-all':
				$city = array();
				if ($find = $wpdb->get_results('
							SELECT l.name as w FROM '.$wpdb->prefix.'geo_list l
							where l.slug like "%'.urldecode(makeSlug($search['st'])).'%" AND l.id_type = 1')) {
					foreach($find as $item) {
						$city[] = str_ireplace(urldecode($search['st']), '<strong>'.urldecode($search['st']).'</strong>', $item->w);
					}

				}
				if ($find = $wpdb->get_results('
							SELECT l.name as p, l2.name as w FROM '.$wpdb->prefix.'geo_list l
							join '.$wpdb->prefix.'geo_list l2 on l2.id=l.id_parent
							where l.slug like "%'.urldecode(makeSlug($search['st'])).'%" and l.id_type = 2')) {
					foreach($find as $item) {
						$city[] = $item->w.', '.str_ireplace(urldecode($search['st']), '<strong>'.urldecode($search['st']).'</strong>', $item->p);
					}

				}
				if ($find = $wpdb->get_results('
							SELECT l.name as m, l2.name as p, l3.name as w FROM '.$wpdb->prefix.'geo_list l
							join '.$wpdb->prefix.'geo_list l2 on l2.id=l.id_parent
							join '.$wpdb->prefix.'geo_list l3 on l3.id=l2.id_parent
							where l.slug like "%'.urldecode(makeSlug($search['st'])).'%" and l.id_type=3')) {
					foreach($find as $item) {
						$city[] = $item->w.', '.$item->p.', '.str_ireplace(urldecode($search['st']), '<strong>'.urldecode($search['st']).'</strong>', $item->m);
					}

				}
				$result = array('status' => 'success', 'city' => $city);
				break;
			default:
				if ($find = $wpdb->get_results('
							SELECT l.name as m,l2.name as p from '.$wpdb->prefix.'geo_list l
							JOIN '.$wpdb->prefix.'geo_list l2 ON l2.id=l.id_parent
							JOIN '.$wpdb->prefix.'geo_list l3 ON l3.id = l2.id_parent
							where l3.name = "'.urldecode($search['w']).'" and l.name like "%'.urldecode($search['st']).'%"')) {
					$city = array();
					foreach($find as $item) {
						$city[] = $item->m.' <small>('.$item->p.' powiat)</small>';
					}
					$result = array('status' => 'success', 'city' => $city);
				}
				break;
		}

		return rest_ensure_response( $result );
	} else {
		return rest_ensure_response( array('response' => 'min.2') );
	}
}

add_action('wp_enqueue_scripts', 'mytheme_scripts');

function mytheme_scripts() {
  wp_dequeue_script( 'pmpro_login' );
  wp_deregister_script( 'pmpro_login' );
  wp_enqueue_script('pmpro_login', get_template_directory_uri() . '/assets/js/plugin/pmpro/pmpro-login.js');
}

// Register Sidebars
function footer_widget() {

    $args = array(
        'id'            => 'footer_widget',
        'name'          => __( 'Footer Widget Area', 'text_domain' ),
        'description'   => __( 'A footer widget area', 'text_domain' ),
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
    );
    register_sidebar( $args );

}
add_action( 'widgets_init', 'footer_widget' );

// function true_rewrite_search_results_permalink() {
// 	global $wp_rewrite;
// 	// обязательно проверим, включены ли чпу, чтобы не закосячить весь поиск
// 	if ( !isset( $wp_rewrite ) || !is_object( $wp_rewrite ) || !$wp_rewrite->using_permalinks() )
// 		return;
// 	if ( is_search() && !is_admin() && strpos( $_SERVER['REQUEST_URI'], "/search/") === false && ! empty( $_GET['s'] ) ) {
// 		wp_redirect( site_url() . "/search/" . urlencode( get_query_var( 's' ) ) );
// 		exit;
// 	}
// }

// add_action( 'template_redirect', 'true_rewrite_search_results_permalink' );

// // вторая функция нужна для поддержки русских букв и специальных символов
// function true_urldecode_s($query) {
// 	if (is_search()) {
// 		$query->query_vars['s'] = urldecode( $query->query_vars['s'] );
// 	}
// 	return $query;
// }

// add_filter('parse_query', 'true_urldecode_s');

add_filter('wpseo_title', function($title){
	$url_path = trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/');
	$path = explode('/', $url_path);
	$template = explode('/', get_page_template());
	if ($template) {
		switch ($template[count($template)-1]) {
			case 'page-city.php':
				$tmp = getLocationInfo();
				if ($tmp['w']) {
					$title = 'Firmy w województwie '.$tmp['w']->name . ' - ' . get_bloginfo();
				} elseif ($tmp['c']) {
					$title = 'Firmy w mieście '.$tmp['c']->name . ' - ' . get_bloginfo();
				}
				break;
		}
	}
	if ( strpos($url_path, 'usluga') !== false && strpos($url_path, 'usluga') == 0) {
		$info = getServiceInfo();
		$title = $info['service'] . ($info['region'] ? ($info['service'] ? ' - ' : '') . $info['region'] : '') . ' - ' . get_bloginfo();
	}
	return $title;
}, 10, 1);

add_filter('wpseo_metadesc', function($metadesc){
	$url_path = trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/');
	$path = explode('/', $url_path);
	$template = explode('/', get_page_template());
	if ($template) {
		switch ($template[count($template)-1]) {
			case 'page-city.php':
				$tmp = getLocationInfo();
				if ($tmp['w']) {
					$metadesc = 'Firmy w województwie '.$tmp['w']->name . ' - ' . get_bloginfo();
				} elseif ($tmp['c']) {
					$metadesc = 'Firmy w mieście '.$tmp['c']->name . ' - ' . get_bloginfo();
				}
				break;
		}
	}
	if ( strpos($url_path, 'usluga') !== false && strpos($url_path, 'usluga') == 0) {
		$info = getServiceInfo();
		$metadesc = 'Wykaz firm i instytucji oferujących '.$info['service'].($info['region'] ? ' w miejscowości '.$info['region'] : '').'. Sprawdź dane firmowe i mapkę dojazdu.';
	}
	return $metadesc;
}, 10, 1);

function getServiceInfo() {
	global $wpdb;

	$service_name_full = '';
	$region_name_full = '';

	$url = add_query_arg( NULL, NULL );
	$service_name = urldecode(str_replace('/usluga/', '', preg_replace('/\?(.+?)$/m','',$url)));
	$region_name = '';

	$service_name = explode('-region-',$service_name);
	if (is_array($service_name)) {
		switch (count($service_name)) {
			case 1:
				$service_name = $service_name[0];
				break;
			default:
				if (strlen($service_name[1]) > 0)
					$region_name = $service_name[1];
				$service_name = $service_name[0];
				break;
		}
	}

	$service_name = mb_strtolower($service_name, 'UTF-8');
	$sl = $wpdb->get_row('SELECT meta_value FROM '.$wpdb->prefix.'postmeta WHERE meta_key = CONCAT((SELECT REPLACE((SELECT meta_key FROM '.$wpdb->prefix.'postmeta p WHERE meta_value = "'.$service_name.'" AND meta_key LIKE "%_slug" LIMIT 1),"_slug", "")), "_opis_uslugi") AND post_id = (SELECT post_id FROM '.$wpdb->prefix.'postmeta p WHERE meta_value = "'.$service_name.'" LIMIT 1)');
	if ($sl)
		$service_name_full = $sl->meta_value;

	$region_name = mb_strtolower($region_name, 'UTF-8');
	$r_sl = $wpdb->get_row('select meta_value from '.$wpdb->prefix.'postmeta where post_id = (SELECT post_id FROM '.$wpdb->prefix.'postmeta WHERE meta_value = "'.$region_name.'" AND meta_key = "region_slug" LIMIT 1) AND meta_key="region"');
	if ($r_sl) {
		$region_name_full = $r_sl->meta_value;
	}

	return [
		'service' => $service_name_full,
		'region' => $region_name_full
	];
}

function getLocationInfo() {
	$city = add_query_arg( NULL, NULL );
	$city = str_replace('/', '', $city);
	global $wpdb;
	$db_name_woew = $wpdb->get_row('Select name from '.$wpdb->prefix.'geo_list where id_type = 1 and slug = "'.$city.'"');
	$db_name_city = $wpdb->get_row('Select name from '.$wpdb->prefix.'geo_list where id_type = 3 and slug = "'.$city.'"');
	if ($db_name_woew) {
		$title = 'Firmy w województwie '.$db_name_woew->name . ' - ' . get_bloginfo();
	} elseif ($db_name_city) {
		$title = 'Firmy w mieście '.$db_name_city->name . ' - ' . get_bloginfo();
	}
	return ['w' => $db_name_woew, 'c' => $db_name_city];
}

add_action('init', function() {
	global $wpdb;
	global $woew_list;
	global $city_list;

	$tmpW = $wpdb->get_results('Select lower(name) as name,slug from '.$wpdb->prefix.'geo_list where id_type = 1 group by slug');
	if ($tmpW) {
		$woew_list = [];
		foreach ($tmpW as $w) {
			$woew_list[$w->slug] = $w->name;
		}

	}
	$tmpC = $wpdb->get_results('Select lower(name) as name,slug from '.$wpdb->prefix.'geo_list where id_type = 3 group by slug');
	if ($tmpC) {
		$city_list = [];
		foreach ($tmpC as $c) {
			$city_list[$c->slug] = $c->name;
		}
	}


	$url_path = trim(parse_url(add_query_arg(array()), PHP_URL_PATH), '/');
	$path = explode('/', $url_path);
	if ($path[0] && (isset($woew_list[$path[0]]) || isset($city_list[$path[0]]))) {
		$load = locate_template('page-city.php', true);
	   	if ($load) {
		  	exit();
	   	}
	}
	if ( strpos($url_path, 'usluga') !== false && strpos($url_path, 'usluga') == 0) {
	   	$load = locate_template('template-service.php', true);
	   	if ($load) {
		  	exit();
	   	}
	}
	if ( strpos($url_path, 'category') !== false && strpos($url_path, 'category') == 0 && count($path) > 2) {
		global $wpdb;
		if ($wpdb->get_row('select * from '.$wpdb->prefix.'geo_list where slug = "'.$path[count($path) - 1].'"')) {
			$load = locate_template('template-category-in-place.php', true);
			if ($load) {
			   exit();
			}
		}
	}
});


add_action( 'acf/save_post', 'action_function_name_5888' );
function action_function_name_5888(){
	if (count($_POST) > 0) {
		if (isset($_POST['acf'])) {
			$polskie = array(',', ' - ',' ','ę', 'Ę', 'ó', 'Ó', 'Ą', 'ą', 'Ś', 's', 'ł', 'Ł', 'ż', 'Ż', 'Ź', 'ź', 'ć', 'Ć', 'ń', 'Ń','-',"'","/","?", '"', ":", 'ś', '!','.', '&', '&', '#', ';', '[',']', '(', ')', '`', '%', '?', '?', '?');
			$miedzyn = array('-','-','-','e', 'e', 'o', 'o', 'a', 'a', 's', 's', 'l', 'l', 'z', 'z', 'z', 'z', 'c', 'c', 'n', 'n','-',"","","","","",'s','','', '', '', '', '',  '', '', '', '', '', '', '');
			global $wpdb;
			foreach ($_POST['acf'] as $ka => $a) {
				if (strpos($ka,'field_') !== false) {
					$field = get_field_object($ka);
					if ($field && strpos($field['name'], 'usluga_') !== false) {
						if (is_array($a)) {
							$val = array_values($a);
							$keys = array_keys($a);
							if (strlen($val[0]) > 0) {
								$slug = mb_strtolower(str_replace($polskie, $miedzyn, $val[0]), 'UTF-8');
								if ($slug) {
									$table = $wpdb->prefix.'postmeta';
									$wpdb->get_results('delete from '.$table.' where meta_key = "'.$field['name'].'_slug" and post_id = '.$_POST['post_ID']);

									$data = array(
										'post_id' => $_POST['post_ID'],
										'meta_key' => $field['name'].'_slug',
										'meta_value' => $slug,
									);
									$wpdb->insert( $table, $data);
								}
							}
						}
					}
					if ($field && strpos($field['name'], 'region') !== false) {
						if (strlen($a) > 0) {
							$parts = explode(', ', $a);
							$slug = '';
							foreach ($parts as $kpart => $part) {
								$slug .= mb_strtolower(str_replace('-','_', str_replace('--', '-', str_replace($polskie, $miedzyn, trim($part)))), 'UTF-8').($kpart < count($parts)-1 ? '-' : '');
							}
							if ($slug) {
								global $wpdb;
								$table = $wpdb->prefix.'postmeta';
								$wpdb->get_results('delete from '.$table.' where meta_key = "'.$field['name'].'_slug" and post_id = '.$_POST['post_ID']);

								$data = array(
									'post_id' => $_POST['post_ID'],
									'meta_key' => $field['name'].'_slug',
									'meta_value' => $slug,
								);
								$wpdb->insert( $table, $data);
							}
						}
					}
				}
			}
		}
	}
}

add_filter( 'wp_insert_post_data' , 'filter_post_data' , '99', 2 );
function filter_post_data( $data , $postarr ) {
	global $wpdb;
	$wpdb->get_results('update '.$wpdb->prefix.'postmeta set meta_value = "" where meta_key like "usluga_%" and (meta_key like "%_slug" or meta_key like "%_opis_uslugi" or meta_key like "%_obraz_uslugi") and post_id = '.$postarr['ID']);
    return $data;
}

add_action('wp_ajax_get_districts', 'get_districts');
add_action('wp_ajax_nopriv_get_districts', 'get_districts');
function get_districts(){
    $w = $_POST['w'];
	global $wpdb;
	if ($pow = $wpdb->get_results('SELECT name from '.$wpdb->prefix.'geo_list where id_parent = (SELECT id from '.$wpdb->prefix.'geo_list where name = "'.$w.'" limit 1) and id_type = 2')) {
		$html = '';
		$polskie = array(',', ' - ',' ','ę', 'Ę', 'ó', 'Ó', 'Ą', 'ą', 'Ś', 's', 'ł', 'Ł', 'ż', 'Ż', 'Ź', 'ź', 'ć', 'Ć', 'ń', 'Ń','-',"'","/","?", '"', ":", 'ś', '!','.', '&', '&', '#', ';', '[',']', '(', ')', '`', '%', '?', '?', '?');
		$miedzyn = array('-','-','-','e', 'e', 'o', 'o', 'a', 'a', 's', 's', 'l', 'l', 'z', 'z', 'z', 'z', 'c', 'c', 'n', 'n','-',"","","","","",'s','','', '', '', '', '',  '', '', '', '', '', '', '');

		foreach ($pow as $p) {
			$slug = mb_strtolower(str_replace($polskie, $miedzyn, $p->name), 'UTF-8');
			$html .= '<option value="'.$slug.'">'.$p->name.'</option>';
		}

		echo $html;
	}

    die();
}

add_action('wp_ajax_get_cities', 'get_cities');
add_action('wp_ajax_nopriv_get_cities', 'get_cities');
function get_cities(){
    $p = $_POST['p'];
    $w = $_POST['w'];
	global $wpdb;
	if ($cities = $wpdb->get_results('select name from '.$wpdb->prefix.'geo_list where id_type = 3 and id_parent = (SELECT id from '.$wpdb->prefix.'geo_list where id_parent = (SELECT id from '.$wpdb->prefix.'geo_list where name = "'.$w.'" limit 1) and id_type = 2 and name = "'.$p.'" limit 1)')) {
		$html = '';
		$polskie = array(',', ' - ',' ','ę', 'Ę', 'ó', 'Ó', 'Ą', 'ą', 'Ś', 's', 'ł', 'Ł', 'ż', 'Ż', 'Ź', 'ź', 'ć', 'Ć', 'ń', 'Ń','-',"'","/","?", '"', ":", 'ś', '!','.', '&', '&', '#', ';', '[',']', '(', ')', '`', '%', '?', '?', '?');
		$miedzyn = array('-','-','-','e', 'e', 'o', 'o', 'a', 'a', 's', 's', 'l', 'l', 'z', 'z', 'z', 'z', 'c', 'c', 'n', 'n','-',"","","","","",'s','','', '', '', '', '',  '', '', '', '', '', '', '');

		foreach ($cities as $p) {
			$slug = mb_strtolower(str_replace($polskie, $miedzyn, $p->name), 'UTF-8');
			$html .= '<option value="'.$slug.'">'.$p->name.'</option>';
		}

		echo $html;
	}

    die();
}