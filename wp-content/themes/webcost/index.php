<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package webcost
 */

get_header();
?>

	<main id="primary" class="site-main">
		<?php if ( !is_front_page() && is_home() ) : ?>
			<header class="entry-header container">
				<h1>Blog</h1>
			</header>
		<?php endif; ?>

		<?php if (have_posts()):
				$i = 1;
				while(have_posts()): the_post(); ?>
					<div class="article_miniature<?php if ($i%2 == 1) echo " reverse"; ?>">
						<div class="container">
							<article class="post row<?php if ($i%2 == 1) echo " flex-sm-row-reverse"; ?>" id="post-<?php the_ID(); ?>">
								<div class="col-xs-12 col-md-6">
									<img class="thumb" src="<?php echo get_the_post_thumbnail_url( $post->ID, 'post_body_img' ); ?>" alt="<?php the_title(); ?>">
								</div>
								<div class="col-xs-12 col-md-6 content">
									<div class="entry-meta">
										<?php
											webcost_posted_on();
											echo "<span class='d-none d-lg-flex'> - </span>";
											the_category();
										?>
									</div>
									<a href="<?php echo get_permalink(); ?>">
										<?php
											the_title( '<h1 class="entry-title">', '</h1>' );
										?>
									</a>
									<div class="entry-content"><?php $content = strip_tags(get_the_content()); (strlen($content) > 150) ? $content=substr($content, 0, 150)."..." : $content; echo $content; ?></div>
									<a class="btn purple-outline see_more" href="<?php echo get_permalink(); ?>">Czytaj  więcej</a>
								</div>
							</article>
							<?php
								if ($i == count($posts))
									kama_pagenavi($before = '', $after = '', $echo = true, $args = array(), $wp_query = $wp_query);
							?>
						</div>
					</div>

			<?php
					$i++;
				endwhile;
			endif;
			// do_shortcode( '[nearby_firm]' );
			echo "<div class='promo_block'>".do_shortcode( '[promo_block]' )."</div>";
		?>
	</main><!-- #main -->

<?php
// get_sidebar();
get_footer();
