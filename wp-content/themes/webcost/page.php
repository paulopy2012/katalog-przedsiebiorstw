<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package webcost
 */
get_header();
?>

	<main id="primary" class="site-main">
		<?php if (is_front_page()): ?>
			<section id="main_slider_wrapper">
				<?php
					echo do_shortcode('[smartslider3 slider="2"]');
				?>
				<div id="slider_search_panel" class="container">
					<form id="slider_search_form" class="form-inline" action="/">
					<input id="type_c" name="c" type="hidden" class="form-control" placeholder="BRANŻA">
					<div class="name_searcher input-group mb-2 mb-lg-0 mr-sm-2">
						<div class="input-group-prepend">
						<div class="input-group-text"><img src="/wp-content/themes/webcost/assets/img/search.svg"></div>
						</div>
						<input type="text" autocomplete="off" class="form-control" name="s" id="name" placeholder="Znajdź firmę, branżę">
					</div>

					<div id="place_search" class="place_searcher no-border input-group mb-2 mb-lg-0 mr-lg-2">
						<div class="input-group-prepend">
						<div class="input-group-text"><img src="/wp-content/themes/webcost/assets/img/place.svg"></div>
						</div>
						<div class="dropdown" style="flex-grow: 1">
							<input autocomplete="off" type="text" class="form-control" id="place" name="region" placeholder="miejscowość, powiat">
						</div>
					</div>
					<input type="hidden" name="post_type[]" value="firmy" />

					<button type="submit" class="blue btn btn-primary mb-2 mb-sm-0">SZUKAJ</button>
					</form>
				</div>
			</section>
			<?php
				echo do_shortcode('[geo_list position="home"]');
			?>
			<section id="industry_search_block">
				<div class="block_title_wrapper">
					<span class="block_title">Ogólnopolska baza firm<br>- KatalogPrzedsiebiorstw.pl</span>
				</div>
				<div class="block_sub_text_wrapper">
					<span class="block_sub_text">
						Szukaj po branży
					</span>
				</div>
				<?php
					echo do_shortcode('[firm_categories number=10]');
				?>
				<a role="button" href="<?php echo get_permalink(154130); ?>">
					Więcej branż
				</a>
			</section>

			<?php
				do_shortcode('[paid_firm]');
				// do_shortcode('[nearby_firm]');
			?>
			<section class='last_firms'>
				<h3 class='block_title'>Ostatnio dodane firmy</h3>
				<?php echo do_shortcode('[last_firm]'); ?>
			</section>

			<section class='last_posts'>
				<?php echo do_shortcode('[last_blog]'); ?>
			</section>

			<section class="promo">
				<?php echo do_shortcode('[promo_block]');?>
			</section>

			<?php do_shortcode('[home_text]'); ?>

		<?php else : ?>
			<?php if ($post->ID == 253) : ?>
				<div class="container">
			<?php endif; ?>

			<?php

			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				// if ( comments_open() || get_comments_number() ) :
				// 	comments_template();
				// endif;

			endwhile; // End of the loop.
			?>

			<?php if ($post->ID == 253) : ?>
				</div>
			<?php endif; ?>
		<?php endif; ?>

	</main><!-- #main -->

<?php
// get_sidebar();
get_footer();
