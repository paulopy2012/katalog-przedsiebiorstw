<form id="searchform" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="name_searcher input-group mb-2 mb-sm-0 mr-sm-2">
        <input id="type_c" name="c" type="hidden" class="form-control" placeholder="BRANŻA">
        <input id="region" name="region" type="hidden" class="form-control">
        <div class="input-group-prepend">
            <div class="input-group-text">
                <img src="/wp-content/themes/webcost/assets/img/search.svg">
            </div>
        </div>
        <input autocomplete="off" id="name" type="text" class="form-control" name="s" placeholder="Znajdź firmę, branżę" value="<?php echo get_search_query(); ?>">
    </div>
    <input type="hidden" name="post_type[]" value="firmy" />
    <button type="submit" class="blue btn btn-primary mb-2 mb-sm-0">SZUKAJ</button>
</form>