<?php

    require('wp-load.php');
    global $wpdb;

    if (isset($_GET['type'])) {
        if ($_GET['type'] == 'cat') {
            echo json_encode([
                'parents' => $wpdb->get_results("SELECT * FROM wp_terms t JOIN wp_term_taxonomy tt ON tt.term_id = t.term_id WHERE tt.taxonomy = 'category' and tt.parent = 0 ORDER BY parent, t.term_id"),
                'sub' => $wpdb->get_results("SELECT * FROM wp_terms t JOIN wp_term_taxonomy tt ON tt.term_id = t.term_id WHERE tt.taxonomy = 'category' and tt.parent <> 0 ORDER BY parent, t.term_id")
            ]);
        } elseif ($_GET['type'] == 'firm') {
            /*
                delete FROM kp_postmeta WHERE post_id IN ( SELECT ID FROM kp_posts WHERE post_type = 'firmy');
                delete FROM kp_term_relationships WHERE object_id IN ( SELECT ID FROM kp_posts WHERE post_type = 'firmy');
                DELETE FROM kp_posts WHERE post_type = 'firmy';
                delete FROM kp_postmeta WHERE post_id IN ( SELECT ID FROM kp_posts WHERE post_type = 'attachment' AND post_modified >= '2022-04-15 10:24:31');
                delete FROM kp_term_relationships WHERE object_id IN ( SELECT ID FROM kp_posts WHERE post_type = 'attachment' AND post_modified >= '2022-04-15 10:24:31');
                delete FROM kp_posts WHERE post_type = 'attachment' AND post_modified >= '2022-04-15 10:24:31';
            */
            $firms = [];
            if ($list = $wpdb->get_results("SELECT pd.*, p.post_content, p.post_title, p.post_excerpt, p.post_status, p.comment_status FROM wp_posts_data pd JOIN wp_posts p ON p.ID = pd.post_ID")) {
                $polskie = array(',', ' - ',' ','ę', 'Ę', 'ó', 'Ó', 'Ą', 'ą', 'Ś', 's', 'ł', 'Ł', 'ż', 'Ż', 'Ź', 'ź', 'ć', 'Ć', 'ń', 'Ń','-',"'","/","?", '"', ":", 'ś', '!','.', '&', '&', '#', ';', '[',']', '(', ')', '`', '%', '?', '?', '?');
                $miedzyn = array('-','-','-','e', 'e', 'o', 'o', 'a', 'a', 's', 's', 'l', 'l', 'z', 'z', 'z', 'z', 'c', 'c', 'n', 'n','-',"","","","","",'s','','', '', '', '', '',  '', '', '', '', '', '', '');
                foreach ($list as $firm) {
                    preg_match('/<!--:pl-->(.+?)<!--:-->/ms', $firm->post_title, $title);
                    if (count($title) == 0) {
                        preg_match('/\[\:pl\](.+?)\[\:\]/ms', $firm->post_title, $title);
                    }
                    preg_match('/<!--:pl-->(.+?)<!--:-->/ms', $firm->post_content, $content);
                    if (count($content) == 0) {
                        preg_match('/\[\:pl\](.+?)\[\:\]/ms', $firm->post_content, $content);
                    }
                    if ($category = $wpdb->get_results("SELECT tr.term_taxonomy_id FROM wp_term_relationships tr JOIN wp_term_taxonomy tt ON tt.term_taxonomy_id = tr.term_taxonomy_id WHERE tt.taxonomy = 'category' and tr.object_id =".$firm->post_ID)) {
                        $ids_par = json_decode(file_get_contents('category_parents.list'), true)['ids'];
                        $ids_sub = json_decode(file_get_contents('category_sub.list'), true)['ids'];
                        foreach($category as $ccat=>$cat) {
                            if (isset($ids_par[$cat->term_taxonomy_id]))
                                $category[$ccat] = (int)$ids_par[$cat->term_taxonomy_id];
                            elseif (isset($ids_sub[$cat->term_taxonomy_id]))
                                $category[$ccat] = (int)$ids_sub[$cat->term_taxonomy_id];
                            else
                                unset($category[$ccat]);
                        }
                    }

                    if (!$firm->wojewodztwo && $firm->miejscowosc) {
                        if ($w = $wpdb->get_row('SELECT name FROM kp_geo_list WHERE id = (SELECT id_parent from kp_geo_list where id = (SELECT id_parent from kp_geo_list where id_type = 3 and NAME = "'.$firm->miejscowosc.'" LIMIT 1) LIMIT 1)')) {
                            $firm->wojewodztwo = $w->name;
                        }
                    }

                    $firms[$firm->post_ID] = [
                        'post_title' => count($title) > 0 ? $title[count($title) - 1] : $firm->post_title,
                        'post_content' => count($content) > 0 ? $content[count($content) - 1] : $firm->post_content,
                        'post_status' => $firm->post_status,
                        'comment_status' => $firm->comment_status,
                        'meta_input' => [
                            'numer telefonu' => $firm->telefon,
                            'e-mail' => $firm->email,
                            'Adres' => $firm->ulica.' '.$firm->kod_pocztowy.' '.$firm->wojewodztwo.' '.$firm->miejscowosc,
                            'strona_internetowa' => $firm->strona_www,
                            'krotki_opis' => '',
                            'region' => $firm->wojewodztwo.', '.$firm->miejscowosc,
                            'region_slug' => mb_strtolower(str_replace($polskie, $miedzyn, str_replace('-','_', trim($firm->wojewodztwo))), 'UTF-8').'-'.mb_strtolower(str_replace($polskie, $miedzyn,  str_replace('-','_', trim($firm->miejscowosc))), 'UTF-8'),
                        ],
                        'post_category' => $category,
                        'tags_input' => explode(',', $firm->tagi),
                        'image' => $firm->img_logo,
                    ];
                }
            }
            echo json_encode([
                'firms' => $firms
            ]);
            file_put_contents('firms.list', json_encode([
                'firms' => $firms
            ]));
        }
    } else {
        return 'error';
    }

?>