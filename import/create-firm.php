<?php

    require('wp-load.php');
    global $wpdb;

    if (isset($_POST['company'])) {
        require_once ABSPATH . 'wp-admin/includes/image.php';
        require_once ABSPATH . 'wp-admin/includes/file.php';
        require_once ABSPATH . 'wp-admin/includes/media.php';

        $ids = [];
        foreach ($_POST['company'] as $company) {
            // $query = new WP_Query([
            //     'title' => $company['post_title'],
            //     'post_type' => 'firmy',
            // ]);
            // if (count($query->posts) > 0) {
            //     // $company['ID'] = $query->posts[0]->ID;
            //     $ids[$company['ID']]['update'][] = $query->posts[0]->ID;
            // }

            $company['post_author'] = 1;
            $company['post_type'] = 'firmy';

            $post_id = 0;
            $post_id = wp_insert_post($company, true);
            if( is_wp_error($post_id) ){
                $ids[$company['ID']]['error'][] = $post_id->get_error_message();
            }

            if ($post_id == 0) {
                echo json_encode([
                    'state' => 0,
                    'error' => $company['ID']
                ]);
                die;
            }

            if (isset($company['image'])) {
                $attachment_id = media_sideload_image( $company['image'],  $post_id, null, 'id');
                if (is_numeric($attachment_id))
                    set_post_thumbnail( $post_id, $attachment_id );
            }
        }

        echo json_encode([
            'state' => 1,
            'report' => $ids,
        ]);
    }
    die;

?>