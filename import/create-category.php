<?php

    require('wp-load.php');
    global $wpdb;

    if (isset($_POST['list'])) {
        if (!file_exists('category_'.$_POST['type'].'.list')) {
            $ids_compare = [];
            foreach ($_POST['list'] as $cat) {
                if ($cat['term_id'] == 1)
                    continue;

                $term = wp_insert_term(
                    $cat['name'],
                    $cat['taxonomy'],
                    array(
                        'description' => $cat['description'],
                        'parent' => $_POST['type'] == 'sub' ? $_POST['ids'][$cat['parent']] : $cat['parent'],
                        'slug' => $cat['slug'],
                    ),
                );

                $ids_compare[$cat['term_id']] = $term['term_id'];
            }
            echo json_encode(['ids' => $ids_compare]);
            file_put_contents('category_'.$_POST['type'].'.list', json_encode(['ids' => $ids_compare]));
        } else {
            echo file_get_contents('category_'.$_POST['type'].'.list');
        }
    }
    die;

?>