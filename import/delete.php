<?php

require('wp-load.php');
global $wpdb;

$firms = $wpdb->get_results('select ID, post_content from kp_posts WHERE post_content LIKE "%{tab%" or post_content LIKE "%[tab%" or post_content LIKE "%[/tab%"' );
if (count($firms) > 0) {
    foreach ($firms as $firm) {
        $post_content = preg_replace('/{tab=(.+?)]/ms', '', $firm->post_content);
        $post_content = preg_replace('/{tab=(.+?)}/ms', '', $post_content);
        $post_content = preg_replace('/\[tabs(.+?)\]/ms', '', $post_content);
        $post_content = preg_replace('/\[tab(.+?)\]/ms', '', $post_content);
        $post_content = preg_replace('/\[\/tab(.+?)\]/ms', '', $post_content);
        $post_content = preg_replace('/\[\/tabs\]/ms', '', $post_content);

        $wpdb->update( 'kp_posts',
            [ 'post_content' => $post_content ],
            [ 'ID' => $firm->ID ]
        );
    }
}

?>